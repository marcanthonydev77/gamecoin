<?php



/**

 * The plugin bootstrap file

 *

 * This file is read by WordPress to generate the plugin information in the plugin

 * admin area. This file also includes all of the dependencies used by the plugin,

 * registers the activation and deactivation functions, and defines a function

 * that starts the plugin.

 *
 * @link              https://modeltheme.com/
 * @since             1.0.0
 * @package           NFT Marketplace Core
 *
 * @wordpress-plugin
 * Plugin Name:       NFT Marketplace Core
 * Plugin URI:        https://modeltheme.com/
 * Description:       NFT Marketplace Core, by ModelTheme.
 * Version:           1.0
 * Author:            ModelTheme
 * Author URI:        https://modeltheme.com/
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       nft-marketplace-core
 * Domain Path:       /languages
 */


// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * Currently plugin version.
 * Start at version 1.0.0 and use SemVer - https://semver.org
 * Rename this for your plugin and update it as you release new versions.
 */
define( 'Nft_marketplace_core_VERSION', '1.2.2' );


/**
 * Store plugin base dir, for easier access later from other classes.
 * (eg. Include, pubic or admin)
 */
define( 'nft_marketplace_core_BASE_DIR', plugin_dir_path( __FILE__ ) );




/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-nft-marketplace-core-deactivator.php
 */
function nft_marketplace_core_deactivate() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-nft-marketplace-core-deactivator.php';
	NFT_Marketplace_Core_Deactivator::deactivate();
}


register_deactivation_hook( __FILE__, 'nft_marketplace_core_deactivate' );
register_activation_hook( __FILE__, 'nft_marketplace_core_create_new_page' );

function nft_marketplace_core_create_new_page() {
  if ( ! current_user_can( 'activate_plugins' ) ) return;
  global $wpdb;

  if ( null === $wpdb->get_row( "SELECT post_name FROM {$wpdb->prefix}posts WHERE post_name = 'new-page-slug'", 'ARRAY_A' ) ) {
    $current_user = wp_get_current_user();
    $page = array(
      'post_title'  => esc_html__( 'NFT Listings','nft-marketplace-core' ),
      'post_status' => 'publish',
      'post_author' => $current_user->ID,
      'post_type'   => 'page',
    );
    wp_insert_post( $page );
  }
}

function nft_marketplace_core_get_url() {
  return plugin_dir_url( __FILE__ );
}

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */

require plugin_dir_path( __FILE__ ) . 'includes/class-nft-marketplace-core.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */


/* INCLUDE METABOX CATEGORIES*/
require_once plugin_dir_path( __FILE__ ) . 'includes/NFT_Marketplace_Core_CT_TAX_META.php';

global $NFT_Marketplace_Core;
$NFT_Marketplace_Core = new NFT_Marketplace_Core();
$NFT_Marketplace_Core->run();
