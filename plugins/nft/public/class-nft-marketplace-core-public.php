<?php
/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    NFT Marketplace Core
 * @subpackage NFT Marketplace Core/public
 * @author     ModelTheme <support@modeltheme.com>
 */
class NFT_Marketplace_Core_Public
{
    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;
    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;
    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }
	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in nft_marketplace_core_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The nft_marketplace_core_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_style( 'bootstrap-grid', plugin_dir_url( __FILE__ ) . 'css/bootstrap-grid.css',[], $this->version, 'all' );
        wp_enqueue_style( 'font-awesome5', plugin_dir_url( __FILE__ ) . 'css/fonts/font-awesome/all.min.css',[], '5.15.4', 'all' );
        wp_enqueue_style( $this->plugin_name."-alerts", plugin_dir_url( __FILE__ ) . 'css/nft-marketplace-core-alerts.css',[], $this->version, 'all' );
		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/nft-marketplace-core-public.css',[], $this->version, 'all' );
		if(isset(get_option( 'nft_marketplace_core_panel_styling' )['nft_marketplace_core_dark']) && get_option( 'nft_marketplace_core_panel_styling' )['nft_marketplace_core_dark'] == 'on' ) {
			wp_enqueue_style( 'dark-mode', plugin_dir_url( __FILE__ ) . 'css/nft-marketplace-core-dark-mode.css',[], $this->version, 'all' );
		}
	}
	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {
		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in nft_marketplace_core_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The nft_marketplace_core_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */
		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/nft-marketplace-core-public.js', array( 'jquery' ), $this->version, false );
        wp_enqueue_script( $this->plugin_name."-market", plugin_dir_url( __FILE__ ) . 'js/nft-marketplace-core-market.js', array( 'wp-i18n' ), $this->version, true );
        wp_set_script_translations( $this->plugin_name."-market", 'nft-marketplace-core' );
        wp_localize_script($this->plugin_name, 'ajax_search', array(
	        'url' => admin_url('admin-ajax.php'),
	    ));
	    wp_localize_script($this->plugin_name, 'ajax_var', array(
	        'url' => admin_url('admin-ajax.php'),
	        'nonce' => wp_create_nonce('ajax-nonce')
	    ));
	}
	//Custom Colors
	function nft_marketplace_core_dynamic_css(){
		$html = '';
		wp_enqueue_style(
	        'nft-marketplace-core-css',
	        plugin_dir_url( __FILE__ ) . '/css/nft-marketplace-core-custom-editor.css'
	    );
	    if(isset(get_option( 'nft_marketplace_core_panel_styling' )['nft_marketplace_core_main_text_color']) && get_option( 'nft_marketplace_core_panel_styling' )['nft_marketplace_core_main_text_color'] != ''){
			$nft_links_color = get_option( 'nft_marketplace_core_panel_styling' )['nft_marketplace_core_main_text_color'];
		} else {
			$nft_links_color = '#D01498';
		}
		if(isset(get_option( 'nft_marketplace_core_panel_styling' )['nft_marketplace_core_main_hover_color']) && get_option( 'nft_marketplace_core_panel_styling' )['nft_marketplace_core_main_hover_color'] != ''){
			$nft_hover_links_color = get_option( 'nft_marketplace_core_panel_styling' )['nft_marketplace_core_main_hover_color'];
		} else {
			$nft_hover_links_color = '#D01498';
		}
		if(isset(get_option( 'nft_marketplace_core_panel_styling' )['nft_marketplace_core_main_button_bg']) && get_option( 'nft_marketplace_core_panel_styling' )['nft_marketplace_core_main_button_bg'] != ''){
			$nft_button_bg = get_option( 'nft_marketplace_core_panel_styling' )['nft_marketplace_core_main_button_bg'];
		} else {
			$nft_button_bg = '#D01498';
		}
		if(isset(get_option( 'nft_marketplace_core_panel_styling' )['nft_marketplace_core_main_button_bg_hover']) && get_option( 'nft_marketplace_core_panel_styling' )['nft_marketplace_core_main_button_bg_hover'] != '' ){
			$nft_button_bg_hover = get_option( 'nft_marketplace_core_panel_styling' )['nft_marketplace_core_main_button_bg_hover'];
		} else {
			$nft_button_bg_hover = '#222';
		}
		$html .= '
		    .author-details-wrapper a.btn-edit,
		    .author-details-wrapper .author-edit-title a,
		    body .nft-listing-collection a,
		    .author-socials li a,
		    body .single-collection-title a {
				color: '.esc_html($nft_links_color).';
			}
			.nft-listing-meta .meta-category a:hover,
			.nft-listing-collection a:hover,
			.nft-listing-name a:hover,
			#sidebar .nft-marketplace-categories li a:hover,
			.author-details-wrapper a.btn-edit:hover,
			.author-details-wrapper .author-edit-title a:hover,
			.author-socials li a:hover {
				color: '.esc_html($nft_hover_links_color).' !important;
			}
			.nft-listing .nft-listing-image a.button,
			.nft-entry-summary button.single_nft_button,
			.author-edit-fields-wrapper button.button-listing {
				background: '.esc_html($nft_button_bg).' !important;
			}
			.nft-listing .nft-listing-image a.button:hover,
			.nft-entry-summary button.single_nft_button:hover,
			.author-edit-fields-wrapper button.button-listing:hover {
				background: '.esc_html($nft_button_bg_hover).' !important;
			}
		';
		wp_add_inline_style( 'nft-marketplace-core-css', $html );
	}


    //Overwrite search theme template
	function nft_marketplace_core_search_template($template){
	    global $wp_query;
	    if (!$wp_query->is_search)
	        return $template;
	    return dirname( __FILE__ ) . '/search.php';
	}


	//Overwrite author theme template
	function nft_marketplace_core_author_template($template){
	    global $wp_query;
	    if (!$wp_query->is_author)
	        return $template;
	    return dirname( __FILE__ ) . '/author.php';
	}


	//Overwrite Page NFT Listings
	function nft_marketplace_core_listings_template($template){
		global $wp_query;
		$theme_files = 'nft-marketplace-core/template-nft-listings.php';
        $exists_in_theme = locate_template( $theme_files, false );
        if (!$wp_query->is_page('nft-listings')) {
	        return $template;
	    } else {
	    	if ( $exists_in_theme != '' ) {
		        $template = $exists_in_theme;
		    } else {
		        if(file_exists(trailingslashit(dirname( __FILE__ ) . '/templates/template-nft-listings.php'))) {
		            $template = trailingslashit(dirname( __FILE__ ) . '/templates/template-nft-listings.php');
		        }else {
		            $template = dirname( __FILE__ ) . '/templates/template-nft-listings.php';
		        }
		    } 
		return $template;
	    } 
	}


    /* Allowed HTML*/
    function nft_marketplace_core_kses_allowed_html($tags, $context) {
      	switch($context) {
	        case 'html': 
	            $tags = array( 
	                'div' => array(
	                    'class' =>[],
                        'style' => [],
                        'id' => []
	                ),
	                'i' => array(
	                    'class' =>[],
	                ),
	                'span' => array(
	                    'class' =>[],
	                ),
	                'a'	  => array(
	                	'class'	=>[],
	                	'data-post_id' =>[],
                        'id' =>[],
                        "href"=>[]
	                ),
                    'h2' => [
                        'class' => []
                    ],
                    'input' => [
                        'class' => [],
                        'name' => [],
                        'id' => [],
                        'type' => [],
                        'value' => [],
                        'placeholder' => [],
                        'checked' => []
                    ],
                    'select' => [
                        'class' => [],
                        'name' => [],
                        'id' => [],
                    ],
                    'option' => [
                        'class' => [],
                        'name' => [],
                        'id' => [],
                        'type' => [],
                        'value' => [],
                        'placeholder' => [],
                    ],
                    'fieldset' => [
                        'class' => []
                    ],
                    'label' => [
                        'class' => [],
                        'for' => [],
                        'id' => [],
                    ],
                    'strong' => [
                            'class' => []
                    ],
                    'p' => [
                        'class' => []
                    ]
	            );
	            return $tags;
	        break;
	        case 'link': 
	            $tags = array( 
	                'a' => array(
	                    'href' =>[],
	                    'class' =>[],
	                    'title' =>[],
	                    'target' =>[],
	                    'rel' =>[],
	                    'data-commentid' =>[],
	                    'data-postid' =>[],
	                    'data-belowelement' =>[],
	                    'data-respondelement' =>[],
	                    'data-replyto' =>[],
	                    'aria-label' =>[],
	                ),
	                'img' => array(
	                    'src' =>[],
	                    'alt' =>[],
	                    'style' =>[],
	                    'height' =>[],
	                    'width' =>[],
	                ),
	            );
	            return $tags;
	        break;
	        case 'icon':
	            $tags = array(
	                'i' => array(
	                    'class' =>[],
	                ),
	            );
	            return $tags;
	        break;
	        case 'categories': 
	            $tags = array( 
	                'a' => array(
	                    'href' =>[],
	                    'class' =>[],
	                    'title' =>[],
	                    'target' =>[],
	                    'rel' =>[],
	                ),
	            );
	            return $tags;
	        break;
            case 'vite':
                $tags = array(
                    'script' => array(
                        'src' =>[],
                        'type' =>[],
                        'crossorigin' =>[],
                    ),
                    "link" => [
                        'href' =>[],
                        'rel' =>[],
                    ]
                );
                return $tags;
                break;
	        default: 
	            return $tags;
        }
    }


    function nft_marketplace_core_list_nft_template_from_directory($single)
    {
        global $wp_query, $post;
        $theme_files = 'nft-marketplace-core/list-nft-listing.php';
        $exists_in_theme = locate_template($theme_files, false);
        /* Checks for single template by post type */
        if ($post->post_type === 'nft-listing' && isset($_GET['list-nft']) && is_single()) {
            if ($exists_in_theme != '') {
                // Try to locate in theme first
                $single = $exists_in_theme;
            } else {
                if (file_exists(plugin_dir_path(__FILE__) . 'templates/list-nft-listing.php')) {
                    $single = plugin_dir_path(__FILE__) . 'templates/list-nft-listing.php';
                }
            }
        }
        return $single;
    }


    function nft_marketplace_core_edit_template_from_directory($single)
    {
        global $wp_query, $post;
        $theme_files = 'nft-marketplace-core/edit-nft-single.php';
        $exists_in_theme = locate_template($theme_files, false);
        /* Checks for single template by post type */
        if ($post->post_type === 'nft-listing' && isset($_GET['edit-nft']) && is_single()) {
            if ($exists_in_theme != '') {
                // Try to locate in theme first
                $single = $exists_in_theme;
            } else {
                if (file_exists(plugin_dir_path(__FILE__) . 'templates/edit-nft-single.php')) {
                    $single = plugin_dir_path(__FILE__) . 'templates/edit-nft-single.php';
                }
            }
        }
        return $single;
    }

    /**
     * Before publishing nft to blockchain.
     * @param $request WP_REST_Request
     * @return WP_REST_Response
     */
    public function nft_marketplace_core_validate_from_creator($request) {
        $data = $request->get_param("formData");
        $vr = $this->check_wpnc($data);

        if($vr !== false) {
            return $vr;
        }

        return new WP_REST_Response(["success" => true], 200);
    }

    /**
     * After publishing nft to blockchain.
     * @param $request WP_REST_Request
     * @return WP_REST_Response
     */
    public function nft_marketplace_core_add_nft_from_creator($request) {
        $data = $request->get_param("formData");
        $vr = $this->check_wpnc($data);

        if($vr !== false) {
            return $vr;
        }

        $transaction = $request->get_param("transaction");
        $uploadData = $request->get_param("uploadData")[0];
        $args = array(
            'hide_empty' => false, // also retrieve terms which are not used yet
            'meta_query' => array(
                array(
                    'key'       => 'nft_marketplace_core_taxonomy_blockchain_currency_chainid',
                    'value'     => '0x'.dechex($transaction["chainId"]),
                )
            ),
            'taxonomy'  => 'nft_listing_blockchains',
        );
        $term = get_terms( $args )[0];

        $args = array(
            'meta_query' => array(
                array(
                    'key' => 'nft_marketplace_core_nft_listing_token_id',
                    'value' => 1
                ),
                array(
                    'key' => 'nft_marketplace_core_nft_listing_address',
                    'value' => $transaction["to"]
                )
            ),
            'post_type' => 'nft-listing',
            'posts_per_page' => -1,
        );

        $posts = get_posts($args);

        if(count($posts) === 0) {
            $my_post = array(
                'post_title' => wp_strip_all_tags($data["name"]),
                'post_content' => isset($data["desc"]) ? $data["desc"] : "",
                'post_status' => 'publish',
                'post_author' => get_current_user_id(),
                'post_type' => "nft-listing",
                'comment_status' => "closed",
            );

            $my_post['meta_input'] = [
                "nft_marketplace_core_nft_listing_description_meta" => isset($data["desc"]) ? $data["desc"] : "",
                "nft_marketplace_core_nft_listing_content_meta" => isset($data["unlockable"]) ? $data["unlockable"] : "",
                "nft_marketplace_core_nft_listing_content" => isset($data["unlockableContent"]) ? $data["unlockableContent"] : "",
                "nft_marketplace_core_nft_listing_explicit_meta" => isset($data["explicit"]) ? $data["explicit"] : false,
                "nft_marketplace_core_nft_listing_owner" => $transaction["from"],
                "nft_marketplace_core_nft_listing_currency_price" => "0",
                "nft_marketplace_core_nft_listing_address" => $transaction["to"],
                "nft_marketplace_core_nft_listing_token_id" => 1
            ];

            foreach ($data["stats"] as $stat) {
                if($stat["name"] === "" || !isset($stat["value"])  || $stat["value"]["from"] === "" || $stat["value"]["to"] === "" || !is_array($stat) || !is_array($stat["value"]) ) {
                    break;
                }
                $my_post['meta_input']["nft_marketplace_core_nft_listing_group"][] = [
                    "nft_marketplace_core_nft_listing_stat_name" => esc_html($stat["name"]),
                    "nft_marketplace_core_nft_listing_stat_rate" => esc_html($stat["value"]["from"]),
                    "nft_marketplace_core_nft_listing_stat_out_of" => esc_html($stat["value"]["to"]),
                ];
            }

            foreach ($data["properties"] as $properties) {
                if($properties["name"] === "" || $properties["type"] === "" || !is_array($properties)) {
                    break;
                }
                $my_post['meta_input']["nft_marketplace_core_nft_listing_properties_group"][] = [
                    "nft_marketplace_core_nft_listing_property_name" => esc_html($properties["type"]),
                    "nft_marketplace_core_nft_listing_property_value" => esc_html($properties["name"]),
                ];
            }

            foreach ($data["levels"] as $levels) {
                if($levels["name"] === "" || !isset($levels["value"])  || $levels["value"]["from"] === "" || $levels["value"]["to"] === "" || !is_array($levels) || !is_array($levels["value"])) {
                    break;
                }

                $my_post['meta_input']["nft_marketplace_core_nft_listing_level_group"][] = [
                    "nft_marketplace_core_nft_listing_level_name" => esc_html($levels["name"]),
                    "nft_marketplace_core_nft_listing_level_rate" => esc_html($levels["value"]["from"]),
                    "nft_marketplace_core_nft_listing_level_out_of" => esc_html($levels["value"]["to"]),
                ];
            }


            // Insert the post into the database
            $post = wp_insert_post($my_post);
            set_post_thumbnail($post, $uploadData["id"]);
            if ($term !== false) {
               wp_set_post_terms($post,[$term->term_id],'nft_listing_blockchains');
            }
        }
        return new WP_REST_Response(["success" => true], 200);
    }

    public function check_wpnc($data) {
        $required = array('image', 'levels', 'name', 'price','properties', 'stats');

        if (count(array_intersect_key(array_flip($required), $data)) !== count($required)) {
            return new WP_REST_Response(["success" => false, "message" => esc_html__("Core: Something went wrong!","nft-marketplace-core")], 401);
        }

        if(!is_float($data["price"]) || !is_int($data["price"])) {
            return new WP_REST_Response(["success" => false, "message" => esc_html__("Core: Not a valid price!","nft-marketplace-core")], 401);
        }

        if(!is_array($data["image"])) {
            return new WP_REST_Response(["success" => false, "message" => esc_html__("Core: Not a valid image!","nft-marketplace-core")], 401);
        }

        return false;
    }


    //GET HEADER TITLE/BREADCRUMBS AREA
	function nft_marketplace_core_breadcrumbs(){
		$term = get_queried_object();
	    echo '<div class="nft_marketplace_core-breadcrumbs">';
	        echo '<div class="container">';
	            echo '<div class="row">';
	                echo '<div class="col-md-12">';
	                    if (is_singular('nft-listing')) {
	                        echo '<h1>'.get_the_title().'</h1>';
	                    } else if(is_search()) {
  							echo '<h1>'.esc_html__('Search results: ','nft-marketplace-core').'</h1>';
	                    } else if (is_tax()) {
	                    	$taxonomy = $term->taxonomy;
							$taxonomy_slug = $term->slug;
	                    	if(is_tax($taxonomy, $taxonomy_slug)) {
	                    		$term = get_queried_object();
								$taxonomy = $term->taxonomy;
								$taxonomy_slug = $term->slug;
								$taxonomy_name = $term->name;
		                    	echo '<h1>'.esc_html($taxonomy_name).'</h1>';
		                    }
	                    } else {
	                    	echo '<h1>'.apply_filters('nft_marketplace_core_taxonomy_heading', esc_html__('Explore All NFTs', 'nft-marketplace-core')).'</h1>'; 
	                    } 
	                echo'</div>';
	                echo '<div class="col-md-12">';
	                    echo '<ol class="breadcrumb">';
	                        $delimiter = '';
						    $name = esc_html__("Home", "nft-marketplace-core");
						        if ((!is_home() && !is_front_page()) || is_paged()) {
						            global $post;
						            global $product;
						            $home = home_url();
						            echo '<li><a href="' . esc_url($home) . '">' . esc_html($name) . '</a></li> ' . esc_html($delimiter) . '';
						            echo  '<li class="active">';
						                the_title();
						            echo  '</li>';
						        }
	                    echo '</ol>';
	                echo '</div>'; 
	            echo'</div>';
	        echo'</div>';
	    echo'</div>';
	}


	function nft_marketplace_core_taxonomy_template_from_directory_nft_listing($template){
		$custom_post_type = 'nft-listing';
		$taxonomy_array = array('nft-listing-category');
		$theme_files = 'nft-marketplace-core/taxonomy-nft-listing.php';
        $exists_in_theme = locate_template( $theme_files, false );
		foreach ($taxonomy_array as $taxonomy_single) {
			if ( is_tax($taxonomy_single) ) {
				if ( $exists_in_theme != '' ) {
		            $template = $exists_in_theme;
		        } else {
		            if(file_exists(trailingslashit(plugin_dir_path( __FILE__ ) . 'templates/taxonomy-nft-listing.php'))) {
		                $template = trailingslashit(plugin_dir_path( __FILE__ ) . 'templates/taxonomy-nft-listing.php');
		            }else {
		                $template = plugin_dir_path( __FILE__ ) . 'templates/taxonomy-nft-listing.php';
		            }
		        }
			}
	    }
	    return $template;
	}


	/* Filter the single_template with our custom function*/
	function nft_marketplace_core_single_template_from_directory_nft_listing($single) {
	    global $wp_query, $post;
	    $theme_files = 'nft-marketplace-core/single-nft-listing.php';
        $exists_in_theme = locate_template( $theme_files, false );
	    /* Checks for single template by post type */
	    if ( $post->post_type == 'nft-listing' && is_single() ) {
			if ( $exists_in_theme != '' ) {
		        $single = $exists_in_theme;
		    } else {
		        if ( file_exists( plugin_dir_path( __FILE__ ) . 'templates/single-nft-listing.php' ) ) {
		            $single = plugin_dir_path( __FILE__ ) . 'templates/single-nft-listing.php';
		        }
			}
	    }
	    return $single;
	}


	/*
	 * Love/Unlove a post
	 * Hooked into wp_ajax_ above to save post IDs when button clicked.
	 */
	function nft_marketplace_core_love_post() {
	  // Security measures for the ajax call
	    $nonce = $_POST['_wpnonce'];
	    if (!wp_verify_nonce($nonce, 'ajax-nonce'))
	        die("Security check has not passed.");
	    if (isset($_POST['nft_marketplace_core_love_post'])) {
	        $post_id = $_POST['post_id'];
	        // Get the count of loves for the particular post
	        $post_mt_love_count = get_post_meta($post_id, "nft_marketplace_core_love_count", true);
	        if (is_user_logged_in()) {
	        	$user_id = get_current_user_id();
		        // Get the users who loved the post 
		        $postmetadata_userIDs = get_post_meta($post_id, "nft_marketplace_core_user_loved");      
		        $users_loved =[];
		        if (count($postmetadata_userIDs) != 0) {
		            $users_loved = $postmetadata_userIDs[0];
		        }
		        if (!is_array($users_loved))
		            $users_loved =[];
		        $users_loved['User_ID-' . $user_id] = $user_id;
		        if (!$this->nft_marketplace_core_already_loved($post_id)) {       
		            // Love
		            update_post_meta($post_id, "nft_marketplace_core_user_loved", $users_loved);
		            update_post_meta($post_id, "nft_marketplace_core_love_count", ++$post_mt_love_count);
		            $response['count']   = $post_mt_love_count;
		            $response['message'] = '<i class="fa fa-heart"></i>';
		        } else {
		            // Unlove
		            $uid_key = array_search($user_id, $loved_users); // find the key
		            unset($loved_users[$uid_key]); // remove from array
		            update_post_meta($post_id, "nft_marketplace_core_user_loved", $loved_users); // Remove user ID from post meta
		            update_post_meta($post_id, "nft_marketplace_core_love_count", --$post_mt_love_count); // -1 count post meta
		            $response['count']   = $post_mt_love_count;
		            $response['message'] = '<i class="fa fa-heart-o"></i>';
		        }
	        } elseif(!is_user_logged_in()) {
	        	$response['count']   = $post_mt_love_count;
		        $response['message'] = '<i class="fa fa-heart-o"></i><span class="mtkb-knowledgebase-nolike">'.esc_html__('You have to login or register to like posts!', 'nft-marketplace-core').'</span>';
	        }  
	        wp_send_json($response);
	    }
	}


	/* Function to display the love button on the front-end below every post */
	function nft_marketplace_core_display_love_button() {
	    // Total counts for the post
	    $post_id = get_the_ID();
	    $love_count = get_post_meta($post_id, "nft_marketplace_core_love_count", true);
	    $count      = (empty($love_count) || $love_count == "0") ? '0' : $love_count;
	    // Prepare button html
	    if(is_user_logged_in() && get_post_type() == 'nft-listing') {
	    	if (!$this->nft_marketplace_core_already_loved($post_id)) {
	        	echo '<div class="nft-listing-wishlist"><a href="#" data-post_id="' . esc_attr($post_id) . '"> <i class="fa fa-heart-o"></i></a><span>'.esc_html($count).'</span></div>';
	    	} else {
	        	echo '<div class="nft-listing-wishlist nft-listing-wishlist-loved"><a href="#" data-post_id="' .esc_attr($post_id) . '"><i class="fa fa-heart"></i></a><span>'.esc_html($count).'</span></div>';
	    	}
	    } elseif(!is_user_logged_in() && get_post_type() == 'nft-listing') {
	    	if($count != '0') {
	    		echo '<div class="nft-listing-wishlist nft-listing-wishlist-loved"><a href="#" data-post_id="'.esc_attr($post_id).'"><i class="fa fa-heart-o"></i></a><span>'. esc_html($count).'</span></div>';
	    	} elseif($count == '0') {
	    		echo '<div class="nft-listing-wishlist"><a href="#" data-post_id="'.esc_attr($post_id).'"><i class="fa fa-heart-o"></i></a><span>'.esc_html($count).'</span></div>';
	    	}
	    }  
	}


	/* Function to check whether the user who clicks the love button already loved the post */
	function nft_marketplace_core_already_loved($post_id) {
	    $user_id = get_current_user_id();
	    $postmetadata_userIDs = get_post_meta($post_id, "nft_marketplace_core_user_loved", false);
	    $users_loved =[];
	    if (count($postmetadata_userIDs) != 0) {
	        $users_loved = $postmetadata_userIDs[0];
	    }
	    if (!is_array($users_loved))
	        $users_loved =[];
	    if (in_array($user_id, $users_loved)) {
	        return true;
	    } else {
	        return false;
	    }
	}


	/* SINGLE BLOCK NFT LISTING*/
	function nft_marketplace_core_single_nft_block()
    {
        global $post;
        $nft_marketplace_core_nft_listing_price = get_post_meta(get_the_ID(), 'nft_marketplace_core_nft_listing_currency_price', true);
        $nft_marketplace_core_nft_listing_symbol = get_post_meta(get_the_ID(), 'nft_marketplace_core_nft_listing_currency_symbol', true);
        echo '<div class="nft-listing-wrapper">';
        $thumbnail_src = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'medium');
        	echo '<div class="nft-listing-image">';
        		echo '<div class="overlay-container">';
        			echo '<div class="hover-container">';
        				echo '<div class="component add-to-cart">';
        					echo '<a class="button" href="#">' . apply_filters('nft_marketplace_core_bid_button', esc_html__('Place a Bid', 'nft-marketplace-core')) . '</a>';
        				echo '</div>';
        			echo '</div>';
        		echo '</div>';
        		if ($thumbnail_src) {
            		echo '<a class="nft-listing-media" href="' . esc_url(get_the_permalink()) . '"><img alt="listing-media" src="' . esc_url($thumbnail_src[0]) . '"></a>';
        		}
        	echo '</div>';
        	echo '<div class="nft-listing-title-metas">';
        		echo '<div class="title-wrapper">';
        			echo '<h3 class="nft-listing-name">';
        				echo '<a href="' . esc_url(get_the_permalink()) . '">';
        					the_title();
        				echo '</a>';
        			echo '</h3>';
        			do_action('nft_marketplace_core_single_nft_block_after_title');
        		echo '</div>';
        		echo '<div class="details-container">';
        			echo '<span>' . apply_filters('nft_marketplace_core_reserve_text', esc_html__('Reserve Price', 'nft-marketplace-core')) . '</span>';
        			if (!empty($nft_marketplace_core_nft_listing_price)) {
            			echo '<span class="nft-listing-price"><img src="' . nft_marketplace_core_get_url() . 'includes/images/token.svg" alt="Ether" width="10" height="10"> ' . esc_attr($nft_marketplace_core_nft_listing_price) . ' ' . esc_attr($nft_marketplace_core_nft_listing_symbol) . '</span>';
        			}
        		echo '</div>';
        	echo '</div>';
        echo '</div>';
    }


    /* NFT LISTING : CALL CATEGORY*/
    function nft_marketplace_core_single_nft_category() {
        $all_categories = get_the_term_list( get_the_ID(), 'nft-listing-category', '', ' / ' );
        if ($all_categories) {
            echo wp_kses($all_categories, 'categories');
        }
    }


    /* NFT LISTING : CALL SINGLE CATEGORY*/
	function nft_marketplace_core_single_nft_single_category() {
        $all_categories = get_the_terms( get_the_ID(), 'nft-listing-category');
        if($all_categories) {
	        $category = $all_categories[0];
	        $term_link = get_term_link($category);
	            echo '<a href="'.esc_url($term_link).'">'.esc_html($category->name).'</a>';
	    }
    }


    //SINGLE NFT : META INFO
	function nft_marketplace_core_single_nft_meta(){
		$nft_marketplace_core_nft_listing_sales = get_post_meta(get_the_ID(), 'nft_marketplace_core_nft_listing_sales', true);
	    echo '<div class="nft-listing-meta">';
	    	echo '<div class="meta-id">';
	       		echo '<span>'.apply_filters('nft_marketplace_core_id_text', esc_html__('ID', 'nft-marketplace-core')).': </span>'.get_the_ID().'';
	       	echo '</div>';
	       	echo '<div class="meta-category">';
	       		echo '<span>'.apply_filters('nft_marketplace_core_category_text', esc_html__('Category', 'nft-marketplace-core')).': </span>';
	       		do_action('nft_marketplace_core_single_nft_before_collection_text');
	       	echo '</div>';
	       	echo '<div class="meta-category">';
	       		echo '<span>'.apply_filters('nft_marketplace_core_presale_text', esc_html__('Presale', 'nft-marketplace-core')).': </span>';
	       		echo 'Yes';
	       	echo '</div>';
	       	if($nft_marketplace_core_nft_listing_sales) {
		       	echo '<div class="meta-category">';
		       		echo '<span>'.apply_filters('nft_marketplace_core_sales_text', esc_html__('Sales', 'nft-marketplace-core')).': </span>';
		       		echo esc_attr($nft_marketplace_core_nft_listing_sales);
		       	echo '</div>';
		    }
	    echo'</div>';
	}


	//SINGLE NFT : STATS TAB
	function nft_marketplace_core_single_nft_tab_stats() {
		$nft_marketplace_core_nft_listing_group = get_post_meta(get_the_ID(), 'nft_marketplace_core_nft_listing_group', true);
		echo '<section class="nft-listing-stats" id="tab-stats">';
			if($nft_marketplace_core_nft_listing_group) {
				echo '<ol>';
			    foreach (array_reverse($nft_marketplace_core_nft_listing_group) as $nft_listing) {
			        echo '<li>';
			            if(!empty($nft_listing['nft_marketplace_core_nft_listing_stat_name'])){ 
			                echo '<span>'.esc_html( $nft_listing['nft_marketplace_core_nft_listing_stat_name'] ).'</span>';
			            }
			            echo '<span>';
			                if(!empty($nft_listing['nft_marketplace_core_nft_listing_stat_rate'])){ 
			                    echo esc_html( $nft_listing['nft_marketplace_core_nft_listing_stat_rate'] );
			                }
			                echo esc_html__(' out of ','nft-marketplace-core');
			                if(!empty($nft_listing['nft_marketplace_core_nft_listing_stat_out_of'])){ 
			                    echo esc_html( $nft_listing['nft_marketplace_core_nft_listing_stat_out_of'] );
			                }
			            echo '</span>';
			        echo '</li>';               
			    }
			    echo '</ol>';
	        } else {
	        	echo '<p>'.apply_filters('nft_marketplace_core_tab_no_text', esc_html__('This NFT Listing does not have', 'nft-marketplace-core')).' '.apply_filters('nft_marketplace_core_stats_tab_text', esc_html__('Stats', 'nft-marketplace-core')).'</p>';
	        }
    	echo '</section>';
	}


	//SINGLE NFT : PROPERTIES TAB
	function nft_marketplace_core_single_nft_tab_properties() {
		$nft_marketplace_core_nft_listing_properties_group = get_post_meta(get_the_ID(), 'nft_marketplace_core_nft_listing_properties_group', true);
		echo '<section class="nft-listing-properties" id="tab-properties">';
			if($nft_marketplace_core_nft_listing_properties_group) {
		            foreach (array_reverse($nft_marketplace_core_nft_listing_properties_group) as $nft_listing) {
		            echo '<div class="properties-item">';
		                if(!empty($nft_listing['nft_marketplace_core_nft_listing_property_name'])){ 
		                    echo '<span class="properties-item-name">'.esc_html( $nft_listing['nft_marketplace_core_nft_listing_property_name'] ).'</span>';
		                }
		                if(!empty($nft_listing['nft_marketplace_core_nft_listing_property_value'])){ 
		                    echo '<span class="properties-item-value">'.esc_html( $nft_listing['nft_marketplace_core_nft_listing_property_value'] ).'</span>';
		                }
		            echo '</div>';              
		            }
        	} else {
        		echo '<p>'.apply_filters('nft_marketplace_core_tab_no_text', esc_html__('This NFT Listing does not have', 'nft-marketplace-core')).' '.apply_filters('nft_marketplace_core_properties_tab_text', esc_html__('Properties', 'nft-marketplace-core')).'</p>';
        	}
    	echo '</section>';
	}


	//SINGLE NFT : LEVELS TAB
	function nft_marketplace_core_single_nft_tab_levels() {
		$nft_marketplace_core_nft_listing_level_group = get_post_meta(get_the_ID(), 'nft_marketplace_core_nft_listing_level_group', true);
		echo '<section class="nft-listing-levels" id="tab-levels">';
			if($nft_marketplace_core_nft_listing_level_group) {
	            echo '<ol>';
			    foreach (array_reverse($nft_marketplace_core_nft_listing_level_group) as $nft_listing) {
			        echo '<li>';
			            if(!empty($nft_listing['nft_marketplace_core_nft_listing_level_name'])){ 
			                echo '<span>'.esc_html( $nft_listing['nft_marketplace_core_nft_listing_level_name'] ).'</span>';
			            }
			            echo '<span>';
				            if(!empty($nft_listing['nft_marketplace_core_nft_listing_level_rate'])){ 
				                echo esc_html( $nft_listing['nft_marketplace_core_nft_listing_level_rate'] ); 
				            }
				            echo esc_html__(' out of ','nft-marketplace-core');
				            if(!empty($nft_listing['nft_marketplace_core_nft_listing_level_out_of'])){ 
				                echo esc_html( $nft_listing['nft_marketplace_core_nft_listing_level_out_of'] );
				            }
			            echo '</span>';
			        echo '</li>';                
			    }
	        	echo '</ol>';
	        } else {
	        	echo '<p>'.apply_filters('nft_marketplace_core_tab_no_text', esc_html__('This NFT Listing does not have', 'nft-marketplace-core')).' '.apply_filters('nft_marketplace_core_levels_tab_text', esc_html__('Levels', 'nft-marketplace-core')).'</p>';
	        }
    	echo '</section>';
	}


	//SINGLE NFT : AUTHOR TAB
	function nft_marketplace_core_single_nft_tab_auth() {
		$author_id = get_post_field( 'post_author', get_the_ID() );
		$author    = get_the_author_meta( 'display_name', $author_id );
		$description    = get_the_author_meta( 'description', $author_id );
		echo '<h4>'.esc_html__('About','nft-marketplace-core').' '.esc_attr($author).'</h4>';
		echo get_avatar( $author_id, 96);
        echo '<p>'.esc_html($description).'</p>';
	}


	//SINGLE NFT : AUTHOR TAB
	function nft_marketplace_core_single_nft_tabs_wrapper() {
		$author_id = get_post_field( 'post_author', get_the_ID() );
		$author    = get_the_author_meta( 'display_name', $author_id );
		$nft_marketplace_core_nft_listing_description_meta = get_post_meta(get_the_ID(), 'nft_marketplace_core_nft_listing_description_meta', true);
		echo '<div class="nft-listing-tabs">';
            echo '<ul class="nft-tab-list">';
	            if (!empty($nft_marketplace_core_nft_listing_description_meta)) {
	            	echo '<li class="description_tab"><a href="#tab-description">'.apply_filters('nft_marketplace_core_description_tab_text', esc_html__('Description', 'nft-marketplace-core')).'</a></li>';
	            }
	            echo '<li class="stats_tab"><a href="#tab-stats">'.apply_filters('nft_marketplace_core_stats_tab_text', esc_html__('Stats', 'nft-marketplace-core')).'</a></li>';
	            echo '<li class="properties_tab"><a href="#tab-properties">'.apply_filters('nft_marketplace_core_properties_tab_text', esc_html__('Properties', 'nft-marketplace-core')).'</a></li>';
	            echo '<li class="levels_tab"><a href="#tab-levels">'.apply_filters('nft_marketplace_core_levels_tab_text', esc_html__('Levels', 'nft-marketplace-core')).'</a></li>';
	            echo '<li class="author_tab"><a href="#tab-author">'.apply_filters('nft_marketplace_core_author_tab_text', esc_html__('About Author', 'nft-marketplace-core')).'</a></li>';
            echo '</ul>';
            if (!empty($nft_marketplace_core_nft_listing_description_meta)) {
                echo '<section class="nft-listing-crypto-description" id="tab-description">';
                	echo '<p>'.esc_html__('Created by ','nft-marketplace-core').' <strong>'.esc_attr($author).'</strong></p>';
                    echo '<p>'.esc_html($nft_marketplace_core_nft_listing_description_meta).'</p>';
                echo '</section>';
            }
            do_action('nft_marketplace_core_single_nft_before_author_tab');
            echo '<section class="nft-listing-author" id="tab-author">';
                do_action('nft_marketplace_core_single_nft_after_levels_tab');
            echo '</section>';
        echo '</div>';
	}


	//SINGLE NFT : RELATED NFTS
	function nft_marketplace_core_single_nft_related() {
		echo '<section class="related nft-listing">';
            echo '<h3>'.apply_filters('nft_marketplace_core_related_nfts', esc_html__('Related NFTs', 'nft-marketplace-core')).'</h3>';
                echo '<div class="row">';
                    $args=array(  
                        'post__not_in'          => array(get_the_ID()),  
                        'posts_per_page'        => 3, 
                        'ignore_sticky_posts'   => 1,
                        'post_type'             => 'nft-listing'  
                    );
                    $my_query = new wp_query( $args );  
                    while( $my_query->have_posts() ) {  
                        $my_query->the_post();  
                        echo '<div class="col-md-3 post">';
                            do_action('nft_marketplace_core_related_listing_query');
                        echo '</div>';
                    }
                    wp_reset_query();
                echo '</div>';
        echo '</section>';
	}


	//SINGLE NFT : COLLECTION BLOCK
	function nft_marketplace_core_single_nft_collection_block() {
		echo '<div class="nft-listing-collection">';
			$nft_category = get_the_term_list( get_the_ID(), 'nft-listing-category', '', ' / ' );
            if ($nft_category) {
				$term = get_term_by( 'slug', $nft_category, 'nft-listing-category');
				echo '<p>'.apply_filters('nft_marketplace_core_collection_title', esc_html__('Collection', 'nft-marketplace-core')).'</p>';
				$img_id = '';
				if(!empty($img_id)) {
	    			$img_id = get_term_meta( $term->term_id, 'category-image-id', true );
	    			$thumbnail_src = wp_get_attachment_image_src( $img_id, 'thumbnail' );
	    			if($thumbnail_src) {
	    				echo '<img class="nft-listing-collection-icon" alt="cat-image" src="'.esc_url($thumbnail_src[0]).'">';
	    			}
	    		}
    			do_action('nft_marketplace_core_single_nft_before_collection_text');
    	}
       echo '</div>';
	}


	//SET POST VIEW
	function nft_marketplace_core_views_count() {
	    $countKey = 'nft_marketplace_core_post_views_count';
	    $postID = get_the_ID();
	    if(get_post_type() == 'nft-listing' && is_singular() ) {
	    	$count = get_post_meta($postID, $countKey, true);
		    if($count==''){
		        $count = 0;
		        update_post_meta($postID, $countKey, '0');
		    }else{
		        $count++;
		        update_post_meta($postID, $countKey, $count);
		    }
	    }
	}


	// SINGLE NFT : DISPLAY VIEW COUNT
	function nft_marketplace_core_display_views_count() {
	    $post_id = get_the_ID();
	    $views_count = get_post_meta($post_id, "nft_marketplace_core_post_views_count", true);
	    echo '<div class="nft-listing-views-counter"><i class="fa fa-eye"></i><span>'.esc_html($views_count).' '.esc_html__('views','nft-marketplace-core').'</span></div>';
	}


	// SINGLE NFT : DISPLAY OWNER
	function nft_marketplace_core_display_owner() {
	  	$nft_marketplace_core_nft_listing_owner = get_post_meta(get_the_ID(), 'nft_marketplace_core_nft_listing_owner', true); 
	  	if($nft_marketplace_core_nft_listing_owner) {
		    echo '<div class="nft-listing-owner">'.esc_html__('Owned by ','nft-marketplace-core').' '.esc_attr($nft_marketplace_core_nft_listing_owner).'</div>';
		}
	}


	//ARCHIVE NFT : PAGINATION
	function nft_marketplace_core_archive_pagination() {
		echo '<div class="modeltheme-pagination-holder col-md-12">';               
				$nft_listing_single_cat = wp_get_post_terms(get_the_ID(), 'nft-listing-category');
				$nr_prod= '';
				if(isset(get_option( 'nft_marketplace_core_panel_shop_page' )['nft_marketplace_core_items_per_page'])){
				    $nr_prod= get_option( 'nft_marketplace_core_panel_shop_page' )['nft_marketplace_core_items_per_page'];
				} else {
				    $nr_prod= '9';
				}
				if($nft_listing_single_cat) {
					$nfts_query_arg = array(
			            'posts_per_page' => $nr_prod, 
			            'post_type'      => 'nft-listing',
			            'tax_query'        => array(
			                array(
			                    'taxonomy' => 'nft-listing-category',
			                    'field'    => 'id',
			                    'terms'    => $nft_listing_single_cat[0]->term_id
			                )
			            )  
			        );
			        $nfts_query = new WP_Query( $nfts_query_arg );
					echo '<div class="modeltheme-pagination pagination">';             
			            echo paginate_links( array(
			                'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
			                'total'        => $nfts_query->max_num_pages,
			                'current'      => max( 1, get_query_var( 'paged' ) ),
			                'format'       => '?paged=%#%',
			                'show_all'     => false,
			                'type'         => 'plain',
			                'end_size'     => 2,
			                'mid_size'     => 1,
			                'prev_next'    => false,
			                'add_args'     => false,
			                'add_fragment' => '',
			            ) );
			    echo '</div>';
				}
		echo '</div>';
	}

    // COLLECTION NFT : QUERY LISTING COUNT
    // ARCHIVE NFT : CATEGORY SIDEBAR
    function nft_marketplace_core_archive_sidebar_category()
    {
        echo '<div class="nft-marketplace-sidebar-category">';
        	echo '<h3 class="nft-sidebar-title">';
        		echo apply_filters('nft_marketplace_core_category_text', esc_html__('Category', 'nft-marketplace-core'));
        	echo '</h3>';
        	echo '<div class="nft-marketplace-categories">';
        		$terms_c = get_terms(array(
            		'taxonomy' => 'nft-listing-category'
        		));
        		if ($terms_c) {
            		foreach ($terms_c as $term) {
                		if ($term->parent == 0) {
                    		echo '<li>';
                    		echo '<a href="' . get_term_link($term->slug, 'nft-listing-category') . '">' . $term->name . '</a>';
                    		echo '</li>';
                		}		
            		}
        		}
        	echo '</div>';
        echo '</div>';
    }


    // ARCHIVE NFT : QUERY LISTING COUNT
    function nft_marketplace_core_archive_listings_count()
    {
        global $wp_query;
        echo '<div class="nft-marketplace-taxonomy-count col-md-9">';
        $query_page = (get_query_var('paged')) ? get_query_var('paged') : 1;
        $posts_per_page = get_query_var('posts_per_page');
        $last_page = $posts_per_page * $query_page;
        $first_page = $last_page - $posts_per_page + 1;
        $total = $wp_query->found_posts;
        echo esc_html__('Showing ' . esc_attr($first_page) . ' - ' . esc_attr($last_page) . ' of ' . esc_attr($total) . ' results', 'nft-marketplace-core');
        echo '</div>';
    }


    // ARCHIVE NFT : ORDERING
	function nft_marketplace_core_archive_ordering(){
		if(!is_search()) {
			global $wp;
			$location = home_url( $wp->request );
	        echo '<div class="nft-marketplace-taxonomy-ordering col-md-3">';  
	            echo '<form class="nft-marketplace-ordering" method="get">'; ?>
					<select onchange="if(this.value != '') document.location = '<?php echo esc_attr($location); ?>/?orderby=' + this.value">
	  					<option value=""><?php echo esc_html__('Default Sorting'); ?></option>
					  <option value="title"<?php if(isset($_GET['order_by']) && $_GET['order_by'] == 'title') echo ' selected="selected"'; ?>><?php echo esc_html__('Sort by Title','nft-marketplace-core');?></option>
					  <option value="date"<?php if(isset($_GET['order_by']) && $_GET['order_by'] == 'date') echo ' selected="selected"'; ?>><?php echo esc_html__('Sort by Date','nft-marketplace-core');?></option>
					</select><?php
				echo '<input type="hidden" name="paged" value="1">';
			echo '</form>';
	        echo '</div>';
	    }
    }


	// AUTHOR SOCIAL PROFILES//
	function nft_marketplace_core_social_profiles() {
		echo '<div class="author-socials">';
			$author = '';
			if(is_author()) {
				$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
				$nft_marketplace_core_user_facebook = get_user_meta($author->ID,'nft_marketplace_core_user_facebook',true);                     
		        $nft_marketplace_core_user_instagram = get_user_meta($author->ID,'nft_marketplace_core_user_instagram',true);
		        $nft_marketplace_core_user_youtube = get_user_meta($author->ID,'nft_marketplace_core_user_youtube',true);
			} else {
				$author= get_post_field ('post_author', get_the_ID());
				$nft_marketplace_core_user_facebook = get_user_meta($author,'nft_marketplace_core_user_facebook',true);                     
		        $nft_marketplace_core_user_instagram = get_user_meta($author,'nft_marketplace_core_user_instagram',true);
		        $nft_marketplace_core_user_youtube = get_user_meta($author,'nft_marketplace_core_user_youtube',true);
			}
	        echo '<ul class="listed-author-info">';
	        	if(!empty($author->user_url) ) {
	            	echo '<li><a href="'.esc_html($author->user_url).'"><i class="fa fa-globe" aria-hidden="true"></i></a></li>';
	            }
	            if(!empty($nft_marketplace_core_user_facebook)) {
	            	echo '<li><a href="'.esc_url($nft_marketplace_core_user_facebook).'"><i class="fa fa-facebook-square"></i></a></li>';
	            }
	            if(!empty($nft_marketplace_core_user_instagram)) {
	                echo '<li><a href="'.esc_url($nft_marketplace_core_user_instagram).'"><i class="fa fa-instagram" aria-hidden="true"></i></a></li>';
	            }
	            if(!empty($nft_marketplace_core_user_youtube)) {
	                echo '<li><a href="'.esc_url($nft_marketplace_core_user_youtube).'"><i class="fa fa-youtube" aria-hidden="true"></i></a></li>';
	            }
	        echo '</ul>';
	    echo '</div>';
	}


	// AUTHOR LISTING PAGINATION //
	function nft_marketplace_core_author_listings_pagination() {
		echo '<div class="modeltheme-pagination-holder col-md-12">';
			$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
			$nfts_query_arg = array(
	            'post_type' => 'nft-listing', 
	            'author' => $author->ID,
	            'posts_per_page'  => 8,
	        );
	        $nfts_query = new WP_Query( $nfts_query_arg );
			echo '<div class="modeltheme-pagination pagination">';             
	            echo paginate_links( array(
	                'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
	                'total'        => $nfts_query->max_num_pages,
	                'current'      => max( 1, get_query_var( 'paged' ) ),
	                'format'       => '?paged=%#%',
	                'show_all'     => false,
	                'type'         => 'plain',
	                'end_size'     => 2,
	                'mid_size'     => 1,
	                'prev_next'    => false,
	                'add_args'     => false,
	                'add_fragment' => '',
	            ) );
	        echo '</div>';
	    echo '</div>';
	}


	// TEMPLATE NFT LISTING: QUERY LISTING COUNT
    function nft_marketplace_core_archive_listings_count_template() {
    	global $wp_query;
    	$nfts_query_arg = array(
            'post_type' => 'nft-listing',
            'posts_per_page'  => -1,
        );
        $nfts_query = new WP_Query( $nfts_query_arg );
    	echo '<div class="nft-marketplace-taxonomy-count col-md-9">';
	    	$query_page = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
	        $posts_per_page = $nfts_query->found_posts;
	        $last_page = $posts_per_page * $query_page;
	        $first_page = $last_page - $posts_per_page + 1;
	        $total = $nfts_query->found_posts;
	        echo esc_html__('Showing '.esc_attr($first_page).' - '.esc_attr($last_page).' of '.esc_attr($total).' results','nft-marketplace-core');
        echo '</div>';
    }


    // TEMPLATE NFT LISTING: ORDERING
	function nft_marketplace_core_archive_ordering_template(){
		$term = get_queried_object();
		$taxonomy = $term->slug;
        echo '<div class="nft-marketplace-taxonomy-ordering col-md-3">';  
            echo '<form class="nft-marketplace-ordering" method="get">'; ?>
				<select onchange="if(this.value != '') document.location ='?orderby=' + this.value">
  					<option value=""><?php echo esc_html__('Default Sorting'); ?></option>
				  <option value="title"<?php if(isset($_GET['order_by']) && $_GET['order_by'] == 'title') echo ' selected="selected"'; ?>><?php echo esc_html__('Sort by Title','nft-marketplace-core');?></option>
				  <option value="date"<?php if(isset($_GET['order_by']) && $_GET['order_by'] == 'date') echo ' selected="selected"'; ?>><?php echo esc_html__('Sort by Date','nft-marketplace-core');?></option>
				</select><?php
			echo '<input type="hidden" name="paged" value="1">';
		echo '</form>';
        echo '</div>';
    }


    /* SHORTCODE : Update form */
    function nft_marketplace_core_update_form_shortcode($params, $content)
    {
        require_once(ABSPATH . 'wp-admin/includes/image.php');
        $current_user = wp_get_current_user();
        $author = get_user_by('slug', get_query_var('author_name'));
        $html = '';
        if (isset($_POST['add-listing'])) {
            $my_listing = array(
                'ID' => $current_user->ID
            );
            $pid = wp_update_user($my_listing);
            if (isset($_POST['nft_marketplace_core_user_facebook'])) {
                update_user_meta($pid, 'nft_marketplace_core_user_facebook', $_POST['nft_marketplace_core_user_facebook']);
            }
            if (isset($_POST['nft_marketplace_core_user_instagram'])) {
                update_user_meta($pid, 'nft_marketplace_core_user_instagram', $_POST['nft_marketplace_core_user_instagram']);
            }
            if (isset($_POST['nft_marketplace_core_user_youtube'])) {
                update_user_meta($pid, 'nft_marketplace_core_user_youtube', $_POST['nft_marketplace_core_user_youtube']);
            }
            if (isset($_POST['nft_marketplace_core_banner_img'])) {
                $nft_marketplace_core_banner_img_string = $_POST['nft_marketplace_core_banner_img'];
                $nft_marketplace_core_banner_img_array = explode(',', $nft_marketplace_core_banner_img_string);
                $count = 0;
                $gallery_media_links =[];
                foreach ($nft_marketplace_core_banner_img_array as $picture_id) {
                    if ($count == 0) {
                        $image_name = wp_get_attachment_url($picture_id);
                        $filetype = wp_check_filetype(basename($image_name), null);
                        $wp_upload_dir = wp_upload_dir();
                        $attachment = array(
                            'guid' => $wp_upload_dir['url'] . '/' . basename($image_name),
                            'post_mime_type' => $filetype['type'],
                            'post_title' => preg_replace('/\.[^.]+$/', '', basename($image_name)),
                            'post_content' => '',
                            'post_status' => 'inherit'
                        );
                        $attach_id = $picture_id;
                        update_option('option_image', $attach_id);
                        update_user_meta($pid, 'nft_marketplace_core_banner_img', $image_name);
                        $attach_data = wp_generate_attachment_metadata($attach_id, $image_name);
                        wp_update_attachment_metadata($attach_id, $attach_data);
                        set_post_thumbnail($pid, $attach_id);
                    }
                }
                update_user_meta($pid, 'dfiFeatured', $gallery_media_links);
            }
        }
        return $html;
    }


    /* SHORTCODE : Display edit form*/
    function nft_marketplace_core_edit_form_shortcode($params, $content)
    {
        $author = get_user_by('slug', get_query_var('author_name'));
        $html = '';
        $html .= '<div class="form-group">
		            <label for="nft_marketplace_core_user_facebook">' . esc_html__('Facebook link', 'nft-marketplace-core') . '</label>';
        $nft_marketplace_core_user_facebook = get_user_meta($author->ID, 'nft_marketplace_core_user_facebook', true);
        $html .= '<input type="text" class="form-control" name="nft_marketplace_core_user_facebook" value="' . $nft_marketplace_core_user_facebook . '" placeholder="' . $nft_marketplace_core_user_facebook . '">
	    		</div>';
        $html .= '<div class="form-group">
		            <label for="nft_marketplace_core_user_youtube">' . esc_html__('Youtube link', 'nft-marketplace-core') . '</label>';
        $nft_marketplace_core_user_youtube = get_user_meta($author->ID, 'nft_marketplace_core_user_youtube', true);
        $html .= '<input type="text" class="form-control" name="nft_marketplace_core_user_youtube" value="' . $nft_marketplace_core_user_youtube . '" placeholder="' . $nft_marketplace_core_user_youtube . '">
	    		</div>';
        $html .= '<div class="form-group">
	        		<label for="nft_marketplace_core_user_instagram">' . esc_html__('Instagram link', 'nft-marketplace-core') . '</label>';
        $nft_marketplace_core_user_instagram = get_user_meta($author->ID, 'nft_marketplace_core_user_instagram', true);
        $html .= '<input type="text" class="form-control" name="nft_marketplace_core_user_instagram" value="' . $nft_marketplace_core_user_instagram . '" placeholder="' . $nft_marketplace_core_user_instagram . '">
	    		</div>';
        wp_reset_postdata();
        return $html;
    }


    /* SHORTCODE : Display edit form*/
    function nft_marketplace_core_submit_form_shortcode($params, $content)
    {
        $html = '';
        $html .= '<div class="form-group pull-left">
	                    <button type="submit" class="button-listing" name="add-listing" class="btn btn-success">' . esc_html__('Save Changes', 'nft-marketplace-core') . '</button>
	                </div>';
        wp_reset_postdata();
        return $html;
    }


	// SHORTCODE: SIDEBAR SEARCH
	function nft_marketplace_core_archive_search_shortcode($params, $content) {
		$html  = '';
	    $html .= '<div class="nft-marketplace-search-bar">';
	    	$html .= '<h3 class="nft-sidebar-title">';
                $html .=  apply_filters('nft_marketplace_core_search_text', esc_html__('Search', 'nft-marketplace-core'));
            $html .= '</h3>';
	        $html .= '<form role="search" method="get" id="nft_marketplace_core_searchform" autocomplete="off" class="clearfix" action="'.esc_url(get_site_url()).'">';
	            $html .= '<input type="hidden" name="post_type" value="nft-listing">';
	            $html .= '<input placeholder="'.apply_filters('nft_marketplace_core_search_text', esc_html__('Search', 'nft-marketplace-core')).'" type="text" name="s" id="nft_marketplace_core_keyword">';
	            $html .= '<button type="submit" id="nft_marketplace_core_searchsubmit"> <i class="fa fa-search"></i></button>';
	        $html .= '</form>';	                
	    $html .= '<div id="mtkb_datafetch"></div>'; 
	    $html .= '</div>';
	    return $html;
	}


	// SHORTCODE: CATEGORY SIDEBAR
	function nft_marketplace_core_archive_sidebar_category_shortcode($params, $content){
		$html  = '';
        $html .= '<div class="nft-marketplace-sidebar-category">';  
            $html .= '<h3 class="nft-sidebar-title">';
               $html .= apply_filters('nft_marketplace_core_category_text', esc_html__('Category', 'nft-marketplace-core'));
            $html .= '</h3>';
            $html .= '<div class="nft-marketplace-categories">';                           
                $terms_c = get_terms( array( 
    					'taxonomy' => 'nft-listing-category'
				) );		
                if($terms_c){
                    foreach ($terms_c as $term) {
                        if ($term->parent == 0) {
                            $html .= '<li>';
                                $html .= '<a href="'.get_term_link( $term->slug, 'nft-listing-category' ).'">'.$term->name.'</a>';
                            $html .= '</li>';
                        }                   
                    }                                       
            }
            $html .= '</div>';                 
        $html .= '</div>';
        return $html;
    }


    /**
     * Provide data for frontend
     * @return void
     */
    public function isLoggedInWithMetaMask() {
        $isLoggedInWithMetamask = function_exists("mtm_metamask_current_user_has_metamask") && is_user_logged_in() && mtm_metamask_current_user_has_metamask();
        ?>
        <script type="text/javascript">
            window.nftMarketplaceCore = {};
            window.nftMarketplaceCore.isLoggedInWithMetamask = <?php echo var_export($isLoggedInWithMetamask, true) ?>;
        </script>
        <?php
    }
}