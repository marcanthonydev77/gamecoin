<?php


/**
 * The template for single NFT Listing pages.
 */

use BCMathExtended\BC;

get_header();
global $wpdb;
$postID = get_the_ID();
$nft_marketplace_core_nft_listing_price = get_post_meta($postID, 'nft_marketplace_core_nft_listing_price_meta', true);
$nft_marketplace_core_nft_listing_token_id = get_post_meta($postID, 'nft_marketplace_core_nft_listing_token_id', true);
$nft_marketplace_core_nft_listing_address = get_post_meta($postID, 'nft_marketplace_core_nft_listing_address', true);
$nft_marketplace_core_nft_listing_blockchains_term = get_the_terms($postID, 'nft_listing_blockchains');
$nft_marketplace_core_nft_listing_blockchains_term_meta = get_term_meta($nft_marketplace_core_nft_listing_blockchains_term[0]->term_id);
$nft_marketplace_core_nft_listing_marketplace_blockchain_address = $wpdb->get_results("SELECT contract_address FROM " . $wpdb->prefix . "nft_marketplace_core_contracts WHERE taxonomy_blockchain_id = " . esc_sql($nft_marketplace_core_nft_listing_blockchains_term[0]->term_id))[0]->contract_address;
if(isset($_POST["nft_marketplace_core_nft_listing_currency_price"])) {
    update_post_meta($postID, 'nft_marketplace_core_nft_listing_currency_price', $_POST["nft_marketplace_core_nft_listing_currency_price"]);
}
if(isset($_POST["nft_listing_blockchains"])) {
    wp_set_object_terms($postID, esc_textarea($_POST["nft_listing_blockchains"]), 'nft_listing_blockchains');
}
?>

<?php do_action('nft_marketplace_core_breadcrumbs'); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="high-padding">
                <div class="container blog-posts single-nft">
                    <div class="row">
                        <div class="col-md-4 main-content row">
                            <?php $thumbnail_src = wp_get_attachment_image_src(get_post_thumbnail_id(get_the_ID()), 'full'); ?>
                            <?php if ($thumbnail_src) { ?>
                                <img src="<?php echo esc_url($thumbnail_src[0]); ?>"
                                     alt="<?php esc_html_e("No image", "nft-marketplace-core"); ?>">
                            <?php } else { ?>
                                <img src="<?php echo plugin_dir_url(dirname(__DIR__)); ?>includes/images/placeholder.png"
                                     alt="<?php echo esc_html(get_the_title()); ?>">
                            <?php } ?>
                        </div>
                        <div class="col-md-8">
                            <h2 itemprop="name"
                                class="nft_title entry-title"><?php echo esc_html(get_the_title()); ?></h2>
                            <br/>

                            <?php
                            require(plugin_dir_path(dirname(__DIR__))."includes/NFT_Marketplace_Core_Contract_Helper.php");
                            $marketplaceContractRaw = file_get_contents(plugin_dir_path(dirname(__DIR__)) . "/contracts/NFTMarket.json");
                            $nftContractRaw = file_get_contents(plugin_dir_path(dirname(__DIR__)) . "/contracts/NFT.json");

                            $provider = $nft_marketplace_core_nft_listing_blockchains_term_meta["nft_marketplace_core_taxonomy_blockchain_currency_rpc_url"][0];

                            $contractHelper = new NFT_Marketplace_Core_Contract_Helper($provider, $nft_marketplace_core_nft_listing_marketplace_blockchain_address, $marketplaceContractRaw, $nft_marketplace_core_nft_listing_address, $nftContractRaw);
                            if (!$contractHelper->isNFTMinted($nft_marketplace_core_nft_listing_token_id)) {
                                ?>
                                <form id="nft-marketplace-core-mint-nft-form" method="post" data-address="<?php echo esc_attr($nft_marketplace_core_nft_listing_address); ?>" class="form-field form-required term-slug-wrap">
                                    <label for="nft-change-blockchain"
                                    ><?php esc_html_e("Blockchain:", "nft-marketplace-core"); ?></label><br>

                                    <?php $items = get_terms(array(
                                        'taxonomy' => 'nft_listing_blockchains',
                                        'hide_empty' => false,
                                    )); ?>
                                    <select required name="nft_listing_blockchains" id="nft-change-blockchain" class="postform">
                                        <option value="" selected disabled
                                                hidden><?php esc_html_e("Choose here", "nft-marketplace-core"); ?></option>
                                        <?php foreach ($items as $item) : ?>
                                            <option value="<?php echo esc_html($item->slug); ?>"
                                                    data-blockchain="<?php echo esc_attr(json_encode(get_term_meta($item->term_taxonomy_id))); ?>"
                                                    id="term-id-<?php echo esc_attr($item->term_id); ?>"><?php echo esc_html($item->name); ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                    <p class="description"><?php esc_html_e("Select the blockchain where you want to deploy the contract to.", "nft-marketplace-core"); ?></p>
                                <button name="submit-cmb"
                                        type="submit"
                                        class="button-primary single_nft_button button alt">
                                    <?php esc_html_e('Mint NFT', 'nft-marketplace-core'); ?></button>
                                </form>

                                <?php
                            } elseif (!$contractHelper->isItemListed($nft_marketplace_core_nft_listing_token_id) && !$contractHelper->canMarketplaceManageNFT($nft_marketplace_core_nft_listing_token_id)) {
                                ?>
                                <p>
                                    <?php esc_html_e('Before selling your token you need to allow the marketplace to manage (transfer) the said token.', 'nft-marketplace-core'); ?>
                                </p>
                                <button name="submit-cmb" id="approve-nft-marketplace"
                                        class="button-primary ps-0 single_nft_button button alt"
                                        data-address="<?php echo esc_attr($nft_marketplace_core_nft_listing_address); ?>"
                                        data-market-address="<?php echo esc_attr($nft_marketplace_core_nft_listing_marketplace_blockchain_address); ?>"
                                        data-tokenid="<?php echo esc_attr($nft_marketplace_core_nft_listing_token_id); ?>">
                                    <?php esc_html_e('Unlock selling functionality', 'nft-marketplace-core'); ?>
                                </button>

                            <?php } elseif (!$contractHelper->isItemListed($nft_marketplace_core_nft_listing_token_id)) {

                                require(plugin_dir_path(dirname(__DIR__))."includes/NFT_Marketplace_Core_Form.php");
                                $form = new NFT_Marketplace_Core_Form(true);
                                $form->nft_marketplace_core_nft_listing_crypto_prices();
                                ?>

                                <form class="cmb-form" id="nft-marketplace-core-list-nft-form" method="post" enctype="multipart/form-data" encoding="multipart/form-data"
                                      data-tokenid="<?php echo esc_attr($nft_marketplace_core_nft_listing_token_id); ?>"
                                      data-address="<?php echo esc_attr($nft_marketplace_core_nft_listing_address); ?>"
                                      data-market-address="<?php echo esc_attr($nft_marketplace_core_nft_listing_marketplace_blockchain_address); ?>"
                                >
                                    <input type="hidden" name="object_id" value="<?php esc_attr_e($postID); ?>">
                                    <?php
                                    $args = array('form_format' => '%3$s');
                                    cmb2_metabox_form("nft_marketplace_core_nft_listing_crypto_prices_group", $postID, $args);
                                    ?>

                                    <button name="submit-cmb"
                                            type="submit"
                                            class="button-primary single_nft_button button alt">
                                        <?php esc_html_e('List on Marketplace', 'nft-marketplace-core'); ?>
                                    </button>
                                    <br/>
                                <?php esc_html_e("Before listing the product there will be a marketplace fee of", "nft-marketplace-core"); ?>

                                <b>
                                    <?php echo BC::div($contractHelper->getMarketplaceFee()->elem_1, "1000000000000000000", 10); ?>
                                </b>

                                <?php
                                echo " " . esc_html($nft_marketplace_core_nft_listing_blockchains_term_meta["nft_marketplace_core_taxonomy_blockchain_currency_symbol"][0]);
                                ?>

                                </form>

                            <?php }
                            elseif (!$contractHelper->isItemOnSale($nft_marketplace_core_nft_listing_token_id)) {
                            if (!$contractHelper->canMarketplaceManageNFT($nft_marketplace_core_nft_listing_token_id)) {
                                ?>

                                <button name="submit-cmb" id="approve-nft-marketplace"
                                        class="button-primary single_nft_button button alt"
                                        data-address="<?php echo esc_attr($nft_marketplace_core_nft_listing_address); ?>"
                                        data-market-address="<?php echo esc_attr($nft_marketplace_core_nft_listing_marketplace_blockchain_address); ?>"
                                        data-tokenid="<?php echo esc_attr($nft_marketplace_core_nft_listing_token_id); ?>">
                                    <?php esc_html_e('Unlock selling functionality', 'nft-marketplace-core'); ?>
                                </button>

                                <?php
                            } else {
                                require(plugin_dir_path(dirname(__DIR__))."includes/NFT_Marketplace_Core_Form.php");
                                $form = new NFT_Marketplace_Core_Form(true);
                                $form->nft_marketplace_core_nft_listing_crypto_prices();

                                ?>
                            <form class="cmb-form" id="nft-marketplace-core-relist-nft-form" method="post" enctype="multipart/form-data" encoding="multipart/form-data"
                                  data-tokenid="<?php echo esc_attr($nft_marketplace_core_nft_listing_token_id); ?>"
                                  data-address="<?php echo esc_attr($nft_marketplace_core_nft_listing_address); ?>"
                                  data-market-address="<?php echo esc_attr($nft_marketplace_core_nft_listing_marketplace_blockchain_address); ?>"
                            >

                                <input type="hidden" name="object_id" value="'<?php esc_attr($postID) ?>">
                                <?php
                                $args = array('form_format' => '%3$s');
                                cmb2_metabox_form("nft_marketplace_core_nft_listing_crypto_prices_group", $postID, $args);
                                ?>

                                <button name="submit-cmb"
                                        class="button-primary single_nft_button button alt"
                                   ><?php esc_html_e('Resell', 'nft-marketplace-core'); ?></button>
                            <br/>
                            <?php esc_html_e("Before reselling the NFT there will be a marketplace fee of", "nft-marketplace-core"); ?>
                                <b><?php

                                    echo BC::div($contractHelper->getMarketplaceFee()->elem_1, "1000000000000000000", 10);
                                    echo " " . esc_html($nft_marketplace_core_nft_listing_blockchains_term_meta["nft_marketplace_core_taxonomy_blockchain_currency_symbol"][0]);
                                    echo '                                </form>
';
                            }

                            } else {
                                esc_html_e("NFT is listed successfully!", "nft-marketplace-core");
                            } ?>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
<?php

get_footer();
?>