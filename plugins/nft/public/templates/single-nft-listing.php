<?php
/**
 * The template for single NFT Listing pages.
 */
get_header();
global $wpdb;
$postID = get_the_ID();


/**
 * Define single NFT price,symbol,regular price
 */
$nft_marketplace_core_nft_listing_token_id = get_post_meta($postID, 'nft_marketplace_core_nft_listing_token_id', true);
$nft_marketplace_core_nft_listing_address = get_post_meta($postID, 'nft_marketplace_core_nft_listing_address', true);
$nft_marketplace_core_nft_listing_blockchains_term = get_the_terms($postID, 'nft_listing_blockchains');
$nft_marketplace_core_nft_listing_price = get_post_meta($postID, 'nft_marketplace_core_nft_listing_currency_price', true);
$nft_marketplace_core_nft_listing_usd_price = null;
require(plugin_dir_path(dirname(__DIR__))."includes/NFT_Marketplace_Core_Contract_Helper.php");
$marketplaceContractRaw = file_get_contents(plugin_dir_path(dirname(__DIR__)). "/contracts/NFTMarket.json");
$nftContractRaw = file_get_contents(plugin_dir_path(dirname(__DIR__)). "/contracts/NFT.json");
$author = get_post( $postID)->post_author;

if(!empty($nft_marketplace_core_nft_listing_blockchains_term)) {
    $nft_marketplace_core_nft_listing_blockchains_term_meta = get_term_meta($nft_marketplace_core_nft_listing_blockchains_term[0]->term_id);
    $nft_marketplace_core_nft_listing_marketplace_blockchain_address =$wpdb->get_results("SELECT contract_address FROM ".$wpdb->prefix."nft_marketplace_core_contracts WHERE taxonomy_blockchain_id = ".esc_sql($nft_marketplace_core_nft_listing_blockchains_term[0]->term_id) )[0]->contract_address;
    $nft_marketplace_core_nft_listing_symbol = $nft_marketplace_core_nft_listing_blockchains_term_meta["nft_marketplace_core_taxonomy_blockchain_currency_symbol"][0];
    $provider = $nft_marketplace_core_nft_listing_blockchains_term_meta["nft_marketplace_core_taxonomy_blockchain_currency_rpc_url"][0];
    $contractHelper = new NFT_Marketplace_Core_Contract_Helper($provider, $nft_marketplace_core_nft_listing_marketplace_blockchain_address, $marketplaceContractRaw, $nft_marketplace_core_nft_listing_address, $nftContractRaw);
}


/**
 * Main Content: Breadcrumbs
 */
do_action('nft_marketplace_core_before_main_content');


/**
 * Main Content:Start of Page
 */?>
<div id="primary" class="content-area">
    <main id="main" class="site-main" role="main">
        <div class="high-padding">
            <div class="container blog-posts single-nft">
                <div class="row">
                    <div class="col-md-12 main-content row">
                        <div class="col-md-6 nft-thumbnails">
                            <?php $thumbnail_src = wp_get_attachment_image_src( get_post_thumbnail_id( get_the_ID() ), 'large' ); ?>
                            <?php if($thumbnail_src) { ?>
                                <img src="<?php echo esc_url($thumbnail_src[0]); ?>">
                            <?php } ?>
                        </div>
                        <div class="summary nft-entry-summary col-md-6">

                            <?php //Wishlist button // ?>
                            <div class="nft-listing-infos-wrapper">

                                <?php 
                                /**
                                 * Hook: Before Title (Set: Author, Likes, Views)
                                 */
                                do_action('nft_marketplace_core_single_nft_before_title'); ?>

                            </div>
                            <h2 itemprop="name" class="nft_title entry-title"><?php echo esc_html(get_the_title()); ?></h2>
                            <?php // Add edit button for the author
                                if(get_current_user_id() === (int) $author) {
                            ?>
                                <a href="<?php echo get_permalink(); ?>?edit-nft=true" class="single_nft_button button alt" ><?php esc_html_e("Edit NFT", "nft-marketplace-core")?></a>
                                <a href="<?php echo get_permalink(); ?>?list-nft=true" class="single_nft_button button alt" ><?php esc_html_e("List NFT", "nft-marketplace-core")?></a>
                            <?php } ?>
                            <div class="single-collection-title">

                                <?php 
                                /**
                                 * Hook: After Title (Set Collection name)
                                 */
                                ?>
                                <?php do_action('nft_marketplace_core_single_nft_after_title'); ?>

                                <span><?php echo apply_filters('nft_marketplace_core_collection_title', esc_html__('Collection', 'nft-marketplace-core')); ?></span>
                            </div>

                            <div class="nft-listing-crypto-price">

                                <?php
                                if(!empty($nft_marketplace_core_nft_listing_blockchains_term) && !empty($nft_marketplace_core_nft_listing_price)) {
                                    echo wp_get_attachment_image($nft_marketplace_core_nft_listing_blockchains_term_meta["nft_marketplace_core_taxonomy_blockchain_currency_image"][0],[30,30]);
                                }
                                ?>

                                <?php if(!empty($nft_marketplace_core_nft_listing_price)) { ?>
                                    <?php if(!empty($nft_marketplace_core_nft_listing_blockchains_term)){ ?>
                                        <b class="nft-importer-price"><?php echo esc_html($nft_marketplace_core_nft_listing_price).' '. esc_html($nft_marketplace_core_nft_listing_symbol); ?></b>
                                    <?php } else { ?>
                                        <b class="nft-importer-price"><?php echo esc_html($nft_marketplace_core_nft_listing_price) ?></b>
                                    <?php } ?>
                                    <?php if (!empty($nft_marketplace_core_nft_listing_usd_price)) { ?>
                                        <?php if(get_option( 'nft_marketplace_core_panel_single_nft' )['nft_marketplace_core_price_usd'] == 'on' && isset(get_option( 'nft_marketplace_core_panel_single_nft' )['nft_marketplace_core_price_usd']) ) { ?>
                                            <span class="nft-listing-crypto-usd"><?php echo esc_html($nft_marketplace_core_nft_listing_usd_price); ?></span>
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>

                                </div>

                            <?php // Collection Block // ?>
                            <?php
                            if(!empty($nft_marketplace_core_nft_listing_blockchains_term)) {
                                $isItemOnSale = $contractHelper->isItemOnSale($nft_marketplace_core_nft_listing_token_id);
                                do_action('nft_marketplace_core_before_button', $isItemOnSale);
                                $curUser = get_current_user_id();

                                $results = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."nft_marketplace_core_transactions WHERE customer = $curUser  AND seller = $author AND nft_id = $postID AND finished = 0 ORDER BY registered_date ASC LIMIT 1", ARRAY_A);
                                if(count($results) > 0) {
                                    if ($contractHelper->didTransactionSucceed($results[0]["transaction_data"])) {
                                        $arg = array(
                                            'ID' => $postID,
                                            'post_author' => get_current_user_id(),
                                        );

                                        wp_update_post($arg);

                                        $wpdb->update($wpdb->prefix . "nft_marketplace_core_transactions", [
                                            "finished" => 1
                                        ], [
                                            "nft_id" => $postID,
                                            "transaction_data" => $results[0]["transaction_data"],
                                            "customer" => get_current_user_id(),
                                            "seller" => $author
                                        ]);
                                    }
                                }
                                if(isset($_POST["nft_marketplace_core_buy_data"])) {
                                    $transaction_data =  json_decode(stripslashes($_POST["nft_marketplace_core_buy_data"]), true)["transactionHash"];

                                    $results = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."nft_marketplace_core_transactions WHERE transaction_data = '$transaction_data'", ARRAY_A);
                                    if($results < 1) {
                                        $c = $wpdb->insert($wpdb->prefix . "nft_marketplace_core_transactions", [
                                            "nft_id" => $postID,
                                            "transaction_data" => $transaction_data,
                                            "transaction_data_raw" => esc_js($_POST["nft_marketplace_core_buy_data"]),
                                            "price" => $nft_marketplace_core_nft_listing_price,
                                            "customer" => get_current_user_id(),
                                            "seller" => $author,
                                            "finished" => 0,
                                        ]);
                                    }
                                }
                                if($isItemOnSale) {
                                ?>
                                    <form method="post" id="nft-marketplace-core-buy-nft-form"  data-tokenid="<?php esc_attr_e($nft_marketplace_core_nft_listing_token_id); ?>"
                                          data-address="<?php esc_attr_e($nft_marketplace_core_nft_listing_address); ?>"
                                          data-marketplace-address="<?php esc_attr_e($nft_marketplace_core_nft_listing_marketplace_blockchain_address); ?>"
                                          data-price="<?php esc_attr_e($nft_marketplace_core_nft_listing_price); ?>"
                                          data-force-switch-to="<?php esc_attr_e(json_encode($nft_marketplace_core_nft_listing_blockchains_term_meta,JSON_UNESCAPED_SLASHES)); ?>">
                                        <button type="submit" class="single_nft_button button alt">
                                            <?php echo apply_filters('nft_marketplace_core_purchase_btn', esc_html__('Purchase Now', 'nft-marketplace-core')); ?>
                                        </button>
                                    </form>

                                <?php } // Metas // ?>
                                <?php do_action('nft_marketplace_core_after_button', $isItemOnSale); ?>
                                <?php
                                /**
                                 * Hook: After Metas (Set Tabs Aside)
                                 */
                                ?>
                            <?php }
                                  if(!isset(get_option( 'nft_marketplace_core_panel_single_nft' )['nft_marketplace_core_tabs']) || get_option( 'nft_marketplace_core_panel_single_nft' )['nft_marketplace_core_tabs'] == 'top') { ?>
                                <?php do_action('nft_marketplace_core_single_nft_after_metas'); ?>
                            <?php } ?>
                        </div>
                    </div>
                </div>

                <?php 
                /**
                * Hook: After Metas (Set Tabs Below)
                */
                ?>
                <?php if(isset(get_option( 'nft_marketplace_core_panel_single_nft' )['nft_marketplace_core_tabs']) && get_option( 'nft_marketplace_core_panel_single_nft' )['nft_marketplace_core_tabs'] == 'bellow') { ?>
                    <?php do_action('nft_marketplace_core_single_nft_before_related'); ?>
                <?php } ?>

                <?php 
                /**
                 * Hook: Related NFT after content
                 */
                ?>
                <?php do_action('nft_marketplace_core_single_nft_related'); ?>

            </div>
        </div>
    </main>
</div>
<?php 
/**
 * Main Content: End of page
 */
 get_footer(); ?>
