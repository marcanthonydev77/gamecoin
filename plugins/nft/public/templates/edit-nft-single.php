<?php
/**
 * The template for single NFT Listing pages.
 */
get_header();
$nft_marketplace_core_nft_listing_price = get_post_meta(get_the_ID(), 'nft_marketplace_core_nft_listing_price_meta', true);
$nft_marketplace_core_nft_listing_description_meta = get_post_meta(get_the_ID(), 'nft_marketplace_core_nft_listing_description_meta', true);

// Redirect to 404 if user is not author
if(get_current_user_id() !== (int) get_post(get_the_ID())->post_author) {
    global $wp_query;
    $wp_query->set_404();
    status_header( 404 );
    get_template_part( 404 ); exit();
}
$post_to_edit = get_post(get_the_ID());

?>

<?php do_action('nft_marketplace_core_breadcrumbs'); ?>

    <div id="primary" class="content-area">
        <main id="main" class="site-main" role="main">
            <div class="high-padding">
                <div class="container blog-posts single-nft">
                    <div class="row">
                        <div class="col-md-12 main-content row">

                            <?php
                            require(plugin_dir_path(dirname(__DIR__))."includes/NFT_Marketplace_Core_Form.php");
                            $form = new NFT_Marketplace_Core_Form(true);
                            $form->nft_marketplace_core_render_form(get_the_ID());
                            ?>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
<?php

get_footer();
?>