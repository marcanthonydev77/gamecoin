<?php
/**
 * The template for displaying archive pages.
 *
 */
get_header(); 

$current_user = wp_get_current_user();
$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
$author_edit_link = home_url($wp->request).'/?profile-edit='.$current_user->ID;
$author_link = home_url($wp->request);

//Update Form
do_shortcode('[nft_marketplace_core_update_author_form_shortcode]');

if (isset($_GET['profile-edit']) && !empty($_GET['profile-edit'])) {
    $user = get_user_by( 'slug', $_GET['profile-edit'] );
    
    if($current_user->ID == $author->ID) {
      $nft_marketplace_core_banner_img = get_user_meta($author->ID,'nft_marketplace_core_banner_img',true); ?>

      <div class="author-fullwidth-banner" style="<?php if($nft_marketplace_core_banner_img){?>background-image:url(<?php echo esc_url($nft_marketplace_core_banner_img); ?>);<?php }else { ?>background:#ddd;<?php } ?>"></div>

      <div class="high-padding nft-marketplace-core-author">
        <div class="container">
            <div class="row">
                <div class="author-details-wrapper col-md-12">
                    <div class="author-title-image">
                        <div class="author-avatar">
                            <?php echo get_avatar( $author->ID, 96); ?>
                        </div>
                    </div>
                    <div class="author-edit-title">
                        <h2>
                            <?php echo esc_html__('Edit Profile Information', 'nft-marketplace-core');?>
                        </h2>
                        <p><a href="<?php echo esc_url($author_link); ?>"><?php echo esc_html__('Go back','nft-marketplace-core');?></a></p>
                    </div> 
                    <div class="author-edit-fields-wrapper">
                        <form id="add-new-listing" class="add-new-listing-form" method="POST" enctype="multipart/form-data">
                            <?php echo do_shortcode('[nft_marketplace_core_edit_author_form_shortcode]'); ?>
                            <?php echo do_shortcode('[frontend-button]'); ?>
                            <?php echo do_shortcode('[nft_marketplace_core_submit_author_form_shortcode]'); ?>
                        </form>

                        <?php
                            if(function_exists("mtm_auth_app_instance_link")) {
                                mtm_auth_app_instance_link();
                            }
                        ?>

                    </div>
                </div>
            </div>      
        </div>
    </div>
  <?php } else { ?>
    <div class="high-padding">
      <div class="container blog-posts">
        <div class="col-md-12"> 
          <?php get_template_part( 'content', 'none' ); ?>
        </div>
      </div>
    </div>
  <?php } ?>

<?php }else{ 

$author = get_user_by( 'slug', get_query_var( 'author_name' ) );
$nft_marketplace_core_banner_img = get_user_meta($author->ID,'nft_marketplace_core_banner_img',true);
?>

<div class="author-fullwidth-banner" style="<?php if($nft_marketplace_core_banner_img){?>background-image:url(<?php echo esc_url($nft_marketplace_core_banner_img); ?>);<?php }else { ?>background:#ddd;<?php } ?>"></div>
<div class="high-padding nft-marketplace-core-author">
    <div class="container">
        <div class="row">
            <div class="author-details-wrapper col-md-12">
                <div class="author-title-image">
                    <div class="author-avatar">
                        <?php echo get_avatar( $author->ID, 96);?>
                    </div>
                    <div class="author-name">
                        <h3><?php echo esc_html($author->display_name); ?></h3>

                        <?php if($current_user->user_nicename == $author->user_nicename) { ?>
                            <p><a class="btn-edit" href="<?php echo esc_url($author_edit_link); ?>"><?php echo esc_html__('Edit Profile','nft-marketplace-core'); ?></a></p>
                        <?php } ?>
                    </div>
                    <div class="author-description">
                        <?php if(function_exists('mtm_metamask_current_user_has_metamask') && mtm_metamask_current_user_has_metamask()) {?>
                            <p class="author-eth-address"><img src="<?php echo nft_marketplace_core_get_url(); ?>includes/images/token.svg" alt="Ether" width="10" height="10">
                                <?php
                                global $wpdb;
                                $tablename = $wpdb->prefix.'metamask_accounts';
                                $row = $wpdb->get_row( "SELECT * FROM  $tablename WHERE `user_id` = '$current_user->ID' AND account_type = 'metamask' ",ARRAY_A );
                                echo esc_html($row["account_address"]);
                                ?>
                            </p>
                        <?php } ?>
                        <?php if(!empty($author->user_description)) {?>
                            <p><?php echo esc_html($author->user_description ); ?></p>
                        <?php } ?>
                    </div>
                </div>

                <?php do_action('nft_marketplace_core_author_header_right'); ?>

            </div>

            <div class="author-listing-wrapper">
                <div class="author-listings">
                    <?php  
                    $nfts_query_arg = array(
                        'post_type' => 'nft-listing', 
                        'author' => $author->ID,
                        'posts_per_page'  => 8,
                    );
                    $nfts_query = new WP_Query( $nfts_query_arg );

                    if( $nfts_query->have_posts() ) : ?>
                        <?php while ( $nfts_query->have_posts() ) : $nfts_query->the_post(); ?>
                            <article id="post-<?php the_ID(); ?>" class="nft-listing col-md-3">
                                <?php do_action('nft_marketplace_core_author_listing_query'); ?>
                            </article>
                        <?php endwhile; ?>
                        <?php wp_reset_postdata(); ?>
                        
                        <div class="clearfix"></div>
            
                        <?php do_action('nft_marketplace_core_author_after_grid'); ?>

                    <?php else : ?>
                        <strong><?php echo esc_html__('There are no NFT Listings!','nft-marketplace-core') ?></strong>
                    <?php endif; ?>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>
<?php get_footer(); ?>