<?php
/**
 * The admin-specific functionality of the plugin.
 *
 * @link       https://modeltheme.com/
 * @since      1.0.0
 *
 * @package    NFT Marketplace Core
 * @subpackage NFT Marketplace Core/admin
 */
/**
 * The admin-specific functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the admin-specific stylesheet and JavaScript.
 *
 * @package    NFT Marketplace Core
 * @subpackage NFT Marketplace Core/admin
 * @author     ModelTheme <support@modeltheme.com>
 */

class NFT_Marketplace_Core_Admin
{
    /**
     * The ID of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $plugin_name The ID of this plugin.
     */
    private $plugin_name;
    /**
     * The version of this plugin.
     *
     * @since    1.0.0
     * @access   private
     * @var      string $version The current version of this plugin.
     */
    private $version;
    /**
     * Initialize the class and set its properties.
     *
     * @param string $plugin_name The name of this plugin.
     * @param string $version The version of this plugin.
     * @since    1.0.0
     */
    public function __construct($plugin_name, $version)
    {
        $this->plugin_name = $plugin_name;
        $this->version = $version;
    }
    /**
     * Register the stylesheets for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_styles()
    {
        wp_enqueue_style( $this->plugin_name."-alerts", plugin_dir_url( __DIR__ ) . 'public/css/nft-marketplace-core-alerts.css', array(), $this->version, 'all' );
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/nft-marketplace-core-admin.css', array(), $this->version, 'all');
        wp_enqueue_style($this->plugin_name, plugin_dir_url(__FILE__) . 'css/nft-marketplace-core-panel.css', array(), $this->version, 'all');
    }
    /**
     * Register the JavaScript for the admin area.
     *
     * @since    1.0.0
     */
    public function enqueue_scripts()
    {
        /**
         * This function is provided for demonstration purposes only.
         *
         * An instance of this class should be passed to the run() function
         * defined in nft_marketplace_core_Loader as all of the hooks are defined
         * in that particular class.
         *
         * The nft_marketplace_core_Loader will then create the relationship
         * between the defined hooks and the functions defined in this
         * class.
         */
        wp_enqueue_script($this->plugin_name, plugin_dir_url(__FILE__) . 'js/nft-marketplace-core-admin.js', array('jquery'), $this->version, false);
        wp_enqueue_script( $this->plugin_name."-market", plugin_dir_url( __DIR__ ) . 'public/js/nft-marketplace-core-market.js', array( 'wp-i18n' ), $this->version, true );
        wp_set_script_translations( $this->plugin_name."-market", 'nft-marketplace-core' );
    }


    /**
     *
     * ||-> CPT - [NFT Listing]
     */
    function nft_marketplace_core_register_nft_listing_custom_post()
    {
        register_post_type('nft-listing', array(
                'label' => esc_html__('NFT Listings', 'nft-marketplace-core'),
                'description' => '',
                'public' => true,
                'show_ui' => true,
                'show_in_menu' => true,
                'capability_type' => 'post',
                'map_meta_cap' => true,
                'hierarchical' => false,
                'publicly_queryable' => true,
                'rewrite' => array('slug' => 'nft', 'with_front' => true),
                'query_var' => true,
                'menu_position' => '1',
                'menu_icon' => plugin_dir_url( __FILE__ ).'/images/nmc-logo.svg',
                'supports' => array('title', 'thumbnail', 'author'),
                'labels' => array(
                    'name' => esc_html__('NFT Listings', 'nft-marketplace-core'),
                    'singular_name' => esc_html__('NFT Listings', 'nft-marketplace-core'),
                    'all_items' => esc_html__('NFT Listings', 'nft-marketplace-core'),
                    'menu_name' => esc_html__('NFT Marketplace Core', 'nft-marketplace-core'),
                    'add_new' => esc_html__('Add NFT', 'nft-marketplace-core'),
                    'add_new_item' => esc_html__('Add New NFT', 'nft-marketplace-core'),
                    'edit' => esc_html__('Edit', 'nft-marketplace-core'),
                    'edit_item' => esc_html__('Edit NFT', 'nft-marketplace-core'),
                    'new_item' => esc_html__('New NFT', 'nft-marketplace-core'),
                    'view' => esc_html__('View NFT', 'nft-marketplace-core'),
                    'view_item' => esc_html__('View NFT', 'nft-marketplace-core'),
                    'search_items' => esc_html__('Search NFT', 'nft-marketplace-core'),
                    'not_found' => esc_html__('No NFT Found', 'nft-marketplace-core'),
                    'not_found_in_trash' => esc_html__('No NFT Found in Trash', 'nft-marketplace-core'),
                    'parent' => esc_html__('Parent NFT', 'nft-marketplace-core'),
                )
            )
        );
    }


    /**
     *
     * ||-> CPT - [NFT Listing] Taxonomy category
     */
    public function nft_marketplace_core_register_nft_listing_category()
    {
        $labels = array(
            'name' => esc_html_x('Collections', 'Taxonomy General Name', 'nft-marketplace-core'),
            'singular_name' => esc_html_x('Collections', 'Taxonomy Singular Name', 'nft-marketplace-core'),
            'menu_name' => esc_html__('Collections', 'nft-marketplace-core'),
            'all_items' => esc_html__('All Items', 'nft-marketplace-core'),
            'parent_item' => esc_html__('Parent Item', 'nft-marketplace-core'),
            'parent_item_colon' => esc_html__('Parent Item:', 'nft-marketplace-core'),
            'new_item_name' => esc_html__('New Item Name', 'nft-marketplace-core'),
            'add_new_item' => esc_html__('Add New Item', 'nft-marketplace-core'),
            'edit_item' => esc_html__('Edit Item', 'nft-marketplace-core'),
            'update_item' => esc_html__('Update Item', 'nft-marketplace-core'),
            'view_item' => esc_html__('View Item', 'nft-marketplace-core'),
            'separate_items_with_commas' => esc_html__('Separate items with commas', 'nft-marketplace-core'),
            'add_or_remove_items' => esc_html__('Add or remove items', 'nft-marketplace-core'),
            'choose_from_most_used' => esc_html__('Choose from the most used', 'nft-marketplace-core'),
            'popular_items' => esc_html__('Popular Items', 'nft-marketplace-core'),
            'search_items' => esc_html__('Search Items', 'nft-marketplace-core'),
            'not_found' => esc_html__('Not Found', 'nft-marketplace-core'),
        );
        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'collection'),
            'public' => true,
        );
        register_taxonomy('nft-listing-category', array('nft-listing'), $args);
    }


    /**
     *
     * ||-> CPT - [NFT Listing] Taxonomy for blockchains
     */
    public function nft_marketplace_core_register_nft_listing_blockchains()
    {
        $labels = array(
            'name' => esc_html_x('Blockchains', 'Taxonomy General Name', 'nft-marketplace-core'),
            'singular_name' => esc_html_x('Blockchain', 'Taxonomy Singular Name', 'nft-marketplace-core'),
            'menu_name' => esc_html__('Blockchains', 'nft-marketplace-core'),
            'all_items' => esc_html__('All Networks', 'nft-marketplace-core'),
            'parent_item' => esc_html__('Parent Item', 'nft-marketplace-core'),
            'parent_item_colon' => esc_html__('Parent Item:', 'nft-marketplace-core'),
            'new_item_name' => esc_html__('New Item Name', 'nft-marketplace-core'),
            'add_new_item' => esc_html__('Add New Network', 'nft-marketplace-core'),
            'edit_item' => esc_html__('Edit Network', 'nft-marketplace-core'),
            'update_item' => esc_html__('Update Network', 'nft-marketplace-core'),
            'view_item' => esc_html__('View Networks', 'nft-marketplace-core'),
            'separate_items_with_commas' => esc_html__('Separate items with commas', 'nft-marketplace-core'),
            'add_or_remove_items' => esc_html__('Add or remove items', 'nft-marketplace-core'),
            'choose_from_most_used' => esc_html__('Choose from the most used', 'nft-marketplace-core'),
            'popular_items' => esc_html__('Popular Items', 'nft-marketplace-core'),
            'search_items' => esc_html__('Search Items', 'nft-marketplace-core'),
            'not_found' => esc_html__('Not Found', 'nft-marketplace-core'),
        );
        $args = array(
            'hierarchical' => true,
            'labels' => $labels,
            'show_ui' => true,
            'show_admin_column' => true,
            'show_in_nav_menus' => true,
            'query_var' => true,
            'rewrite' => array('slug' => 'blockchain'),
            'public' => true,
        );
        register_taxonomy('nft_listing_blockchains', array('nft-listing'), $args);
    }


    function nft_marketplace_core_bltx_add_term_fields($term)
    {
        echo '
            <div class="form-field form-required term-slug-wrap">
                <label for="nft_marketplace_core_taxonomy_blockchain_currency_symbol">'.esc_html__("Symbol", "nft-marketplace-core").'</label>
                <input required name="nft_marketplace_core_taxonomy_blockchain_currency_symbol" id="nft_marketplace_core_taxonomy_blockchain_currency_symbol" type="text" value="" />
                <p class="description">'.esc_html__("Ex: ETH, MATIC etc.", "nft-marketplace-core").'</p>
            </div>
            <div class="form-field form-required term-slug-wrap">
                <label for="nft_marketplace_core_taxonomy_blockchain_currency_chainid">'.esc_html__("Chain ID", "nft-marketplace-core").'</label>
                <input required name="nft_marketplace_core_taxonomy_blockchain_currency_chainid" id="nft_marketplace_core_taxonomy_blockchain_currency_chainid" type="text" value="" />
                <p class="description">'.esc_html__("The value will be automatically converted to hex.","nft-marketplace-core").'</p>
            </div>
            <div class="form-field form-required term-slug-wrap">
                <label for="nft_marketplace_core_taxonomy_blockchain_currency_rpc_url">'.esc_html__("RPC Url", "nft-marketplace-core").'</label>
                <input required name="nft_marketplace_core_taxonomy_blockchain_currency_rpc_url" id="nft_marketplace_core_taxonomy_blockchain_currency_rpc_url" type="text" value="" />
                <p class="description"></p>
            </div>
            <div class="form-field form-required term-slug-wrap">
                <label for="nft_marketplace_core_taxonomy_blockchain_currency_image">'.esc_html__("Image", "nft-marketplace-core").'</label>
                 <input required aria-required="true" type="hidden" required id="nft_marketplace_core_taxonomy_blockchain_currency_image" name="nft_marketplace_core_taxonomy_blockchain_currency_image" class="custom_media_url" value="">
                 <div id="category-image-wrapper"></div>
                 <p>
                   <input type="button" class="button button-secondary ct_tax_media_button" id="ct_tax_blockchain_media_add" name="ct_tax_media_button" value="Add" />
                   <input type="button" class="button button-secondary ct_tax_media_remove" id="ct_tax_blockchain_media_remove" name="ct_tax_media_remove" value="Remove" />
                </p>
            </div>
           <ul>
           <p class="description">
           '.esc_html__("Learn how to add more networks on the websites below (skip to point 3):", "nft-marketplace-core").'
            </p>
                <li><a href="https://autofarm.gitbook.io/autofarm-network/how-tos/binance-smart-chain-bsc/metamask-add-binance-smart-chain-bsc-network">'.esc_html__("BSC", "nft-marketplace-core").'</a></li>
                <li><a href="https://autofarm.gitbook.io/autofarm-network/how-tos/polygon-chain-matic/metamask-add-polygon-matic-network">'.esc_html__("MATIC (Polygon)", "nft-marketplace-core").'</a></li>
            </ul>
        ';
    }


    function nft_marketplace_core_bltx_edit_term_fields($term) { ?>
        <tr class="form-field term-group-wrap">
            <th scope="row">
                <label for="nft_marketplace_core_taxonomy_blockchain_currency_symbol"><?php esc_html_e( 'Currency Symbol', 'nft-marketplace-core' ); ?></label>
            </th>
            <td>
                 <?php $symbol = get_term_meta ( $term -> term_id, 'nft_marketplace_core_taxonomy_blockchain_currency_symbol', true ); ?>
                 <input aria-required="true" name="nft_marketplace_core_taxonomy_blockchain_currency_symbol" id="nft_marketplace_core_taxonomy_blockchain_currency_symbol" type="text" value="<?php echo esc_attr($symbol) ?>" />
                 <p class="description"><?php echo esc_html__("Ex: ETH, MATIC etc.", "nft-marketplace-core"); ?></p>
            </td>
        </tr>
        <tr class="form-field term-group-wrap">
            <th scope="row">
                <label for="nft_marketplace_core_taxonomy_blockchain_currency_chainid"><?php esc_html_e( 'Chain ID', 'nft-marketplace-core' ); ?></label>
            </th>
            <td>
                <?php $symbol = get_term_meta ( $term -> term_id, 'nft_marketplace_core_taxonomy_blockchain_currency_chainid', true ); ?>
                <input aria-required="true" name="nft_marketplace_core_taxonomy_blockchain_currency_chainid" id="nft_marketplace_core_taxonomy_blockchain_currency_chainid" type="text" value="<?php echo esc_attr($symbol) ?>" />
                <p class="description"><?php esc_html_e("The value will be automatically converted to the right format (hex).","nft-marketplace-core"); ?></p>
            </td>
        </tr>
        <tr class="form-field term-group-wrap">
            <th scope="row">
                <label for="nft_marketplace_core_taxonomy_blockchain_currency_rpc_url"><?php esc_html_e( 'RPC Url', 'nft-marketplace-core' ); ?></label>
            </th>
            <td>
                <?php $symbol = get_term_meta ( $term -> term_id, 'nft_marketplace_core_taxonomy_blockchain_currency_rpc_url', true ); ?>
                <input aria-required="true" name="nft_marketplace_core_taxonomy_blockchain_currency_rpc_url" id="nft_marketplace_core_taxonomy_blockchain_currency_rpc_url" type="text" value="<?php echo esc_attr($symbol) ?>" />
            </td>
        </tr>
        <tr class="form-field term-group-wrap">
            <th scope="row">
                <label for="nft_marketplace_core_taxonomy_blockchain_currency_image"><?php esc_html_e( 'Currency Image', 'nft-marketplace-core' ); ?></label>
            </th>
            <td>
                <?php $image_id = get_term_meta ( $term -> term_id, 'nft_marketplace_core_taxonomy_blockchain_currency_image', true ); ?>
                <input type="hidden" id="nft_marketplace_core_taxonomy_blockchain_currency_image" name="nft_marketplace_core_taxonomy_blockchain_currency_image" value="<?php echo esc_attr($image_id); ?>">
                <div id="category-image-wrapper">
                    <?php if ( $image_id ) { ?>
                        <?php echo wp_get_attachment_image ( $image_id, 'thumbnail' ); ?>
                    <?php } ?>
                </div>
                <p>
                    <input type="button" class="button button-secondary ct_tax_media_button" id="ct_tax_media_button" name="ct_tax_media_button" value="<?php esc_html_e( 'Add Image', 'nft-marketplace-core' ); ?>" />
                    <input type="button" class="button button-secondary ct_tax_media_remove" id="ct_tax_media_remove" name="ct_tax_media_remove" value="<?php esc_html_e( 'Remove Image', 'nft-marketplace-core' ); ?>" />
                </p>
            </td>
        </tr>
        <?php
    }


    function nft_marketplace_core_bltx_save_term_fields($term_id)
    {
        add_term_meta(
            $term_id,
            'nft_marketplace_core_taxonomy_blockchain_currency_rpc_url',
            sanitize_text_field($_POST['nft_marketplace_core_taxonomy_blockchain_currency_rpc_url'])
        );
        add_term_meta(
            $term_id,
            'nft_marketplace_core_taxonomy_blockchain_currency_symbol',
            sanitize_text_field($_POST['nft_marketplace_core_taxonomy_blockchain_currency_symbol'])
        );
        add_term_meta(
            $term_id,
            'nft_marketplace_core_taxonomy_blockchain_currency_chainid',
            sanitize_text_field('0x'.dechex($_POST['nft_marketplace_core_taxonomy_blockchain_currency_chainid']))
        );
        if( isset( $_POST['nft_marketplace_core_taxonomy_blockchain_currency_image'] ) && '' !== $_POST['nft_marketplace_core_taxonomy_blockchain_currency_image'] ){
            $image = $_POST['nft_marketplace_core_taxonomy_blockchain_currency_image'];
            add_term_meta( $term_id, 'nft_marketplace_core_taxonomy_blockchain_currency_image', $image, true );
        }
    }


    function nft_marketplace_core_bltx_update_term_fields($term_id)
    {
        update_term_meta(
            $term_id,
            'nft_marketplace_core_taxonomy_blockchain_currency_rpc_url',
            sanitize_text_field($_POST['nft_marketplace_core_taxonomy_blockchain_currency_rpc_url'])
        );
        update_term_meta(
            $term_id,
            'nft_marketplace_core_taxonomy_blockchain_currency_chainid',
            sanitize_text_field('0x'.dechex($_POST['nft_marketplace_core_taxonomy_blockchain_currency_chainid']))
        );
        update_term_meta(
            $term_id,
            'nft_marketplace_core_taxonomy_blockchain_currency_symbol',
            sanitize_text_field($_POST['nft_marketplace_core_taxonomy_blockchain_currency_symbol'])
        );
        if( isset( $_POST['nft_marketplace_core_taxonomy_blockchain_currency_image'] ) && '' !== $_POST['nft_marketplace_core_taxonomy_blockchain_currency_image'] ){
            $image = $_POST['nft_marketplace_core_taxonomy_blockchain_currency_image'];
            update_term_meta ( $term_id, 'nft_marketplace_core_taxonomy_blockchain_currency_image', $image );
        } else {
            update_term_meta ( $term_id, 'nft_marketplace_core_taxonomy_blockchain_currency_image', '' );
        }
    }


    public function add_submenu_page_for_contract_management_page() {
        $page = add_submenu_page(
            "edit.php?post_type=nft-listing",
            esc_html__( 'Manage Contracts', 'nft-marketplace-core' ),
            esc_html__( 'Manage Contracts', 'nft-marketplace-core' ),
            "manage_options",
            "nft-marketplace-core-panel-contracts",
            array(
                $this,
                'render_admin_contract_management_callback',
            )
        );
    }


    public function render_admin_contract_management_callback()
    {
        require( plugin_dir_path(__DIR__). "admin/templates/marketplace-modify-contract.php");
    }


    public function nft_marketplace_core_bltx_add_script() { ?>
        <script>
            jQuery(document).ready( function($) {
                "use strict";
                function ct_media_upload(button_class) {
                    var _custom_media = true,
                        _orig_send_attachment = wp.media.editor.send.attachment;
                    $('body').on('click', button_class, function(e) {
                        var button_id = '#'+$(this).attr('id');
                        var send_attachment_bkp = wp.media.editor.send.attachment;
                        var button = $(button_id);
                        _custom_media = true;
                        wp.media.editor.send.attachment = function(props, attachment){
                            if ( _custom_media ) {
                                $('#nft_marketplace_core_taxonomy_blockchain_currency_image').val(attachment.id);
                                $('#category-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
                                $('#category-image-wrapper .custom_media_image').attr('src',attachment.url).css('display','block');
                            } else {
                                return _orig_send_attachment.apply( button_id, [props, attachment] );
                            }
                        }
                        wp.media.editor.open(button);
                        return false;
                    });
                }
                ct_media_upload('.ct_tax_media_button.button');
                $('body').on('click','.ct_tax_media_remove',function(){
                    $('#nft_marketplace_core_taxonomy_blockchain_currency_image').val('');
                    $('#category-image-wrapper').html('<img class="custom_media_image" src="" style="margin:0;padding:0;max-height:100px;float:none;" />');
                });
                // Thanks: http://stackoverflow.com/questions/15281995/wordpress-create-category-ajax-response
                $(document).ajaxComplete(function(event, xhr, settings) {
                    var queryStringArr = settings.data.split('&');
                    if( $.inArray('action=add-tag', queryStringArr) !== -1 ){
                        var xml = xhr.responseXML;
                        $response = $(xml).find('term_id').text();
                        if($response!=""){
                            // Clear the thumb image
                            $('#category-image-wrapper').html('');
                        }
                    }
                });
            });
        </script>
    <?php }
    function cmb2_after_form_do_js_validation($post_id, $cmb)
    {
        static $added = false;
        // Only add this to the page once (not for every metabox)
        if ($added) {
            return;
        }
        $added = true;
        ?>
        <script type="text/javascript">
            jQuery(document).ready(function ($) {
                "use strict"
                let $form = $(document.getElementById('post'));
                let $htmlbody = $('html, body');
                let $toValidate = $('[data-validation]');
                if (!$toValidate.length) {
                    return;
                }
                function checkValidation(evt) {
                    var labels = [];
                    var $first_error_row = null;
                    var $row = null;
                    function add_required($row) {
                        $row.css({'background-color': 'rgb(255, 170, 170)'});
                        $first_error_row = $first_error_row ? $first_error_row : $row;
                        labels.push($row.find('.cmb-th label').text());
                    }
                    function remove_required($row) {
                        $row.css({background: ''});
                    }
                    $toValidate.each(function () {
                        var $this = $(this);
                        var val = $this.val();
                        $row = $this.parents('.cmb-row');
                        if ($this.is('[type="button"]') || $this.is('.cmb2-upload-file-id')) {
                            return true;
                        }
                        if ('required' === $this.data('validation')) {
                            if ($row.is('.cmb-type-file-list')) {
                                var has_LIs = $row.find('ul.cmb-attach-list li').length > 0;
                                if (!has_LIs) {
                                    add_required($row);
                                } else {
                                    remove_required($row);
                                }
                            } else {
                                if (!val) {
                                    add_required($row);
                                } else {
                                    remove_required($row);
                                }
                            }
                        }
                    });
                    if ($first_error_row) {
                        evt.preventDefault();
                        alert('<?php esc_html_e('The following fields are required and highlighted below:', 'nft-marketplace-core'); ?> ' + labels.join(', '));
                        $htmlbody.animate({
                            scrollTop: ($first_error_row.offset().top - 200)
                        }, 1000);
                    }
                }
                $form.on('submit', checkValidation);
            });
        </script>
        <?php
    }


    /**
     * Hook in and add a metabox to add fields to taxonomy terms
     */
    function nft_marketplace_core_nft_listing_repeatable_stats()
    {
        $fields_group = new_cmb2_box(array(
            'id' => 'nft_marketplace_core_nft_listing_group',
            'title' => esc_html__('NFT’s Stats', 'nft-marketplace-core'),
            'object_types' => array('nft-listing'),
        ));
        // $fields_group_id is the field id string, so in this case: 'yourprefix_group_demo'
        $fields_group_id = $fields_group->add_field(array(
            'id' => 'nft_marketplace_core_nft_listing_group',
            'type' => 'group',
            'options' => array(
                'group_title' => esc_html__('Stat', 'nft-marketplace-core'), // {#} gets replaced by row number
                'add_button' => esc_html__('Add New Stat', 'nft-marketplace-core'),
                'remove_button' => esc_html__('Remove', 'nft-marketplace-core'),
                'sortable' => true,
                'closed' => true, // true to have the groups closed by default
                'remove_confirm' => esc_html__('Are you sure you want to delete this Stat?', 'nft-marketplace-core'), // Performs confirmation before removing group.
            ),
        ));
        $fields_group->add_group_field($fields_group_id, array(
            'name' => esc_html__('Name', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_stat_name',
            'type' => 'text'
        ));
        $fields_group->add_group_field($fields_group_id, array(
            'name' => esc_html__('Rate', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_stat_rate',
            'type' => 'text'
        ));
        $fields_group->add_group_field($fields_group_id, array(
            'name' => esc_html__('Out of', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_stat_out_of',
            'type' => 'text'
        ));
    }


    function nft_marketplace_core_nft_listing_price()
    {
        $cmb = new_cmb2_box(array(
            'id' => 'nft_marketplace_core_nft_listing_price_group',
            'title' => esc_html__('General Information', 'nft-marketplace-core'),
            'object_types' => array('nft-listing'),
        ));
        $cmb->add_field(array(
            'name' => esc_html__('NFT Description', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_description_meta',
            'type' => 'textarea'
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Unlockable Content', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_content_meta',
            'type' => 'checkbox',
            'desc' => esc_html__('Include unlockable content that can only be revealed by the owner of the item. This field is public. You must use another service to hide this value.', 'nft-marketplace-core')
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Content', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_content',
            'type' => 'text'
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Explicit Content', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_explicit_meta',
            'type' => 'checkbox',
            'desc' => esc_html__('Setting your asset as explicit and sensitive content, like pornography and other not safe for work (NSFW) content, will protect users with safe search while browsing.', 'nft-marketplace-core')
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Sales', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_sales',
            'type' => 'text'
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Presale', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_presale',
            'type' => 'radio',
            'options' => array(
                'standard' => esc_html__('Yes', 'nft-marketplace-core'),
                'custom' => esc_html__('No', 'nft-marketplace-core')
            ),
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Owner', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_owner',
            'type' => 'text'
        ));
    }


    function nft_marketplace_core_nft_listing_socials()
    {
        $cmb = new_cmb2_box(array(
            'id' => 'nft_marketplace_core_nft_listing_socials_group',
            'title' => esc_html__('Socials', 'nft-marketplace-core'),
            'object_types' => array('nft-listing'),
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Slug', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_slug',
            'type' => 'text'
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Telegram Url', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_telegram_url',
            'type' => 'text'
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Twitter', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_twitter_url',
            'type' => 'text'
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Instagram', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_instagram_url',
            'type' => 'text'
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Wiki', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_wiki_url',
            'type' => 'text'
        ));
        $cmb->add_field(array(
            'name' => esc_html__('NFTs Contract Address', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_address',
            'type' => 'text',
            'attributes' => array(
                'data-validation' => 'required',
            )
        ));
    }


    function nft_marketplace_core_nft_listing_crypto_prices()
    {
        $cmb = new_cmb2_box(array(
            'id' => 'nft_marketplace_core_nft_listing_crypto_prices_group',
            'title' => esc_html__('Crypto Prices', 'nft-marketplace-core'),
            'object_types' => array('nft-listing'),
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Currency Symbol', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_currency_symbol',
            'type' => 'text',
            'attributes' => array(
                'data-validation' => 'required',
            )
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Currency Name', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_currency_name',
            'type' => 'text',
            'attributes' => array(
                'data-validation' => 'required',
            )
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Currency Price', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_currency_price',
            'type' => 'text',
            'attributes' => array(
                'data-validation' => 'required',
            )
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Regular Price (USD)', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_regular_price',
            'type' => 'text'
        ));
    }


    function nft_marketplace_core_nft_listing_repeatable_properties()
    {
        $fields_group = new_cmb2_box(array(
            'id' => 'nft_marketplace_core_nft_listing_properties_group',
            'title' => esc_html__('NFT’s Properties', 'nft-marketplace-core'),
            'object_types' => array('nft-listing'),
        ));
        // $fields_group_id is the field id string, so in this case: 'yourprefix_group_demo'
        $fields_group_id = $fields_group->add_field(array(
            'id' => 'nft_marketplace_core_nft_listing_properties_group',
            'type' => 'group',
            'options' => array(
                'group_title' => esc_html__('Property', 'nft-marketplace-core'), // {#} gets replaced by row number
                'add_button' => esc_html__('Add New Property', 'nft-marketplace-core'),
                'remove_button' => esc_html__('Remove', 'nft-marketplace-core'),
                'sortable' => true,
                'closed' => true, // true to have the groups closed by default
                'remove_confirm' => esc_html__('Are you sure you want to delete this Property?', 'nft-marketplace-core'), // Performs confirmation before removing group.
            ),
        ));
        $fields_group->add_group_field($fields_group_id, array(
            'name' => esc_html__('Name', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_property_name',
            'type' => 'text'
        ));
        $fields_group->add_group_field($fields_group_id, array(
            'name' => esc_html__('Value', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_property_value',
            'type' => 'text'
        ));
    }


    function nft_marketplace_core_nft_listing_repeatable_levels()
    {
        $fields_group = new_cmb2_box(array(
            'id' => 'nft_marketplace_core_nft_listing_level_group',
            'title' => esc_html__('NFT’s Levels', 'nft-marketplace-core'),
            'object_types' => array('nft-listing'),
        ));
        // $fields_group_id is the field id string, so in this case: 'yourprefix_group_demo'
        $fields_group_id = $fields_group->add_field(array(
            'id' => 'nft_marketplace_core_nft_listing_level_group',
            'type' => 'group',
            'options' => array(
                'group_title' => esc_html__('Level', 'nft-marketplace-core'), // {#} gets replaced by row number
                'add_button' => esc_html__('Add New Level', 'nft-marketplace-core'),
                'remove_button' => esc_html__('Remove', 'nft-marketplace-core'),
                'sortable' => true,
                'closed' => true, // true to have the groups closed by default
                'remove_confirm' => esc_html__('Are you sure you want to delete this Level?', 'nft-marketplace-core'), // Performs confirmation before removing group.
            ),
        ));
        $fields_group->add_group_field($fields_group_id, array(
            'name' => esc_html__('Name', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_level_name',
            'type' => 'text',
            'default' => ''
        ));
        $fields_group->add_group_field($fields_group_id, array(
            'name' => esc_html__('Rate', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_level_rate',
            'type' => 'text',
            'default' => ''
        ));
        $fields_group->add_group_field($fields_group_id, array(
            'name' => esc_html__('Out of', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_nft_listing_level_out_of',
            'type' => 'text'
        ));
    }


    function nft_marketplace_core_author_fields()
    {
        $cmb = new_cmb2_box(array(
            'id' => 'nft_marketplace_core_user_extra_fields',
            'object_types' => array('user'),
        ));
        $cmb->add_field(array(
            'name' => esc_html__('NFT Marketplace Core Extra Fields', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_user_extra_fields',
            'type' => 'title'
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Profile Banner Image','nft-marketplace-core'),
            'id' => 'nft_marketplace_core_banner_img',
            'type' => 'file',
            'options' => array(
                'url' => false,
            ),
            'text' => array(
                'add_upload_file_text' => 'Add File'
            ),
            // query_args are passed to wp.media's library query.
            'query_args' => array(
                'type' => array(
                    'image/gif',
                    'image/jpeg',
                    'image/png',
                ),
            ),
            'preview_size' => 'large',
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Address Hash', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_user_address',
            'type' => 'text'
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Facebook link', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_user_facebook',
            'type' => 'text'
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Instagram link', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_user_instagram',
            'type' => 'text'
        ));
        $cmb->add_field(array(
            'name' => esc_html__('Youtube link', 'nft-marketplace-core'),
            'id' => 'nft_marketplace_core_user_youtube',
            'type' => 'text'
        ));
    }

    function nft_marketplace_core_admin_notice_before_activation() {
        $screen = get_current_screen(); 
        $home_url = get_home_url().'/wp-admin/edit.php?post_type=nft-listing&page=nft-marketplace-core-panel#activator' ;
        if($screen->id == "nft-listing" && $screen->base == "post"):?>
            <div class="error">
                <h3><?php esc_html_e( 'You must activate your license for NFT Marketplace Core in order to experience the full benefits of our plugin. Go back to ', 'nft-marketplace-core' ); ?><a href="<?php echo esc_url($home_url); ?>"><?php esc_html_e('License Manager.','nft-marketplace-core'); ?></a></h3>
            </div>
        <?php endif; 
    }
}