<?php
if (count(get_terms(array(
        'taxonomy' => 'nft_listing_blockchains', // taxonomy slug
        'hide_empty' => false,
    ))) === 0) { ?>

    <!-- todo stilizeaza -->
    <p><?php esc_html_e("Add a blockchain first.", "nft-marketplace-core"); ?></p>

    <?php
} else {
    if (isset($_GET["edit-nft"])) {
        global $wpdb;

        if (isset($_POST["nft_listing_price"])) {
            global $wpdb;
            if (!is_numeric($_POST["nft_listing_price"])) {
                ?>

                <div id="ajax-response">
                    <div class="error">
                        <p><?php esc_html_e("Please check if you have set the price correctly.", "nft-marketplace-core"); ?></p>
                    </div>
                </div>

                <?php
            } else {
                $wpdb->update($wpdb->prefix . "nft_marketplace_core_contracts", [
                    "listing_price" => $_POST["nft_listing_price"],
                ], ["id" => esc_sql($_GET["edit-nft"])]);
            }
        }

        $contract = $wpdb->get_row("SELECT * FROM " . $wpdb->prefix . "nft_marketplace_core_contracts WHERE id = " . esc_sql($_GET["edit-nft"]));
        ?>

        <div class="wrap nosubsub">
            <h1 class="wp-heading-inline"><?php esc_html_e("Contracts", "nft-marketplace-core"); ?></h1>

            <div id="col-container" class="wp-clearfix">

                <div id="col-left">
                    <div class="col-wrap">
                        <form method="post" data-function-name="updateListingPrice" data-contract-address="<?php esc_html_e($contract->contract_address); ?>"  id="nft-marketplace-core-call-setter"  class="form-wrap">
                            <h2><?php esc_html_e("Edit Contract", "nft-marketplace-core"); ?></h2>
                            <div class="form-field form-required term-slug-wrap">
                                <label for="nft_listing_price"><?php esc_html_e("New Listing price:", "nft-marketplace-core"); ?></label><br>
                                <input required="" name="nft_listing_price" id="nft_listing_price" type="number" step="0.000000005"  data-is-big-number="true"
                                       value="<?php esc_html_e($contract->listing_price); ?>">
                                <p class="description"><?php esc_html_e("The price you want to get paid per NFT listed for sale on your marketplace.", "nft-marketplace-core"); ?></p>
                            </div>
                            <button type="submit"
                                    class="button button-primary"><?php esc_html_e("Update", "nft-marketplace-core"); ?></button>
                        </form>
                    </div>
                </div>

            </div><!-- /col-container -->

        </div>
        <?php
    } else {
        if (isset($_POST["nft_listing_price"])) {
            global $wpdb;
            $termData = get_term_by('slug', $_POST["nft_listing_blockchains"], "nft_listing_blockchains");
            if ($termData === false || !is_numeric($_POST["nft_listing_price"])) {
                ?>

                <div id="ajax-response">
                    <div class="error">
                        <p><?php esc_html_e("Please check if you have set the price correctly.", "nft-marketplace-core"); ?></p>
                    </div>
                </div>

                <?php
            }  elseif(!isset($_POST['nft_listing_result']) || !preg_match("/^0x[a-fA-F0-9]{40}$/", $_POST['nft_listing_result']) ) {
                ?>
                <div id="ajax-response">
                    <div class="error">
                        <p><?php esc_html_e("The contract data is empty, did you really deploy the contract?", "nft-marketplace-core"); ?></p>
                    </div>
                </div>
                <?php
            }else {
                $wpdb->insert($wpdb->prefix . "nft_marketplace_core_contracts", [
                    "taxonomy_blockchain_id" => $termData->term_id,
                    "is_deployed" => "1",
                    "listing_price" => $_POST["nft_listing_price"],
                    "contract_address" => $_POST['nft_listing_result'],
                ]);
            }
        }
        ?>
        <div class="wrap nosubsub">
            <h1 class="wp-heading-inline"><?php esc_html_e("Contracts", "nft-marketplace-core"); ?></h1>

            <div id="col-container" class="wp-clearfix">

                <div id="col-left">
                    <div class="col-wrap">
                        <form method="post" id="nft-marketplace-core-form-to-deploy" class="form-wrap">
                            <h2><?php esc_html_e("Add New Contract", "nft-marketplace-core"); ?></h2>
                            <div class="form-field form-required term-slug-wrap">
                                <label for="nft_listing_price"
                                       ><?php esc_html_e("Listing price:", "nft-marketplace-core"); ?></label><br>
                                <input required="" name="nft_listing_price" id="nft_listing_price" type="number" step="0.000000005" value="">
                                <p class="description"><?php esc_html_e("The price you want to get paid per NFT listed for sale on your marketplace.", "nft-marketplace-core"); ?></p>
                            </div>

                            <input hidden data-ignore="true" name="nft_listing_result" id="nft_listing_result" type="text" value="">

                            <div class="form-field form-required term-slug-wrap">
                                <label for="nft-change-blockchain"
                                       ><?php esc_html_e("Blockchain:", "nft-marketplace-core"); ?></label><br>

                                <?php $items = get_terms( array(
                                    'taxonomy' => 'nft_listing_blockchains',
                                    'hide_empty' => false,
                                ) );?>
                                <select required name="nft_listing_blockchains" id="nft-change-blockchain" data-ignore="true" class="postform">
                                    <option value="" selected disabled hidden><?php esc_html_e("Choose here", "nft-marketplace-core"); ?></option>
                                    <?php foreach( $items as $item ) : ?>
                                        <option value="<?php echo esc_attr($item->slug); ?>" data-blockchain="<?php echo esc_attr(json_encode(get_term_meta($item->term_taxonomy_id))); ?>" id="term-id-<?php echo esc_attr($item->term_id); ?>"><?php echo esc_html($item->name); ?></option>
                                    <?php endforeach; ?>
                                </select>
                                <p class="description"><?php esc_html_e("Select the blockchain where you want to deploy the contract to.", "nft-marketplace-core"); ?></p>
                            </div>

                            <button type="submit" class="button button-primary"><?php esc_html_e("Deploy", "nft-marketplace-core"); ?></button>
                        </form>
                    </div>
                </div>

                <div id="col-right">
                    <div class="col-wrap">
                        <?php

                        require plugin_dir_path(dirname(__DIR__)) . "includes/NFT_Marketplace_Core_List_Table_Custom.php";

                        $table = new NFT_Marketplace_Core_List_Table_Custom();
                        $table->prepare_items();
                        $table->display();
                        ?>

                    </div>
                </div><!-- /col-right -->

            </div><!-- /col-container -->

        </div>
    <?php }
}
?>