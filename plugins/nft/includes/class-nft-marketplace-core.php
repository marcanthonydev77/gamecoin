<?php
/**
 * The file that defines the core plugin class
 *
 * A class definition that includes attributes and functions used across both the
 * public-facing side of the site and the admin area.
 *
 * @link       https://modeltheme.com/
 * @since      1.0.0
 *
 * @package    NFT_Marketplace_Core
 * @subpackage NFT_Marketplace_Core/includes
 */


/**
 * The core plugin class.
 *
 * This is used to define internationalization, admin-specific hooks, and
 * public-facing site hooks.
 *
 * Also maintains the unique identifier of this plugin as well as the current
 * version of the plugin.
 *
 * @since      1.0.0
 * @package    NFT_Marketplace_Core
 * @subpackage NFT_Marketplace_Core/includes
 * @author     ModelTheme <support@modeltheme.com>
 */


class NFT_Marketplace_Core {
	/**
	 * The loader that's responsible for maintaining and registering all hooks that power
	 * the plugin.
	 */
	protected $loader;
	/**
	 * The unique identifier of this plugin.
	 */
	protected $plugin_name;
	/**
    * The array of templates that this plugin tracks.
    */
    protected $templates;
	/**
	 * The current version of the plugin.
	 */
	protected $version;
	/**
	 * Store plugin admin class to allow public access.
	 */
	public $admin;
	/**
	 * Store plugin admin class to allow public access.
	 */
	public $shortcode_manager;
	/**
	 * Store plugin public class to allow public access.
	 */
	public $public;
	/**
	 * Store plugin public class to allow public access.
	 */
	public $wp_bakery_map;
	public function __construct() {
		if ( defined( 'nft_marketplace_core_VERSION' ) ) {
			$this->version = nft_marketplace_core_VERSION;
		} else {
			$this->version = '1.2.2';
		}
		$this->plugin_name = 'nft-marketplace-core';
		$this->load_dependencies();
		$this->set_locale();
		$this->define_admin_hooks();
        $this->define_public_hooks();
        $this->define_api_hooks();
    }


	/**
	 * Load the required dependencies for this plugin.
	 *
	 * Include the following files that make up the plugin:
	 *
	 * - nft_marketplace_core_Loader. Orchestrates the hooks of the plugin.
	 * - nft_marketplace_core_i18n. Defines internationalization functionality.
	 * - nft_marketplace_core_Admin. Defines all hooks for the admin area.
	 * - nft_marketplace_core_Public. Defines all hooks for the public side of the site.
	 *
	 * Create an instance of the loader which will be used to register the hooks
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function load_dependencies() {
		/**
		 * The class responsible for orchestrating the actions and filters of the
		 * core plugin.
		 */
		require_once plugin_dir_path( __DIR__ ) . 'includes/class-nft-marketplace-core-loader.php';
		/**
		 * The class responsible for defining internationalization functionality
		 * of the plugin.
		 */
		require_once plugin_dir_path( __DIR__ ) . 'includes/class-nft-marketplace-core-i18n.php';
		/**
		 * The class responsible for defining all actions that occur in the admin area.
		 */
		require_once plugin_dir_path( __DIR__ ) . 'admin/class-nft-marketplace-core-admin.php';
		/**
		 * The class responsible for defining all actions that occur in the public-facing
		 * side of the site.
		 */
		require_once plugin_dir_path( __DIR__ ) . 'public/class-nft-marketplace-core-public.php';
		/**
		 * The class responsible for including cmb2 library
		 */
		require_once plugin_dir_path( __DIR__ ) . 'includes/cmb2/init.php';
		/**
		 * The class responsible for including Panel
		 */
		require_once plugin_dir_path( __DIR__ ) . 'includes/nft-marketplace-core-panel/nft-marketplace-core-panel.php';
		/**
		 * The class responsible Frontend Media */
		require_once plugin_dir_path( __DIR__ ) . 'includes/frontend-media/frontend-media.php';
		/**
		 * The class responsible Vendor */
        require_once plugin_dir_path(__DIR__)."includes/vendor/autoload.php";
        
        $this->loader = new nft_marketplace_core_Loader();
	}


	/**
	 * Define the locale for this plugin for internationalization.
	 *
	 * Uses the nft_marketplace_core_i18n class in order to set the domain and to register the hook
	 * with WordPress.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function set_locale() {
		$plugin_i18n = new nft_marketplace_core_i18n();
		$this->loader->add_action( 'plugins_loaded', $plugin_i18n, 'load_plugin_textdomain' );
	}


	/**
	 * Register all of the hooks related to the admin area functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_admin_hooks() {
        global $plugin_admin;
        $plugin_admin = new nft_marketplace_core_Admin($this->get_plugin_name(), $this->get_version());
        $this->admin = new nft_marketplace_core_Admin($this->get_plugin_name(), $this->get_version());

        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_styles');
        $this->loader->add_action('admin_enqueue_scripts', $plugin_admin, 'enqueue_scripts');

		//CPT
		$this->loader->add_action( 'init', $plugin_admin, 'nft_marketplace_core_register_nft_listing_custom_post' );
        $this->loader->add_action( 'init', $plugin_admin, 'nft_marketplace_core_register_nft_listing_category', 1 );
        $this->loader->add_action( 'init', $plugin_admin, 'nft_marketplace_core_register_nft_listing_blockchains' );
        $this->loader->add_action( 'cmb2_after_form', $plugin_admin, 'cmb2_after_form_do_js_validation', 10, 2 );

        // Blockchains Taxonomy Custom Fields
        $this->loader->add_action( 'nft_listing_blockchains_add_form_fields', $plugin_admin,'nft_marketplace_core_bltx_add_term_fields', 10, 2 );
        $this->loader->add_action( 'nft_listing_blockchains_edit_form_fields', $plugin_admin,'nft_marketplace_core_bltx_edit_term_fields', 10, 2 );
        $this->loader->add_action( 'created_nft_listing_blockchains', $plugin_admin,'nft_marketplace_core_bltx_save_term_fields' );
        $this->loader->add_action( 'edited_nft_listing_blockchains', $plugin_admin,'nft_marketplace_core_bltx_update_term_fields' );
        $this->loader->add_action( 'admin_footer', $plugin_admin, 'nft_marketplace_core_bltx_add_script' );
        $this->loader->add_action('admin_footer', $this, "loadDev");

        // Add new menu for contract management
        $this->loader->add_action( 'admin_menu', $plugin_admin, 'add_submenu_page_for_contract_management_page' );

        if(get_option("modelthemeAPIactivator") !== false) {
			$ch = curl_init();
			$productID = 36258540;
            curl_setopt($ch, CURLOPT_URL, "http://api.modeltheme.com/activator/license-status.php");
           	curl_setopt($ch, CURLOPT_POST, 1);
           	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
                    "key" => get_option("modelthemeAPIactivator")[0],
                    "productId" => $productID,
                    "data" => [
                        "user" => [
                            ["value" => get_option("siteurl")],
                            ["value" => get_option("admin_email")]
                        ]
                    ]
                ], JSON_UNESCAPED_SLASHES));

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $server_output = curl_exec($ch);

                curl_close($ch);
                $state = json_decode($server_output, true);
				$state["success"] = true ;
                if ($state["success"] !== false) {
                	$this->loader->add_action( 'cmb2_admin_init', $plugin_admin, 'nft_marketplace_core_nft_listing_price' );
		            $this->loader->add_action( 'cmb2_admin_init', $plugin_admin, 'nft_marketplace_core_nft_listing_repeatable_stats' );
		            $this->loader->add_action( 'cmb2_admin_init', $plugin_admin, 'nft_marketplace_core_nft_listing_repeatable_properties' );
		            $this->loader->add_action( 'cmb2_admin_init', $plugin_admin, 'nft_marketplace_core_nft_listing_repeatable_levels' );
		            $this->loader->add_action( 'cmb2_admin_init', $plugin_admin, 'nft_marketplace_core_nft_listing_socials' );
		            $this->loader->add_action( 'cmb2_admin_init', $plugin_admin, 'nft_marketplace_core_nft_listing_crypto_prices' );
		            $this->loader->add_action( 'cmb2_admin_init', $plugin_admin, 'nft_marketplace_core_author_fields' ); 
		        } else {
		        	$this->loader->add_action('admin_notices', $plugin_admin, 'nft_marketplace_core_admin_notice_before_activation');
		        }
		}
    }

	/**
	 * Register all of the hooks related to the public-facing functionality
	 * of the plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 */
	private function define_public_hooks() {
		global $plugin_public;
		$plugin_public = new nft_marketplace_core_Public( $this->get_plugin_name(), $this->get_version() );
	    $this->public = new nft_marketplace_core_Public( $this->get_plugin_name(), $this->get_version() );

	    //ACTIONS
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_styles' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'enqueue_scripts' );
		$this->loader->add_action( 'wp_enqueue_scripts', $plugin_public, 'nft_marketplace_core_dynamic_css' );
		$this->loader->add_action( 'wp_kses_allowed_html', $plugin_public, 'nft_marketplace_core_kses_allowed_html', 10, 2); 
    	$this->loader->add_action('wp_footer', $this,"loadDev");
    	$this->loader->add_action('wp_head', $plugin_public,"isLoggedInWithMetaMask");

		//Single NFT
		if(isset(get_option( 'nft_marketplace_core_panel_single_nft' )['nft_marketplace_core_likes']) || isset(get_option( 'nft_marketplace_core_panel_single_nft' )['nft_marketplace_core_likes']) && get_option( 'nft_marketplace_core_panel_single_nft' )['nft_marketplace_core_likes'] == 'on') {
			$this->loader->add_action( 'nft_marketplace_core_single_nft_before_title', $plugin_public, 'nft_marketplace_core_display_love_button', 30);
		}
		if(!isset(get_option( 'nft_marketplace_core_panel_single_nft' )['nft_marketplace_core_views']) || isset(get_option( 'nft_marketplace_core_panel_single_nft' )['nft_marketplace_core_views']) && get_option( 'nft_marketplace_core_panel_single_nft' )['nft_marketplace_core_views'] == 'on') {	
            $this->loader->add_action( 'nft_marketplace_core_single_nft_before_title', $plugin_public, 'nft_marketplace_core_display_views_count', 20);
		}
		$this->loader->add_action( 'nft_marketplace_core_single_nft_before_title', $plugin_public, 'nft_marketplace_core_display_owner', 10);
		$this->loader->add_action( 'nft_marketplace_core_single_nft_after_title', $plugin_public, 'nft_marketplace_core_single_nft_single_category');
		$this->loader->add_action( 'nft_marketplace_core_single_nft_after_button', $plugin_public, 'nft_marketplace_core_single_nft_meta');
		if(!isset(get_option( 'nft_marketplace_core_panel_single_nft' )['nft_marketplace_core_related']) || isset(get_option( 'nft_marketplace_core_panel_single_nft' )['nft_marketplace_core_related']) && get_option( 'nft_marketplace_core_panel_single_nft' )['nft_marketplace_core_related'] == 'on') {
			$this->loader->add_action( 'nft_marketplace_core_single_nft_related', $plugin_public, 'nft_marketplace_core_single_nft_related');
		}
		$this->loader->add_action( 'nft_marketplace_core_single_nft_before_author_tab', $plugin_public, 'nft_marketplace_core_single_nft_tab_stats', 10);
		$this->loader->add_action( 'nft_marketplace_core_single_nft_before_author_tab', $plugin_public, 'nft_marketplace_core_single_nft_tab_properties', 20);
		$this->loader->add_action( 'nft_marketplace_core_single_nft_before_author_tab', $plugin_public, 'nft_marketplace_core_single_nft_tab_levels', 30);
		$this->loader->add_action( 'nft_marketplace_core_single_nft_after_metas', $plugin_public, 'nft_marketplace_core_single_nft_tabs_wrapper');
		$this->loader->add_action( 'nft_marketplace_core_single_nft_before_related', $plugin_public, 'nft_marketplace_core_single_nft_tabs_wrapper');
		$this->loader->add_action( 'nft_marketplace_core_single_nft_after_levels_tab', $plugin_public, 'nft_marketplace_core_social_profiles', 20);
		$this->loader->add_action( 'nft_marketplace_core_single_nft_after_levels_tab', $plugin_public, 'nft_marketplace_core_single_nft_tab_auth', 10);
		$this->loader->add_action( 'nft_marketplace_core_single_nft_before_collection_text', $plugin_public, 'nft_marketplace_core_single_nft_category');

		//Category
		$this->loader->add_action( 'nft_marketplace_core_archive_before_grid', $plugin_public, 'nft_marketplace_core_archive_ordering', 20);
		$this->loader->add_action( 'nft_marketplace_core_archive_before_grid', $plugin_public, 'nft_marketplace_core_archive_listings_count', 10);
		$this->loader->add_action( 'nft_marketplace_core_archive_after_grid_template', $plugin_public, 'nft_marketplace_core_archive_pagination');
		$this->loader->add_action( 'nft_marketplace_core_archive_listing_query', $plugin_public, 'nft_marketplace_core_single_nft_block');

		//NFT Listings template
		$this->loader->add_action( 'nft_marketplace_core_archive_before_grid_template', $plugin_public, 'nft_marketplace_core_archive_listings_count_template', 10);
		$this->loader->add_action( 'nft_marketplace_core_archive_before_grid_template', $plugin_public, 'nft_marketplace_core_archive_ordering_template', 20);
		$this->loader->add_action( 'nft_marketplace_core_archive_listing_query_template', $plugin_public, 'nft_marketplace_core_single_nft_block');

		//GENERAL USE
		$this->loader->add_action( 'nft_marketplace_core_before_main_content', $plugin_public, 'nft_marketplace_core_breadcrumbs');
		$this->loader->add_action( 'nft_marketplace_core_single_nft_block_after_title', $plugin_public, 'nft_marketplace_core_display_love_button');
		$this->loader->add_action( 'nft_marketplace_core_search_listing_query', $plugin_public, 'nft_marketplace_core_single_nft_block');
		$this->loader->add_action( 'nft_marketplace_core_related_listing_query', $plugin_public, 'nft_marketplace_core_single_nft_block');

		//Author page
		$this->loader->add_action( 'nft_marketplace_core_author_header_right', $plugin_public, 'nft_marketplace_core_social_profiles');
		$this->loader->add_action( 'nft_marketplace_core_author_after_grid', $plugin_public, 'nft_marketplace_core_author_listings_pagination');
		$this->loader->add_action( 'nft_marketplace_core_author_listing_query', $plugin_public, 'nft_marketplace_core_single_nft_block');

		//Likes
		$this->loader->add_action( 'wp_ajax_nft_marketplace_core_love_post', $plugin_public, 'nft_marketplace_core_love_post'); 
		$this->loader->add_action( 'wp_ajax_nopriv_nft_marketplace_core_love_post', $plugin_public, 'nft_marketplace_core_love_post');

		//FILTERS
		$this->loader->add_filter( 'single_template', $plugin_public, 'nft_marketplace_core_single_template_from_directory_nft_listing' );
    	$this->loader->add_filter( 'taxonomy_template', $plugin_public, 'nft_marketplace_core_taxonomy_template_from_directory_nft_listing' );
		$this->loader->add_filter( 'template_redirect', $plugin_public, 'nft_marketplace_core_views_count' );
		$this->loader->add_filter( 'template_include', $plugin_public, 'nft_marketplace_core_search_template');
		$this->loader->add_filter( 'template_include', $plugin_public, 'nft_marketplace_core_author_template');
		$this->loader->add_filter( 'page_template', $plugin_public, 'nft_marketplace_core_listings_template' );

		//SHORTCODES
		$this->loader->add_shortcode( 'nft_marketplace_core_edit_author_form_shortcode', $plugin_public, 'nft_marketplace_core_edit_form_shortcode' );
		$this->loader->add_shortcode( 'nft_marketplace_core_update_author_form_shortcode', $plugin_public, 'nft_marketplace_core_update_form_shortcode' );
		$this->loader->add_shortcode( 'nft_marketplace_core_submit_author_form_shortcode', $plugin_public, 'nft_marketplace_core_submit_form_shortcode' );
		$this->loader->add_shortcode( 'nft_archive_search_shortcode', $plugin_public, 'nft_marketplace_core_archive_search_shortcode');
		$this->loader->add_shortcode( 'nft_archive_category_shortcode', $plugin_public, 'nft_marketplace_core_archive_sidebar_category_shortcode');

        // EDIT NFT Page
        $this->loader->add_filter( 'single_template', $plugin_public, 'nft_marketplace_core_edit_template_from_directory' );
        // List NFT Page
        $this->loader->add_filter( 'single_template', $plugin_public, 'nft_marketplace_core_list_nft_template_from_directory' );

        //Compatibility with Wordpress NFT Creator
        $this->loader->add_filter('wpnc_event_after_creation', $this->public, 'nft_marketplace_core_add_nft_from_creator');
        $this->loader->add_filter('wpnc_event_before_creation', $this->public, 'nft_marketplace_core_validate_from_creator');
    }

    /**
     * Load dev env
     * @return void
     */
    public function loadDev()
    {
        $path = plugin_dir_path(__DIR__)."dev-do-not-upload/Vite.php";
        if(file_exists( $path )) {
            require $path;
            $vite = new Vite();

            echo wp_kses($vite, 'vite');
        }
    }

	/**
	 * Run the loader to execute all of the hooks with WordPress.
	 *
	 * @since    1.0.0
	 */
	public function run() {
		$this->loader->run();
	}


	/**
	 * The name of the plugin used to uniquely identify it within the context of
	 * WordPress and to define internationalization functionality.
	 *
	 * @since     1.0.0
	 * @return    string    The name of the plugin.
	 */
	public function get_plugin_name() {
		return $this->plugin_name;
	}


	/**
	 * The reference to the class that orchestrates the hooks with the plugin.
	 *
	 * @since     1.0.0
	 * @return    nft_marketplace_core_Loader    Orchestrates the hooks of the plugin.
	 */
	public function get_loader() {
		return $this->loader;
	}

	
	/**
	 * Retrieve the version number of the plugin.
	 *
	 * @since     1.0.0
	 * @return    string    The version number of the plugin.
	 */
	public function get_version() {
		return $this->version;
	}

    private function define_api_hooks()
    {
    }
}