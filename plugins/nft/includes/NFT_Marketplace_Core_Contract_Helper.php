<?php

use kornrunner\Keccak;
use SWeb3\SWeb3;
use SWeb3\SWeb3_Contract;
use Web3\Web3;
use Web3\Providers\HttpProvider;
use Web3\RequestManagers\HttpRequestManager;
use Web3\Contract;

require(plugin_dir_path(__DIR__)."includes/SWeb3_Wrapper.php");

class NFT_Marketplace_Core_Contract_Helper
{

    private $provider;
    private $sweb3;

    private $marketplaceAddress;
    private $marketplaceABI;
    private $marketplaceContractRawData;
    private $marketplaceContract;
    private $nftAddress;

    private $nftContractRawData;
    /**
     * @var Contract
     */
    private $nftContract;

    public function __construct($provider, $marketplaceAddress, $marketplaceContractRawData, $nftAddress = null, $nftContractRawData = null)
    {
        $this->provider = $provider;
        $this->sweb3 = new SWeb3_Wrapper($this->provider);
        if($nftAddress !== null) {
            $this->nftAddress = $nftAddress;
            $this->nftContractRawData = $nftContractRawData;
            $this->nftABI = json_decode($nftContractRawData, true)["abi"];
            $this->nftContract = new SWeb3_contract($this->sweb3, $this->nftAddress, json_encode($this->nftABI));
        }
        $this->marketplaceAddress = $marketplaceAddress;
        $this->marketplaceContractRawData = $marketplaceContractRawData;
        $this->marketplaceABI = json_decode($marketplaceContractRawData, true)["abi"];
        $this->marketplaceContract = new SWeb3_contract($this->sweb3, $this->marketplaceAddress ,json_encode($this->marketplaceABI));
    }

    /**
     * @throws Exception
     */
    public function isItemListed($tokenID) {
        return !property_exists($this->marketplaceContract->call("getListedItem", [$this->nftAddress,$tokenID]), "error");
    }

    /**
     * @throws Exception
     */
    public function isItemOnSale($tokenID) {
        $call = $this->marketplaceContract->call("getListedItem", [$this->nftAddress,$tokenID]);
        return !property_exists($call, "error") && !$call->tuple_1->sold;
    }

    /**
     * @throws Exception
     */
    public function didTransactionSucceed($transaction) {
        $call = $this->sweb3->call("eth_getTransactionReceipt", [$transaction]);
        if(property_exists($call, "error")) {
            return false;
        }
        return str_replace("0x", "", $call->result->status) === "1";
    }

    /**
     * @throws Exception
     */
    public function canMarketplaceManageNFT($tokenID) {
        return strtolower($this->nftContract->call("getApproved", [$tokenID])->elem_1) === strtolower($this->marketplaceAddress);
    }

    /**
     * @throws Exception
     */
    public function isNFTMinted($tokenID) {
        return !property_exists($this->nftContract->call("tokenURI", [$tokenID]), "error");
    }

    /**
     * @throws Exception
     */
    public function getMarketplaceFee() {
        return $this->marketplaceContract->call("getListingPrice");
    }

    /**
     * @throws Exception
     */
    public function fetchMarketItems() {
         return $this->marketplaceContract->call("fetchMarketItems");
    }
}