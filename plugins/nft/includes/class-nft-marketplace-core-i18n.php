<?php


/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://modeltheme.com/
 * @since      1.0.0
 *
 * @package    NFT_Marketplace_Core
 * @subpackage NFT_Marketplace_Core/includes
 */


/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    NFT_Marketplace_Core
 * @subpackage NFT_Marketplace_Core/includes
 * @author     ModelTheme <support@modeltheme.com>
 */
class NFT_Marketplace_Core_i18n {

	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain(
			'nft-marketplace-core',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages/'
		);
	}

}