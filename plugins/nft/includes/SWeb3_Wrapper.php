<?php

use kornrunner\Ethereum\Transaction;
use SWeb3\Ethereum_CRPC;
use SWeb3\PersonalData;
use SWeb3\Utils;

class SWeb3_Wrapper extends \SWeb3\SWeb3
{
    private $provider;
    private $extra_curl_params;

    public $utils;

    public $personal;
    public $gasPrice;
    public $chainId;

    private $do_batch;
    private $batched_calls;
    function __construct($url_provider, $extra_curl_params = null)
    {
        parent::__construct($url_provider,$extra_curl_params);
        $this->provider = $url_provider;
    }
    function setPersonalData($address, $privKey)
    {
        $this->personal = new PersonalData($this, $address, $privKey);
    }


    function call($method, $params = null)
    {
        if ($params != null) $params = $this->utils->forceAllNumbersHex($params);

        //format api data
        $ethRequest = new Ethereum_CRPC();
        $ethRequest->id = 1;
        $ethRequest->jsonrpc = '2.0';
        $ethRequest->method = $method;
        $ethRequest->params = $params;

        if ($this->do_batch) {
            $this->batched_calls []= $ethRequest;
            return true;
        }
        else {
            $sendData = json_encode($ethRequest);
            return $this->makeCurl($sendData);
        }
    }


    function send($params)
    {
        if ($params != null) $params = $this->utils->forceAllNumbersHex($params);

        //prepare data
        $nonce = (isset($params['nonce'])) ? $params['nonce'] : '';
        $gasPrice = (isset($params['gasPrice'])) ? $params['gasPrice'] : $this->getGasPrice();
        $gasLimit = (isset($params['gasLimit'])) ? $params['gasLimit'] : '';
        $to = (isset($params['to'])) ? $params['to'] : '';
        $value = (isset($params['value'])) ? $params['value'] : '';
        $data = (isset($params['data'])) ? $params['data'] : '';
        $chainId = (isset($this->chainId)) ? $this->chainId : '0x0';

        //sign transaction
        $transaction = new Transaction ($nonce, $gasPrice, $gasLimit, $to, $value, $data);
        $signedTransaction = '0x' . $transaction->getRaw ($this->personal->privateKey, $chainId);

        //SEND RAW TRANSACTION
        //format api data
        $ethRequest = new Ethereum_CRPC();
        $ethRequest->id = 0;
        $ethRequest->jsonrpc = '2.0';
        $ethRequest->method = 'eth_sendRawTransaction';

        $ethRequest->params = [$signedTransaction];

        if ($this->do_batch) {
            $this->batched_calls []= $ethRequest;
            return true;
        }
        else {
            $sendData = json_encode($ethRequest);
            return $this->makeCurl($sendData);
        }
    }
    private function makeCurl($sendData)
    {
        //prepare curl
        $tuCurl = curl_init();
        curl_setopt($tuCurl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($tuCurl, CURLOPT_URL, $this->provider);

        curl_setopt($tuCurl, CURLOPT_PORT , isset(explode(":",str_replace( "://", "", $this->provider))[1]) ? explode(":",str_replace( "://", "", $this->provider))[1] : 443);
        curl_setopt($tuCurl, CURLOPT_POST, 1);
        curl_setopt($tuCurl, CURLOPT_HTTPHEADER, array("Content-Type: application/json", "Content-length: ".strlen($sendData)));
        curl_setopt($tuCurl, CURLOPT_POSTFIELDS, $sendData);

        //execute call
        $tuData = curl_exec($tuCurl);
        if (!curl_errno($tuCurl)) {
            $info = curl_getinfo($tuCurl);
        } else {
            throw new \RuntimeException('Curl send error: ' . curl_error($tuCurl));
        }

        curl_close($tuCurl);

        return json_decode($tuData);
    }
}