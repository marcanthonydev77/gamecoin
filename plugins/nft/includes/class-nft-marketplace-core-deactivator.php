<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://modeltheme.com/
 * @since      1.0.0
 *
 * @package    NFT_Marketplace_Core
 * @subpackage NFT_Marketplace_Core/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    NFT_Marketplace_Core
 * @subpackage NFT_Marketplace_Core/includes
 * @author     ModelTheme <support@modeltheme.com>
 */

class NFT_Marketplace_Core_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */

	public static function deactivate() {

	}

}