/* global tb_remove, JSON */
window.wp = window.wp || {};

(function( $, wp ) {
	'use strict';

	wp.nftmarketplacecore = {};

	/**
	 * User nonce for ajax calls.
	 *
	 * @since 1.0.0
	 *
	 * @var string
	 */
	wp.nftmarketplacecore.ajaxNonce = window._wpUpdatesSettings.ajax_nonce;

	/**
	 * Whether filesystem credentials need to be requested from the user.
	 *
	 * @since 1.0.0
	 *
	 * @var bool
	 */
	wp.nftmarketplacecore.shouldRequestFilesystemCredentials = null;

	/**
	 * Filesystem credentials to be packaged along with the request.
	 *
	 * @since 1.0.0
	 *
	 * @var object
	 */
	wp.nftmarketplacecore.filesystemCredentials = {
		ftp: {
			host: null,
			username: null,
			password: null,
			connectionType: null
		},
		ssh: {
			publicKey: null,
			privateKey: null
		}
	};

	/**
	 * Flag if we're waiting for an update to complete.
	 *
	 * @since 1.0.0
	 *
	 * @var bool
	 */
	wp.nftmarketplacecore.updateLock = false;

	/**
	 * * Flag if we've done an update successfully.
	 *
	 * @since 1.0.0
	 *
	 * @var bool
	 */
	wp.nftmarketplacecore.updateDoneSuccessfully = false;

	/**
	 * If the user tries to update a plugin while an update is
	 * already happening, it can be placed in this queue to perform later.
	 *
	 * @since 1.0.0
	 *
	 * @var array
	 */
	wp.nftmarketplacecore.updateQueue = [];

	/**
	 * Store a jQuery reference to return focus to when exiting the request credentials modal.
	 *
	 * @since 1.0.0
	 *
	 * @var jQuery object
	 */
	wp.nftmarketplacecore.$elToReturnFocusToFromCredentialsModal = null;

	/**
	 * Decrement update counts throughout the various menus.
	 *
	 * @since 3.9.0
	 *
	 * @param {string} upgradeType
	 */
	wp.nftmarketplacecore.decrementCount = function( upgradeType ) {
		var count,
				pluginCount,
				$adminBarUpdateCount = $( '#wp-admin-bar-updates .ab-label' ),
				$dashboardNavMenuUpdateCount = $( 'a[href="update-core.php"] .update-plugins' ),
				$pluginsMenuItem = $( '#menu-plugins' );

		count = $adminBarUpdateCount.text();
		count = parseInt( count, 10 ) - 1;
		if ( count < 0 || isNaN( count ) ) {
			return;
		}
		$( '#wp-admin-bar-updates .ab-item' ).removeAttr( 'title' );
		$adminBarUpdateCount.text( count );

		$dashboardNavMenuUpdateCount.each(function( index, elem ) {
			elem.className = elem.className.replace( /count-\d+/, 'count-' + count );
		});
		$dashboardNavMenuUpdateCount.removeAttr( 'title' );
		$dashboardNavMenuUpdateCount.find( '.update-count' ).text( count );

		if ( 'plugin' === upgradeType ) {
			pluginCount = $pluginsMenuItem.find( '.plugin-count' ).eq( 0 ).text();
			pluginCount = parseInt( pluginCount, 10 ) - 1;
			if ( pluginCount < 0 || isNaN( pluginCount ) ) {
				return;
			}
			$pluginsMenuItem.find( '.plugin-count' ).text( pluginCount );
			$pluginsMenuItem.find( '.update-plugins' ).each(function( index, elem ) {
				elem.className = elem.className.replace( /count-\d+/, 'count-' + pluginCount );
			});

			if ( pluginCount > 0 ) {
				$( '.subsubsub .upgrade .count' ).text( '(' + pluginCount + ')' );
			} else {
				$( '.subsubsub .upgrade' ).remove();
			}
		}
	};

	/**
	 * Send an Ajax request to the server to update a plugin.
	 *
	 * @since 1.0.0
	 *
	 * @param {string} plugin
	 * @param {string} slug
	 */
	wp.nftmarketplacecore.updatePlugin = function( plugin, slug ) {
		var data,
				$message = $( '.nft-marketplace-single-' + slug ).find( '.update-now' ),
				name = $message.data( 'name' );

		var updatingMessage = wp.i18n.sprintf( wp.i18n.esc_html__( 'Updating %s...', 'nft-marketplace-core' ), name );
		$message.attr( 'aria-label', updatingMessage );

		$message.addClass( 'updating-message' );
		if ( $message.html() !== updatingMessage ) {
			$message.data( 'originaltext', $message.html() );
		}

		$message.text( updatingMessage );

		if ( wp.nftmarketplacecore.updateLock ) {
			wp.nftmarketplacecore.updateQueue.push({
				type: 'update-plugin',
				data: {
					plugin: plugin,
					slug: slug
				}
			});
			return;
		}

		wp.nftmarketplacecore.updateLock = true;

		data = {
			_ajax_nonce: wp.nftmarketplacecore.ajaxNonce,
			plugin: plugin,
			slug: slug,
			username: wp.nftmarketplacecore.filesystemCredentials.ftp.username,
			password: wp.nftmarketplacecore.filesystemCredentials.ftp.password,
			hostname: wp.nftmarketplacecore.filesystemCredentials.ftp.hostname,
			connection_type: wp.nftmarketplacecore.filesystemCredentials.ftp.connectionType,
			public_key: wp.nftmarketplacecore.filesystemCredentials.ssh.publicKey,
			private_key: wp.nftmarketplacecore.filesystemCredentials.ssh.privateKey
		};

		wp.ajax.post( 'update-plugin', data )
				.done( wp.nftmarketplacecore.updateSuccess )
				.fail( wp.nftmarketplacecore.updateError );
	};

	/**
	 * Send an Ajax request to the server to update a theme.
	 *
	 * @since 1.0.0
	 *
	 * @param {string} plugin
	 * @param {string} slug
	 */
	wp.nftmarketplacecore.updateTheme = function( slug ) {
		var data,
				$message = $( '.nft-marketplace-single-' + slug ).find( '.update-now' ),
				name = $message.data( 'name' );

		var updatingMessage = wp.i18n.sprintf( wp.i18n.esc_html__( 'Updating %s...', 'nft-marketplace-core' ), name );
		$message.attr( 'aria-label', updatingMessage );

		$message.addClass( 'updating-message' );
		if ( $message.html() !== updatingMessage ) {
			$message.data( 'originaltext', $message.html() );
		}

		$message.text( updatingMessage );

		if ( wp.nftmarketplacecore.updateLock ) {
			wp.nftmarketplacecore.updateQueue.push({
				type: 'update-theme',
				data: {
					theme: slug
				}
			});
			return;
		}

		wp.nftmarketplacecore.updateLock = true;

		data = {
			_ajax_nonce: wp.nftmarketplacecore.ajaxNonce,
			theme: slug,
			slug: slug,
			username: wp.nftmarketplacecore.filesystemCredentials.ftp.username,
			password: wp.nftmarketplacecore.filesystemCredentials.ftp.password,
			hostname: wp.nftmarketplacecore.filesystemCredentials.ftp.hostname,
			connection_type: wp.nftmarketplacecore.filesystemCredentials.ftp.connectionType,
			public_key: wp.nftmarketplacecore.filesystemCredentials.ssh.publicKey,
			private_key: wp.nftmarketplacecore.filesystemCredentials.ssh.privateKey
		};

		wp.ajax.post( 'update-theme', data )
				.done( wp.nftmarketplacecore.updateSuccess )
				.fail( wp.nftmarketplacecore.updateError );
	};

	/**
	 * On a successful plugin update, update the UI with the result.
	 *
	 * @since 1.0.0
	 *
	 * @param {object} response
	 */
	wp.nftmarketplacecore.updateSuccess = function( response ) {
		var $card, $updateColumn, $updateMessage, $updateVersion, name, version, versionText;

		$card = $( '.nft-marketplace-single-' + response.slug );
		$updateColumn = $card.find( '.column-update' );
		$updateMessage = $card.find( '.update-now' );
		$updateVersion = $card.find( '.version' );

		name = $updateMessage.data( 'name' );
		version = $updateMessage.data( 'version' );
		versionText = $updateVersion.attr( 'aria-label' ).replace( '%s', version );

		$updateMessage.addClass( 'disabled' );

		var updateMessage = wp.i18n.sprintf( wp.i18n.esc_html__( 'Updating %s...', 'nft-marketplace-core' ), name );
		$updateMessage.attr( 'aria-label', updateMessage );
		$updateVersion.text( versionText );

		$updateMessage.removeClass( 'updating-message' ).addClass( 'updated-message' );
		$updateMessage.text( wp.i18n.esc_html__( 'Updated!', 'nft-marketplace-core' ) );
		wp.a11y.speak( updateMessage );
		$updateColumn.addClass( 'update-complete' ).delay( 1000 ).fadeOut();

		wp.nftmarketplacecore.decrementCount( 'plugin' );

		wp.nftmarketplacecore.updateDoneSuccessfully = true;

		/*
		 * The lock can be released since the update was successful,
		 * and any other updates can commence.
		 */
		wp.nftmarketplacecore.updateLock = false;

		$( document ).trigger( 'nft-marketplace-core-update-success', response );

		wp.nftmarketplacecore.queueChecker();
	};

	/**
	 * On a plugin update error, update the UI appropriately.
	 *
	 * @since 1.0.0
	 *
	 * @param {object} response
	 */
	wp.nftmarketplacecore.updateError = function( response ) {
		var $message, name;
		wp.nftmarketplacecore.updateDoneSuccessfully = false;
		if ( response.errorCode && 'unable_to_connect_to_filesystem' === response.errorCode && wp.nftmarketplacecore.shouldRequestFilesystemCredentials ) {
			wp.nftmarketplacecore.credentialError( response, 'update-plugin' );
			return;
		}
		$message = $( '.nft-marketplace-single-' + response.slug ).find( '.update-now' );

		name = $message.data( 'name' );
		$message.attr( 'aria-label',  wp.i18n.esc_html__( 'Updating failed', 'nft-marketplace-core' ) );

		$message.removeClass( 'updating-message' );
		$message.html( wp.i18n.sprintf( wp.i18n.esc_html__( 'Updating failed %s...', 'nft-marketplace-core' ),  typeof 'undefined' !== response.errorMessage ? response.errorMessage : response.error ) );

		/*
		 * The lock can be released since this failure was
		 * after the credentials form.
		 */
		wp.nftmarketplacecore.updateLock = false;

		$( document ).trigger( 'nft-marketplace-core-update-error', response );

		wp.nftmarketplacecore.queueChecker();
	};

	/**
	 * Show an error message in the request for credentials form.
	 *
	 * @param {string} message
	 * @since 1.0.0
	 */
	wp.nftmarketplacecore.showErrorInCredentialsForm = function( message ) {
		var $modal = $( '.notification-dialog' );

		// Remove any existing error.
		$modal.find( '.error' ).remove();

		$modal.find( 'h3' ).after( '<div class="error">' + message + '</div>' );
	};

	/**
	 * Events that need to happen when there is a credential error
	 *
	 * @since 1.0.0
	 */
	wp.nftmarketplacecore.credentialError = function( response, type ) {
		wp.nftmarketplacecore.updateQueue.push({
			'type': type,
			'data': {

				// Not cool that we're depending on response for this data.
				// This would feel more whole in a view all tied together.
				plugin: response.plugin,
				slug: response.slug
			}
		});
		wp.nftmarketplacecore.showErrorInCredentialsForm( response.error );
		wp.nftmarketplacecore.requestFilesystemCredentials();
	};

	/**
	 * If an update job has been placed in the queue, queueChecker pulls it out and runs it.
	 *
	 * @since 1.0.0
	 */
	wp.nftmarketplacecore.queueChecker = function() {
		var job;

		if ( wp.nftmarketplacecore.updateLock || wp.nftmarketplacecore.updateQueue.length <= 0 ) {
			return;
		}

		job = wp.nftmarketplacecore.updateQueue.shift();

		wp.nftmarketplacecore.updatePlugin( job.data.plugin, job.data.slug );
	};

	/**
	 * Request the users filesystem credentials if we don't have them already.
	 *
	 * @since 1.0.0
	 */
	wp.nftmarketplacecore.requestFilesystemCredentials = function( event ) {
		if ( false === wp.nftmarketplacecore.updateDoneSuccessfully ) {
			wp.nftmarketplacecore.$elToReturnFocusToFromCredentialsModal = $( event.target );

			wp.nftmarketplacecore.updateLock = true;

			wp.nftmarketplacecore.requestForCredentialsModalOpen();
		}
	};

	/**
	 * Keydown handler for the request for credentials modal.
	 *
	 * Close the modal when the escape key is pressed.
	 * Constrain keyboard navigation to inside the modal.
	 *
	 * @since 1.0.0
	 */
	wp.nftmarketplacecore.keydown = function( event ) {
		if ( 27 === event.keyCode ) {
			wp.nftmarketplacecore.requestForCredentialsModalCancel();
		} else if ( 9 === event.keyCode ) {

			// #upgrade button must always be the last focusable element in the dialog.
			if ( 'upgrade' === event.target.id && ! event.shiftKey ) {
				$( '#hostname' ).focus();
				event.preventDefault();
			} else if ( 'hostname' === event.target.id && event.shiftKey ) {
				$( '#upgrade' ).focus();
				event.preventDefault();
			}
		}
	};

	/**
	 * Open the request for credentials modal.
	 *
	 * @since 1.0.0
	 */
	wp.nftmarketplacecore.requestForCredentialsModalOpen = function() {
		var $modal = $( '#request-filesystem-credentials-dialog' );
		$( 'body' ).addClass( 'modal-open' );
		$modal.show();

		$modal.find( 'input:enabled:first' ).focus();
		$modal.keydown( wp.nftmarketplacecore.keydown );
	};

	/**
	 * Close the request for credentials modal.
	 *
	 * @since 1.0.0
	 */
	wp.nftmarketplacecore.requestForCredentialsModalClose = function() {
		$( '#request-filesystem-credentials-dialog' ).hide();
		$( 'body' ).removeClass( 'modal-open' );
		wp.nftmarketplacecore.$elToReturnFocusToFromCredentialsModal.focus();
	};

	/**
	 * The steps that need to happen when the modal is canceled out
	 *
	 * @since 1.0.0
	 */
	wp.nftmarketplacecore.requestForCredentialsModalCancel = function() {
		var slug, $message;

		// No updateLock and no updateQueue means we already have cleared things up
		if ( false === wp.nftmarketplacecore.updateLock && 0 === wp.nftmarketplacecore.updateQueue.length ) {
			return;
		}

		slug = wp.nftmarketplacecore.updateQueue[0].data.slug,

				// Remove the lock, and clear the queue
				wp.nftmarketplacecore.updateLock = false;
		wp.nftmarketplacecore.updateQueue = [];

		wp.nftmarketplacecore.requestForCredentialsModalClose();
		$message = $( '.nft-marketplace-single-' + slug ).find( '.update-now' );

		$message.removeClass( 'updating-message' );
		$message.html( $message.data( 'originaltext' ) );
	};
	/**
	 * Potentially add an AYS to a user attempting to leave the page
	 *
	 * If an update is on-going and a user attempts to leave the page,
	 * open an "Are you sure?" alert.
	 *
	 * @since 1.0.0
	 */

	wp.nftmarketplacecore.beforeunload = function() {
		if ( wp.nftmarketplacecore.updateLock ) {
			return wp.i18n.esc_html__( 'Update in progress, really leave?', 'nft-marketplace-core' );
		}
	};

	$( document ).ready(function() {
		/*
		 * Check whether a user needs to submit filesystem credentials based on whether
		 * the form was output on the page server-side.
		 *
		 * @see {wp_print_request_filesystem_credentials_modal() in PHP}
		 */
		wp.nftmarketplacecore.shouldRequestFilesystemCredentials = ( $( '#request-filesystem-credentials-dialog' ).length <= 0 ) ? false : true;

		// File system credentials form submit noop-er / handler.
		$( '#request-filesystem-credentials-dialog form' ).on( 'submit', function() {

			// Persist the credentials input by the user for the duration of the page load.
			wp.nftmarketplacecore.filesystemCredentials.ftp.hostname = $( '#hostname' ).val();
			wp.nftmarketplacecore.filesystemCredentials.ftp.username = $( '#username' ).val();
			wp.nftmarketplacecore.filesystemCredentials.ftp.password = $( '#password' ).val();
			wp.nftmarketplacecore.filesystemCredentials.ftp.connectionType = $( 'input[name="connection_type"]:checked' ).val();
			wp.nftmarketplacecore.filesystemCredentials.ssh.publicKey = $( '#public_key' ).val();
			wp.nftmarketplacecore.filesystemCredentials.ssh.privateKey = $( '#private_key' ).val();

			wp.nftmarketplacecore.requestForCredentialsModalClose();

			// Unlock and invoke the queue.
			wp.nftmarketplacecore.updateLock = false;
			wp.nftmarketplacecore.queueChecker();

			return false;
		});

		// Close the request credentials modal when
		$( '#request-filesystem-credentials-dialog [data-js-action="close"], .notification-dialog-background' ).on( 'click', function() {
			wp.nftmarketplacecore.requestForCredentialsModalCancel();
		});

		// Hide SSH fields when not selected
		$( '#request-filesystem-credentials-dialog input[name="connection_type"]' ).on( 'change', function() {
			$( this ).parents( 'form' ).find( '#private_key, #public_key' ).parents( 'label' ).toggle( ( 'ssh' === $( this ).val() ) );
		}).change();

		// Click handler for plugin updates.
		$( '.nft-marketplace-single.plugin' ).on( 'click', '.update-now', function( e ) {
			var $button = $( e.target );
			e.preventDefault();

			if ( wp.nftmarketplacecore.shouldRequestFilesystemCredentials && ! wp.nftmarketplacecore.updateLock ) {
				wp.nftmarketplacecore.requestFilesystemCredentials( e );
			}

			wp.nftmarketplacecore.updatePlugin( $button.data( 'plugin' ), $button.data( 'slug' ) );
		});

		// Click handler for theme updates.
		$( '.nft-marketplace-single.theme' ).on( 'click', '.update-now', function( e ) {
			var $button = $( e.target );
			e.preventDefault();

			if ( wp.nftmarketplacecore.shouldRequestFilesystemCredentials && ! wp.nftmarketplacecore.updateLock ) {
				wp.nftmarketplacecore.requestFilesystemCredentials( e );
			}

			wp.nftmarketplacecore.updateTheme( $button.data( 'slug' ) );
		});

		// @todo
		$( '#plugin_update_from_iframe' ).on( 'click', function( e ) {
			var target, data;

			target = window.parent === window ? null : window.parent,
					$.support.postMessage = !! window.postMessage;

			if ( false === $.support.postMessage || null === target || window.parent.location.pathname.indexOf( 'update-core.php' ) !== -1 ) {
				return;
			}

			e.preventDefault();

			data = {
				'action': 'updatePlugin',
				'slug': $( this ).data( 'slug' )
			};

			target.postMessage( JSON.stringify( data ), window.location.origin );
		});
	});

	$( window ).on( 'message', function( e ) {
		var event = e.originalEvent,
				message,
				loc = document.location,
				expectedOrigin = loc.protocol + '//' + loc.hostname;

		if ( event.origin !== expectedOrigin ) {
			return;
		}

		if(event.data) {

			try {
				message = $.parseJSON(event.data);
			} catch (error) {
				message = event.data;
			}

			try {
				if ('undefined' === typeof message.action) {
					return;
				}
			} catch (error) {

			}

			try {
				switch (message.action) {
					case 'decrementUpdateCount' :
						wp.nftmarketplacecore.decrementCount(message.upgradeType);
						break;
					case 'updatePlugin' :
						tb_remove();
						$('.nft-marketplace-single-' + message.slug).find('h4 a').focus();
						$('.nft-marketplace-single-' + message.slug).find('[data-slug="' + message.slug + '"]').trigger('click');
						break;
					default:
				}
			} catch (error) {

			}
		}

	});

	$( window ).on( 'beforeunload', wp.nftmarketplacecore.beforeunload );

})( jQuery, window.wp, window.ajaxurl );
