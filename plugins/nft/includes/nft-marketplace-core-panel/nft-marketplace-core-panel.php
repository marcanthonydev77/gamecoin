<?php
/**
 * Plugin Name: NFT Marketplace Core
 * Plugin URI: https://enefti-marketplace.com/
 *
 * @package Nft_Marketplace_Core_Panel
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly.
}


/* Set plugin version constant. */
define( 'NFT_MARKETPLACE_CORE_PANEL_VERSION', '2.0.7' );

/* Debug output control. */
define( 'NFT_MARKETPLACE_CORE_PANEL_DEBUG_OUTPUT', 0 );

/* Set constant path to the plugin directory. */
define( 'NFT_MARKETPLACE_CORE_PANEL_SLUG', basename( plugin_dir_path( __FILE__ ) ) );

/* Set constant path to the main file for activation call */
define( 'NFT_MARKETPLACE_CORE_PANEL_CORE_FILE', __FILE__ );

/* Set constant path to the plugin directory. */
define( 'NFT_MARKETPLACE_CORE_PANEL_PATH', trailingslashit( plugin_dir_path( __FILE__ ) ) );

/* Set the constant path to the plugin directory URI. */
define( 'NFT_MARKETPLACE_CORE_PANEL_URI', trailingslashit( plugin_dir_url( __FILE__ ) ) );


if ( ! version_compare( PHP_VERSION, '7.3', '>=' ) ) {
	add_action( 'admin_notices', 'nft_marketplace_core_panel_fail_php_version' );
} elseif ( NFT_MARKETPLACE_CORE_PANEL_SLUG !== 'nft-marketplace-core-panel' ) {
	add_action( 'admin_notices', 'nft_marketplace_core_panel_fail_installation_method' );
} else {

	if ( ! function_exists( 'is_plugin_active_for_network' ) ) {
		// Makes sure the plugin functions are defined before trying to use them.
		require_once( ABSPATH . '/wp-admin/includes/plugin.php' );
	}
	define( 'NFT_MARKETPLACE_CORE_PANEL_NETWORK_ACTIVATED', is_plugin_active_for_network( NFT_MARKETPLACE_CORE_PANEL_SLUG . '/nft-marketplace-core-panel.php' ) );

	/* Nft_Marketplace_Core_Panel Class */
	require_once NFT_MARKETPLACE_CORE_PANEL_PATH . 'inc/class-nft-marketplace-core-panel.php';

	if ( ! function_exists( 'nft_marketplace_core_panel' ) ) :
		/**
		 * The main function responsible for returning the one true
		 * Nft_Marketplace_Core_Panel Instance to functions everywhere.
		 *
		 * Use this function like you would a global variable, except
		 * without needing to declare the global.
		 *
		 * Example: <?php $nft_marketplace_core_panel = nft_marketplace_core_panel(); ?>
		 *
		 * @since 1.0.0
		 * @return Nft_Marketplace_Core_Panel The one true Nft_Marketplace_Core_Panel Instance
		 */
		function nft_marketplace_core_panel() {
			return Nft_Marketplace_Core_Panel::instance();
		}
	endif;


	/**
	 * Loads the main instance of Nft_Marketplace_Core_Panel to prevent
	 * the need to use globals.
	 *
	 * This doesn't fire the activation hook correctly if done in 'after_setup_theme' hook.
	 *
	 * @since 1.0.0
	 * @return object Nft_Marketplace_Core_Panel
	 */
	nft_marketplace_core_panel();

}

if ( ! function_exists( 'nft_marketplace_core_panel_fail_php_version' ) ) {

	/**
	 * Show in WP Dashboard notice about the plugin is not activated.
	 *
	 * @since 2.0.0
	 *
	 * @return void
	 */
	function nft_marketplace_core_panel_fail_php_version() {
		$message      = esc_html__( 'This plugin requires PHP version 7.3+, plugin is currently NOT ACTIVE. Please contact the hosting provider to upgrade the version of PHP.', 'nft-marketplace-core' );
		$html_message = sprintf( '<div class="notice notice-error">%s</div>', wpautop( $message ) );
		echo wp_kses_post( $html_message );
	}
}



if ( ! function_exists( 'nft_marketplace_core_panel_fail_installation_method' ) ) {

	/**
	 * The plugin needs to be installed into the `nft-marketplace-core-panel/` folder otherwise it will not work correctly.
	 * This alert will display if someone has installed it into the incorrect folder (i.e. github download zip).
	 *
	 * @since 2.0.0
	 *
	 * @return void
	 */
	function nft_marketplace_core_panel_fail_installation_method() {
		$message      = sprintf( esc_html__( 'This plugin is not installed correctly. Please delete this plugin and get the correct zip file.', 'nft-marketplace-core' ), '' );
		$html_message = sprintf( '<div class="notice notice-error">%s</div>', wpautop( $message ) );
		echo wp_kses_post( $html_message );
	}
}

