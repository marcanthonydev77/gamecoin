<?php
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( class_exists( 'Nft_Marketplace_Core_Panel_Settings' ) ) {

	$nft_marketplace_core_obj = new Nft_Marketplace_Core_Panel_Settings();
	$nft_marketplace_core_obj->add_section(
		array(
			'id'    => 'nft_marketplace_core_panel_shop_page',
			'title' => esc_html__( 'NFTs Page', 'nft-marketplace-core' ),
		)
	);
	$nft_marketplace_core_obj->add_section(
		array(
			'id'    => 'nft_marketplace_core_panel_single_nft',
			'title' => esc_html__( 'Single NFT', 'nft-marketplace-core' ),
		)
	);
	$nft_marketplace_core_obj->add_section(
		array(
			'id'    => 'nft_marketplace_core_panel_styling',
			'title' => esc_html__( 'Styling', 'nft-marketplace-core' ),
		)
	);
	

	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_shop_page',
		array(
			'id'   => 'nft_marketplace_core_listings_title',
			'type' => 'title',
			'name' => '<h3>'.esc_html__('NFT Listing Options','nft-marketplace-core').'</h3>',
		)
	);
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_shop_page',
		array(
			'id'      => 'nft_marketplace_core_items_per_row',
			'type'    => 'select',
			'name'    => esc_html__( 'Items per row', 'nft-marketplace-core' ),
			'options' => array(
				'col-md-6'  => esc_html__( '2 NFTs/Row','nft-marketplace-core'),
				'col-md-4'  => esc_html__( '3 NFTs/Row','nft-marketplace-core'),
				'col-md-3'  => esc_html__( '4 NFTs/Row','nft-marketplace-core'),
			),
			'desc'    => esc_html__( 'Number of NFTs displayed on one row.', 'nft-marketplace-core' ),
		)
	);
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_shop_page',
		array(
			'id'                => 'nft_marketplace_core_items_per_page',
			'type'              => 'number',
			'name'              => esc_html__( 'Items per page', 'nft-marketplace-core' ),
			'default'           => 9,
			'sanitize_callback' => 'intval',
			'desc'    => esc_html__( 'Number of NFTs displayed on Shop Page.', 'nft-marketplace-core' ),
		)
	);

	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_shop_page',
		array(
			'id'   => 'nft_marketplace_core_sidebar_title',
			'type' => 'title',
			'name' => '<h3>'.esc_html__('Sidebar Options','nft-marketplace-core').'</h3>',
		)
	);

	$options = $nft_marketplace_core_obj->nft_marketplace_core_sidebar_select();
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_shop_page',
		array(
			'id'      => 'nft_marketplace_core_sidebar',
			'type'    => 'checkbox',
			'name'    => esc_html__( 'Shop Sidebar', 'nft-marketplace-core' ),
			'desc'    => esc_html__( 'Enable/Disable the Sidebar from the Shop Page.', 'nft-marketplace-core' ),
			'default' => 'on'
		)
	);
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_shop_page',
		array(
			'id'      => 'nft_marketplace_core_select_sidebar',
			'type'    => 'sidebars',
			'name'    => esc_html__( 'Select Sidebar', 'nft-marketplace-core' ),
			'desc'    => esc_html__( 'Enable/Disable the Sidebar from the Shop Page.', 'nft-marketplace-core' ),
			'options' => $options,
		)
	);
	
	/*SINGLE NFT*/
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_single_nft',
		array(
			'id'   => 'nft_marketplace_core_single_listing_title',
			'type' => 'title',
			'name' => '<h3>'.esc_html__('Page Options','nft-marketplace-core').'</h3>',
		)
	);
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_single_nft',
		array(
			'id'      => 'nft_marketplace_core_views',
			'type'    => 'checkbox',
			'name'    => esc_html__( 'Views', 'nft-marketplace-core' ),
			'desc'    => esc_html__( 'Enable/Disable the Views counter.', 'nft-marketplace-core' ),
			'default' => 'on'
		)
	);
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_single_nft',
		array(
			'id'      => 'nft_marketplace_core_likes',
			'type'    => 'checkbox',
			'name'    => esc_html__( 'Likes', 'nft-marketplace-core' ),
			'desc'    => esc_html__( 'Enable/Disable the Likes counter.', 'nft-marketplace-core' ),
			'default' => 'on'
		)
	);
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_single_nft',
		array(
			'id'      => 'nft_marketplace_core_price_usd',
			'type'    => 'checkbox',
			'name'    => esc_html__( 'Regular Price', 'nft-marketplace-core' ),
			'desc'    => esc_html__( 'Enable/Disable the Regular Price.', 'nft-marketplace-core' ),
			'default' => 'on'
		)
	);
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_single_nft',
		array(
			'id'      => 'nft_marketplace_core_related',
			'type'    => 'checkbox',
			'name'    => esc_html__( 'Related NFTs', 'nft-marketplace-core' ),
			'desc'    => esc_html__( 'Enable/Disable the Related NFTs section.', 'nft-marketplace-core' ),
			'default' => 'on'
		)
	);
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_single_nft',
		array(
			'id'   => 'nft_marketplace_core_single_tabs_title',
			'type' => 'title',
			'name' => '<h3>'.esc_html__('Tabs Options','nft-marketplace-core').'</h3>',
		)
	);
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_single_nft',
		array(
			'id'      => 'nft_marketplace_core_tabs',
			'type'    => 'select',
			'name'    => esc_html__( 'Tabs Position', 'nft-marketplace-core' ),
			'options' => array(
				'top' => esc_html__( 'Aside Image','nft-marketplace-core'),
				'bellow'  => esc_html__( 'Below Image','nft-marketplace-core'),
			),
			'desc'    => esc_html__( 'Change position from aside to below image.', 'nft-marketplace-core' ),
		)
	);

	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_styling',
		array(
			'id'   => 'nft_marketplace_core_styling_dark',
			'type' => 'title',
			'name' => '<h3>'.esc_html__('Dark Mode','nft-marketplace-core').'</h3>',
		)
	);
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_styling',
		array(
			'id'      => 'nft_marketplace_core_dark',
			'type'    => 'checkbox',
			'name'    => esc_html__( 'Dark Mode', 'nft-marketplace-core' ),
			'desc'    => esc_html__( 'Enable/Disable dark mode on NFT pages.', 'nft-marketplace-core' ),
			'default' => 'off'
		)
	);

	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_styling',
		array(
			'id'   => 'nft_marketplace_core_styling_links',
			'type' => 'title',
			'name' => '<h3>'.esc_html__('Links','nft-marketplace-core').'</h3>',
		)
	);
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_styling',
		array(
			'id'          => 'nft_marketplace_core_main_text_color',
			'type'        => 'color',
			'name'        => esc_html__( 'Links Color', 'nft-marketplace-core' ),
			'desc'        => esc_html__( 'Change the link color for NFT pages.', 'nft-marketplace-core' ),
			'placeholder' => esc_html__( '#D01498', 'nft-marketplace-core' )
		)
	);
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_styling',
		array(
			'id'          => 'nft_marketplace_core_main_hover_color',
			'type'        => 'color',
			'name'        => esc_html__( 'Links Hover Color', 'nft-marketplace-core' ),
			'desc'        => esc_html__( 'Change the links hover color for NFT pages.', 'nft-marketplace-core' ),
			'placeholder' => esc_html__( '#D01498', 'nft-marketplace-core' )
		)
	);
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_styling',
		array(
			'id'   => 'nft_marketplace_core_styling_button',
			'type' => 'title',
			'name' => '<h3>'.esc_html__('Buttons','nft-marketplace-core').'</h3>',
		)
	);
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_styling',
		array(
			'id'          => 'nft_marketplace_core_main_button_bg',
			'type'        => 'color',
			'name'        => esc_html__( 'Buttons Background Color', 'nft-marketplace-core' ),
			'desc'        => esc_html__( 'Change the buttons background for NFT pages.', 'nft-marketplace-core' ),
			'placeholder' => esc_html__( '#D01498', 'nft-marketplace-core' )
		)
	);
	$nft_marketplace_core_obj->add_field(
		'nft_marketplace_core_panel_styling',
		array(
			'id'          => 'nft_marketplace_core_main_button_bg_hover',
			'type'        => 'color',
			'name'        => esc_html__( 'Buttons Background HoverColor', 'nft-marketplace-core' ),
			'desc'        => esc_html__( 'Change the buttons hover background for NFT pages.', 'nft-marketplace-core' ),
			'placeholder' => esc_html__( '#222', 'nft-marketplace-core' )
		)
	);

}
