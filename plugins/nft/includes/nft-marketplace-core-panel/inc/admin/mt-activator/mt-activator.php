<?php
require 'Vite.php';

// render form
function  mta_license_manager_render_plugin_settings_page($tid) {
    ?>
    <div id="app"
         data-nonce="<?php echo wp_create_nonce( 'wp_rest' );?>"
         data-code="<?php echo esc_attr(json_encode(get_option("modelthemeAPIactivator")));?>"
         data-install="<?php echo esc_url(get_site_url());?>"
         data-api-location="<?php echo esc_url(get_rest_url());?>"
         data-tid="<?php echo esc_attr(json_encode($tid));?>"
    >
        <noscript><?php echo esc_html__('You need javascript enabled to see this content', 'nft-marketplace-core'); ?>.</noscript>
        <?php echo esc_html__('Loading...', 'nft-marketplace-core'); ?>
    </div>
    <?php

    $vite = new Mta_Vite();
    echo wp_kses($vite, 'html');

}