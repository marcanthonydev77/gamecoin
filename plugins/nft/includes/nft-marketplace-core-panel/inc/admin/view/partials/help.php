<?php
/**
 * Help
 */

?>
<?php 
$request = wp_remote_get('https://enefti-marketplace.com/nft-marketplace-api/help.json');
$json = json_decode(wp_remote_retrieve_body($request), true); ?>

<div id="help" class="panel">
	<div class="nft-marketplace-core-panel-blocks">
		<?php $count = 0; ?>
		<?php foreach ( $json as $slug => $block ) { ?>
			<?php if($count >= 3) { ?>
				<a class="nft-marketplace-core-panel-ad-row-url" href="<?php echo esc_url($block['block_link']); ?>" target="_blank">
				<div class="nft-marketplace-core-panel-ad-row" style="background:url('<?php echo esc_url($block['block_bg']); ?>;')">
					<img src="<?php echo esc_html($block['icon']);?>">
					<div class="nft-ad-wrapper">
						<h3><?php echo esc_html($block['block_title']);?></h3>
						<p><?php echo esc_html($block['block_subtitle']);?></p>
					</div>
					<button href="#"><?php echo esc_html($block['block_button']);?></button>
				</div>
			</a>
			<?php }else{?>
			<div class="nft-marketplace-core-panel-2-column">
				<div class="clickable-box">
					<a href="<?php echo esc_url($block['block_link']); ?>" target="_blank">
						<span class="dashicons <?php echo esc_attr($block['icon']);?>"></span>
						<h3><?php echo esc_html($block['block_title']);?></h3>
						<p><?php echo esc_html($block['block_subtitle']);?></p>
						</a>
				</div>
			</div>
			<?php $count++; }
		} ?>	
	</div>

</div>
