<?php
/**
 * Intro partial
 *
 * @package Nft_Marketplace_Core_Panel
 * @since 1.0.0
 */

?>
<div class="col">
	<h1 class="about-title"><img class="about-logo" src="<?php echo nft_marketplace_core_panel()->get_plugin_url(); ?>images/nft-logo.png" alt="NFT Marketplace Core"></h1>
</div>