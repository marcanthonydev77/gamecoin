<?php
/**
 * Plugins panel partial
 *
 * @package Nft_Marketplace_Core_Panel
 * @since 1.0.0
 */

?>
<div id="plugins" class="panel">
	<div class="nft-marketplace-core-panel-blocks">
		<?php
		nft_marketplace_core_panel_plugins_column( 'install' );
		?>
	</div>
</div>
