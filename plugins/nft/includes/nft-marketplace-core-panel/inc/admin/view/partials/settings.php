<?php
/**
 * Settings panel partial
 *
 * @package Nft_Marketplace_Core_Panel
 * @since 1.0.0
 */

$token = nft_marketplace_core_panel()->get_option( 'token' );
$items = nft_marketplace_core_panel()->get_option( 'items', array() );

?>
<div id="settings" class="panel">
	<div class="nft-marketplace-core-panel-blocks">
		<?php settings_fields( nft_marketplace_core_panel()->get_slug() ); ?>
		<?php Nft_Marketplace_Core_Panel_Admin::do_api_settings_sections( nft_marketplace_core_panel()->get_slug(), 2 ); ?>
	</div>
	<p class="submit">
		<input type="submit" name="submit" id="submit" class="button button-primary" value="<?php esc_html_e( 'Save Changes', 'nft-marketplace-core' ); ?>" />
		<?php if ( ( '' !== $token || ! empty( $items ) ) && 10 !== has_action( 'admin_notices', array( $this, 'error_notice' ) ) ) { ?>
			<a href="<?php echo esc_url( add_query_arg( array( 'authorization' => 'check' ), nft_marketplace_core_panel()->get_page_url() ) ); ?>" class="button button-secondary auth-check-button" style="margin:0 5px"><?php esc_html_e( 'Test API Connection', 'nft-marketplace-core' ); ?></a>
		<?php } ?>
	</p>
</div>
