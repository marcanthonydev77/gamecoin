<?php
/**
 * Settings panel partial
 *
 * @package Nft_Marketplace_Core_Panel
 * @since 1.0.0
 */
?>
<div id="general" class="panel" style="display:block;">
	<?php 
        do_action('nft_show_navigation');
        do_action('nft_show_forms');
    ?> 
</div>
