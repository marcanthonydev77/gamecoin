<?php
/**
 * Themes panel partial
 *
 * @package Nft_Marketplace_Core_Panel
 * @since 1.0.0
 */

?>
<div id="themes" class="panel">
	<div class="nft-marketplace-core-panel-blocks">
		<?php
			nft_marketplace_core_panel_themes_column();
		?>
	</div>
</div>
