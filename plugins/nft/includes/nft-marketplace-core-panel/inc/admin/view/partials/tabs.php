<?php
/**
 * Tabs partial
 *
 */

$tab     = isset( $_GET['tab'] ) ? sanitize_key( wp_unslash( $_GET['tab'] ) ) : '';
$general_class = array();
if ( empty( $tab ) ) {
	$tab = 'themes';
}
if ( 'themes' === $tab ) {
		$general_class[] = 'nav-tab-active';
}
$theme_class = array();
if ( empty( $tab ) ) {
	$tab = 'themes';
}
if ( 'themes' === $tab ) {
	$theme_class[] = '';
}
$plugin_class = array();
if ( empty( $tab ) ) {
	$tab = 'plugins';
}
if ( 'plugins' === $tab ) {
	$plugin_class[] = 'nav-tab-active';
}

?>

<div class="nav-tab-wrapper">
	<h1 class="about-title"><img class="about-logo" src="<?php echo nft_marketplace_core_panel()->get_plugin_url(); ?>images/nft-logo.png" alt="NFT Marketplace Core"></h1>
	<?php
	echo '<a href="#general" class="panel-tab '.esc_attr(implode(' ',$general_class)).'"><span class="dashicons dashicons-admin-generic"></span> '.esc_html__( 'Settings', 'nft-marketplace-core' ).'</a>';
	echo '<a href="#themes" data-id="theme" class="panel-tab '.esc_attr(implode(' ',$theme_class)).'"><span class="dashicons dashicons-admin-appearance"></span> '.esc_html__('Themes','nft-marketplace-core').'</a>';
	echo '<a href="#plugins" data-id="plugin" class="panel-tab '.esc_attr(implode(' ',$plugin_class)).'"><span class="dashicons dashicons-admin-plugins"></span> '.esc_html__('Plugins','nft-marketplace-core').'</a>';
	echo '<a href="#help" class="panel-tab '.esc_attr('help' === $tab ? 'nav-tab-active' : '').'"><span class="dashicons dashicons-editor-help"></span> '.esc_html__( 'Help', 'nft-marketplace-core' ).'</a>';
	?>
</div>
