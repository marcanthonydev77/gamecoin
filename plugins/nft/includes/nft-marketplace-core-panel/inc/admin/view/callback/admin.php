<?php
/**
 * Admin UI
 *
 * @package Nft_Marketplace_Core_Panel
 * @since 1.0.0
 */

if ( isset( $_GET['action'] ) ) {
	$id = ! empty( $_GET['id'] ) ? absint( trim( $_GET['id'] ) ) : '';

	if ( 'install-plugin' === $_GET['action'] ) {
		Nft_Marketplace_Core_Panel_Admin::install_plugin( $id );
	} elseif ( 'install-theme' === $_GET['action'] ) {
		Nft_Marketplace_Core_Panel_Admin::install_theme( $id );
	}
} else {
	add_thickbox(); ?>
	<div class="wrap about-wrap full-width-layout">	
		<?php Nft_Marketplace_Core_Panel_Admin::render_intro_partial(); ?>
		<?php $status = get_option("modelthemeAPIactivator");
		if(get_option("modelthemeAPIactivator") !== false) {
			$ch = curl_init();
			$productID = 36258540;
            curl_setopt($ch, CURLOPT_URL, "http://api.modeltheme.com/activator/license-status.php");
           	curl_setopt($ch, CURLOPT_POST, 1);
           	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode([
                    "key" => get_option("modelthemeAPIactivator")[0],
                    "productId" => $productID,
                    "data" => [
                        "user" => [
                            ["value" => get_option("siteurl")],
                            ["value" => get_option("admin_email")]
                        ]
                    ]
                ], JSON_UNESCAPED_SLASHES));

                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

                $server_output = curl_exec($ch);

                curl_close($ch);
                $state = json_decode($server_output, true);
				$state["success"] = true;
                if ($state["success"] !== false) { ?>
					<div class="nft-panel-wrapper">
						<?php Nft_Marketplace_Core_Panel_Admin::render_tabs_partial(); ?>
						<?php Nft_Marketplace_Core_Panel_Admin::render_general_panel_partial(); ?>
						<form method="POST" action="<?php echo esc_url( NFT_MARKETPLACE_CORE_PANEL_NETWORK_ACTIVATED ? network_admin_url( 'edit.php?action=nft_marketplace_core_panel_network_settings' ) : admin_url( 'options.php' ) ); ?>">
							<?php Nft_Marketplace_Core_Panel_Admin::render_themes_panel_partial(); ?>
							<?php Nft_Marketplace_Core_Panel_Admin::render_plugins_panel_partial(); ?>
							<?php Nft_Marketplace_Core_Panel_Admin::render_help_panel_partial(); ?>
						</form>
					</div>
		<?php } } ?>
		<div id="activator" class="panel-activator">
			<?php
			$adrr=str_replace('admin',"",plugin_dir_path(__DIR__));
			
            require str_replace("view/","",$adrr).'/mt-activator/Vite.php';

            // render form
            function  nft_marketplace_core_render_plugin_settings_page($tid) {
                ?>
                <div id="app"
                     data-nonce="<?php echo wp_create_nonce( 'wp_rest' );?>"
                     data-code="<?php echo esc_attr(json_encode(get_option("modelthemeAPIactivator")));?>"
                     data-install="<?php echo esc_url(get_site_url());?>"
                     data-api-location="<?php echo esc_url(get_rest_url());?>"
                     data-tid="<?php echo esc_attr(json_encode($tid));?>"
                >
                    <noscript><?php echo esc_html__('You need javascript enabled to see this content', 'nft-marketplace-core'); ?>.</noscript>
                    <?php echo esc_html__('Loading...', 'nft-marketplace-core'); ?>
                </div>
                <?php
            }
            $vite = new Mta_Vite();
            echo wp_kses($vite, 'vite');
			//fix: changed function
			    nft_marketplace_core_render_plugin_settings_page(ReduxFramework_extension_mt_activator::$productID);
			 ?>
		</div>
	</div>
	<?php
}
