<?php
/**
 * Token setting
 *
 * @package Nft_Marketplace_Core_Panel
 * @since 1.0.0
 */

?>
<input type="text" name="<?php echo esc_attr( nft_marketplace_core_panel()->get_option_name() ); ?>[token]" class="widefat" value="<?php echo esc_html( nft_marketplace_core_panel()->get_option( 'token' ) ); ?>" autocomplete="off">

<p class="description"><?php esc_html_e( 'Enter your Envato API Personal Token.', 'nft-marketplace-core' ); ?></p>
