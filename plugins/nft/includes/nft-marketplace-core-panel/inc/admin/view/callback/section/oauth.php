<?php
/**
 * OAuth section
 *
 * @package Nft_Marketplace_Core_Panel
 * @since 1.0.0
 */

?>
<div class="nft-marketplace-core-note-token">
    <ol>
        <li><?php echo esc_html__('Generate an Envato Token by going at','nft-marketplace-core'); ?><a href="https://build.envato.com/create-token/?default=t&purchase:download=t&purchase:list=t" target="_blank"><?php esc_html_e(" this link.", "nft-marketplace-core"); ?></a></li>
        <li><?php echo esc_html__('Give the token a name.','nft-marketplace-core') ?></li>
        <li><?php echo esc_html__('Ensure the following permissions are enabled:','nft-marketplace-core') ?></li>
            <ul>
                <li><?php echo esc_html__('View and search Envato sites','nft-marketplace-core'); ?></li>
                <li><?php echo esc_html__('Download your purchased items','nft-marketplace-core'); ?></li>
                <li><?php echo esc_html__('List purchases you have made','nft-marketplace-core'); ?></li>
                <li><?php echo esc_html__('Copy the token into the box below','nft-marketplace-core'); ?></li>
            </ul>
        <li><?php echo esc_html__('Click the "Save Changes" button.','nft-marketplace-core') ?></li>
    </ol>
</div>
<p>
	<?php esc_html_e( 'Please enter your Envato API token to activate the NFT Marketplace Core.', 'nft-marketplace-core' ); ?>
</p>
