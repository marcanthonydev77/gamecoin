<?php
/**
 * Success notice
 *
 * @package Nft_Marketplace_Core_Panel
 * @since 1.0.0
 */

?>
<div class="notice notice-success is-dismissible">
	<p><?php esc_html_e( 'All Single Use OAuth Personal Tokens have been verified.', 'nft-marketplace-core' ); ?></p>
</div>
