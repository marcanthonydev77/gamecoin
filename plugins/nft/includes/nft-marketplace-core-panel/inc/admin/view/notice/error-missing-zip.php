<?php
/**
 * Error notice
 *
 * @package Nft_Marketplace_Core_Panel
 * @since 2.0.1
 */

?>
<div class="notice notice-error is-dismissible">
	<p><?php printf( esc_html__( 'Failed to locate the package file for this item. Please contact the item author for support, or install/upgrade the item manually from the %s.', 'nft-marketplace-core' ), '<a href="https://themeforest.net/downloads" target="_blank">downloads page</a>' ); ?></p>
</div>
