<?php
/**
 * Success notice
 *
 * @package Nft_Marketplace_Core_Panel
 * @since 1.0.0
 */

?>
<div class="notice notice-success is-dismissible">
	<p><?php esc_html_e( 'Your OAuth Personal Token has been verified. However, there are no WordPress downloadable items in your account.', 'nft-marketplace-core' ); ?></p>
</div>
