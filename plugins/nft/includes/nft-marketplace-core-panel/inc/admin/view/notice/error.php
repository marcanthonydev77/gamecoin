<?php
/**
 * Error notice
 *
 * @package Nft_Marketplace_Core_Panel
 * @since 1.0.0
 */

?>
<div class="notice notice-error is-dismissible">
	<p><?php esc_html_e( 'The OAuth Personal Token could not be verified. Please check that the Token has been entered correctly and has the minimum required permissions.', 'nft-marketplace-core' ); ?></p>
</div>
