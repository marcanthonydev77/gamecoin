<?php
/**
 * Error notice
 *
 * @package Nft_Marketplace_Core_Panel
 * @since 1.0.0
 */

?>
<div class="notice notice-error is-dismissible">
	<p><?php esc_html_e( 'One or more Single Use OAuth Personal Tokens could not be verified and should be removed.', 'nft-marketplace-core' ); ?></p>
</div>
