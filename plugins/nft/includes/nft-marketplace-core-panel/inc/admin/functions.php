<?php
/**
 * Functions
 *
 * @package Nft_Marketplace_Core_Panel
 */

/**
 * Interate over the themes array and displays each theme.
 *
 * @since 1.0.0
 *
 */
function nft_marketplace_core_panel_themes_column($group = 'install') {
	$request = wp_remote_get('https://enefti-marketplace.com/nft-marketplace-api/themes.json');
    $json = json_decode(wp_remote_retrieve_body($request), true);


	$premium = nft_marketplace_core_panel()->items()->themes($group);
	$current_theme = '';
	$current_theme =  wp_get_theme();

	$get_all_themes = array();
	$themes = wp_get_themes();
	foreach ($themes as $theme_1) {
		$get_all_themes[]  = $theme_1->get( 'Name' );
	}
	$get_all_purchases = array();
	foreach ($premium as $purchased_theme) {
		$get_all_purchases[]  = $purchased_theme['name'];
	}
	foreach ( $json as $slug => $theme ) :
		$name               = $theme['name'];
		$version            = $theme['version'];
		$description        = $theme['description'];
		$url                = $theme['url'];
		$docs_url           = $theme['docs_url'];
		$theme['hasUpdate'] = false;

		if ( $theme['name'] == $current_theme->get( 'Name' )) {
			if ( $current_theme->exists() ) {
				$name        = $current_theme->get( 'Name' );
				$version     = $current_theme->get( 'Version' );
				if ( version_compare( $version, $theme['version'], '<' ) ) {
					$theme['hasUpdate'] = true;
				}
			}
		}

		?>
		<div class="nft-marketplace-core-panel-block" data-id="<?php echo esc_attr( $theme['id'] ); ?>">
			<?php $update_cls = '';
				if ( $theme['hasUpdate'] === true) {
					$update_cls = 'update';
					if ( current_user_can( 'update_themes' ) ) {
						$slug = strtolower($theme['name']);
						$upgrade_link = add_query_arg(
							array(
								'action' => 'upgrade-theme',
								'theme'  => esc_attr( $slug ),
							),
							self_admin_url( 'update.php' )
						);

						echo '<div class="panel-update-wrapper">';
							echo sprintf(
								'<a class="update-now button" href="%1$s" aria-label="%2$s" data-name="%3$s %5$s" data-slug="%4$s" data-version="%5$s">%6$s</a>',
								wp_nonce_url( $upgrade_link, 'upgrade-theme_' . $slug ),
								esc_attr__( 'Update %s now', 'nft-marketplace-core' ),
								esc_attr( $name ),
								esc_attr( $slug ),
								esc_attr( $theme['version'] ),
								esc_html__( 'Update Available', 'nft-marketplace-core')
							);
						echo '</div>';
					}
				}
			?>
			<div class="nft-marketplace-single theme <?php echo esc_attr($update_cls);?>">
				<div class="panel-top">
					<div class="panel-theme-featured-img">
						<a href="<?php echo esc_url( $url ); ?>" target="_blank"><img src="<?php echo esc_url( $theme['landscape_url'] ); ?>"/></a>
					</div>
					<div class="panel-theme-name">
						<h4>
							<a href="<?php echo esc_url( $url ); ?>" target="_blank"><?php echo esc_html( $name ); ?></a>
							<span class="version" aria-label="<?php esc_attr_e( 'Version %s', 'nft-marketplace-core' ); ?>">
								<?php echo esc_html( sprintf( esc_html__( 'Version %s', 'nft-marketplace-core' ), $version ) ); ?>
							</span>
						</h4>
					</div>
					<div class="panel-theme-name">
						<div class="description">
							<?php echo wp_kses_post( wpautop( strip_tags( $description ) ) ); ?>
						</div>
					</div>
					<?php if ( ! empty( $update_actions ) ) { ?>
						<div class="column-update">
							<?php echo implode( "\n", $update_actions ); ?>
						</div>
					<?php } ?>
				</div>
				<div class="panel-bottom">
					<div class="panel-buttons"><?php 
						if ( $theme['name'] == $current_theme->get( 'Name' )) {
						$slug = strtolower($theme['name']);
						// Customize theme.
						$customize_url        = admin_url( 'customize.php' );
						$customize_url       .= '?theme=' . urlencode( $slug );
						$customize_url       .= '&return=' . urlencode( nft_marketplace_core_panel()->get_page_url() . '#themes' );
						echo '<a href="' . esc_url( $customize_url ) . '" class="button button-primary load-customize hide-if-no-customize"><span aria-hidden="true">' . esc_html__( 'Customize', 'nft-marketplace-core' ) . '</span><span class="screen-reader-text">' . sprintf(esc_html__( 'Customize &#8220;%s&#8221;', 'nft-marketplace-core' ), $name ) . '</span></a>';
					} else if ($current_theme->get( 'Name' ) != $get_all_themes and in_array($theme['name'], $get_all_themes)) { 
						$slug = strtolower($theme['name']);
						$activate_link = add_query_arg(
							array(
								'action'     => 'activate',
								'stylesheet' => urlencode( $slug ),
							),
							admin_url( 'themes.php' )
						);
						$activate_link = wp_nonce_url( $activate_link, 'switch-theme_' . $slug );

						// Activate link.
						echo '<a href="' . esc_url( $activate_link ) . '" class="button"><span aria-hidden="true">' . esc_html__( 'Activate', 'nft-marketplace-core' ) . '</span><span class="screen-reader-text">' . sprintf( esc_html__( 'Activate &#8220;%s&#8221;', 'nft-marketplace-core' ), $name ) . '</span></a>';

						// Preview theme.
						if ( current_user_can( 'edit_theme_options' ) && current_user_can( 'customize' ) ) {
							$preview_url                  = admin_url( 'customize.php' );
							$preview_url                 .= '?theme=' . urlencode( $slug );
							$preview_url                 .= '&return=' . urlencode( nft_marketplace_core_panel()->get_page_url() . '#themes' );
							echo '<a href="' . esc_url( $preview_url ) . '" class="button button-primary load-customize hide-if-no-customize"><span aria-hidden="true">' . esc_html__( 'Live Preview', 'nft-marketplace-core' ) . '</span><span class="screen-reader-text">' . sprintf( esc_html__( 'Live Preview &#8220;%s&#8221;', 'nft-marketplace-core' ), $name ) . '</span></a>';
						}
					} else if(in_array($theme['name'], $get_all_purchases)){
						$install_link = add_query_arg(
							array(
								'page'   => nft_marketplace_core_panel()->get_slug(),
								'action' => 'install-theme',
								'id'     => $theme['id'],
							),
							self_admin_url( 'admin.php' )
						);

						echo '
						<a href="' . wp_nonce_url( $install_link, 'install-theme_' . $theme['id'] ) . '" class="button button-primary">
							<span aria-hidden="true">' . esc_html__( 'Install', 'nft-marketplace-core' ) . '</span>
							<span class="screen-reader-text">' . sprintf( esc_html__( 'Install %s', 'nft-marketplace-core' ), $name ) . '</span>
						</a>';
					} else { 
						// Install link.
						$install_link = add_query_arg(
							array(
								'page'   => nft_marketplace_core_panel()->get_slug(),
								'action' => 'install-theme',
								'id'     => $theme['id'],
							),
							self_admin_url( 'admin.php' )
						);

						echo '
						<a href="' . $theme['url'] . '" target="_blank" class="button button-primary">
							<span aria-hidden="true">' . esc_html__( 'Get Theme', 'nft-marketplace-core' ) . '</span>
							<span class="screen-reader-text">' . sprintf( esc_html__( 'Get Theme %s', 'nft-marketplace-core' ), $name ) . '</span>
						</a>';
					}
					echo '
					<a href="' . $theme['docs_url'] . '" target="_blank" class="button button-primary">
						<span aria-hidden="true">' .esc_html__( 'Docs', 'nft-marketplace-core' ) . '</span>
						<span class="screen-reader-text">' . sprintf( esc_html__( 'Check Docs %s', 'nft-marketplace-core' ), $name ) . '</span>
					</a>';
					?>
					</div>
				</div>
			</div>
		</div>
		<?php
	endforeach;
}

/**
 * Interate over the plugins array and displays each plugin.
 */

function nft_marketplace_core_panel_plugins_column( $group = 'install' ) {
	$premium = nft_marketplace_core_panel()->items()->plugins( $group );
	$request = wp_remote_get('https://enefti-marketplace.com/nft-marketplace-api/plugins.json');
    $json = json_decode(wp_remote_retrieve_body($request), true);

	$plugins = nft_marketplace_core_panel()->items()->wp_plugins();

	$get_all_purchases = array();
	foreach ($premium as $purchased_plugin) {
		$get_all_purchases[]  = $purchased_plugin['name'];
	}

	$current_plugins = array_keys($plugins);

	foreach ( $json as $slug => $plugin ) :
		$name                = $plugin['name'];
		$version             = $plugin['version'];
		$description         = $plugin['description'];
		$url                 = $plugin['url'];
		$plugin_path         = $plugin['plugin_path'];
		$docs_url            = $plugin['docs_url'];
		$plugin['hasUpdate'] = false;

		?>
		<div class="nft-marketplace-core-panel-block" data-id="<?php echo esc_attr( $plugin['id'] ); ?>">
			<div class="nft-marketplace-single plugin">
				<div class="panel-top">
					<div class="panel-theme-featured-img">
						<a href="<?php echo esc_url( $url ); ?>" target="_blank"><img src="<?php echo esc_url( $plugin['landscape_url'] ); ?>"/></a>
					</div>
					<div class="panel-theme-name">
						<h4>
							<a href="<?php echo esc_url( $url ); ?>" target="_blank"><?php echo esc_html($name); ?></a>
							<span class="version" aria-label="<?php esc_attr_e( 'Version %s', 'nft-marketplace-core' ); ?>">
								<?php echo esc_html( sprintf(esc_html__( 'Version %s', 'nft-marketplace-core' ), ( isset( $plugins[ $slug ] ) ? $plugins[ $slug ]['Version'] : $version ) ) ); ?>
							</span>
						</h4>
					</div>
					<div class="panel-theme-description">
						<div class="description">
							<?php echo wp_kses_post( wpautop( strip_tags( $description ) ) ); ?>
						</div>
					</div>
					<?php if ( ! empty( $update_actions ) ) { ?>
						<div class="column-update">
							<?php echo implode( "\n", $update_actions ); ?>
						</div>
					<?php } ?>
				</div>
				<div class="panel-bottom">
					<div class="panel-buttons">
						<?php if(in_array($plugin_path, $current_plugins)) {
							if ( !is_plugin_active( $plugin_path ) ) {
								$activate_link = add_query_arg(
									array(
										'action' => 'activate',
										'plugin' => $plugin_path,
									),
									self_admin_url( 'plugins.php' )
								);

								echo '<a href="' .wp_nonce_url( $activate_link, 'activate-plugin_' . $plugin_path ) .'" class="button">
										<span aria-hidden="true">' . esc_html__( 'Activate', 'nft-marketplace-core' ).'</span>
									</a>';
							} else {
								$deactivate_link = add_query_arg(
									array(
										'action' => 'deactivate',
										'plugin' => $plugin_path,
									),
									self_admin_url( 'plugins.php' )
								);

								echo '<a href="' . wp_nonce_url( $deactivate_link, 'deactivate-plugin_' . $plugin_path ) . '" class="button">
										<span aria-hidden="true">'.esc_html__( 'Deactivate', 'nft-marketplace-core' ).'</span>
									</a>';
							}
						} else if(in_array($plugin['name'], $get_all_purchases)){
							$install_link = add_query_arg(
								array(
									'page'   => nft_marketplace_core_panel()->get_slug(),
									'action' => 'install-plugin',
									'id'     => $plugin['id'],
								),
								self_admin_url( 'admin.php' )
							);

							echo '<a href="' . wp_nonce_url( $install_link, 'install-plugin_' . $plugin['id'] ) . '" class="button button-primary">
									<span aria-hidden="true">'.esc_html__( 'Install', 'nft-marketplace-core' ).'</span>
								</a>';

						} else {
							$install_link = add_query_arg(
								array(
									'page'   => nft_marketplace_core_panel()->get_slug(),
									'action' => 'install-plugin',
									'id'     => $plugin['id'],
								),
								self_admin_url( 'admin.php' )
							);

							echo '<a href="'.esc_url($plugin['url']).'" target="_blank" class="button button-primary">
									<span aria-hidden="true">'.esc_html__( 'Get Plugin', 'nft-marketplace-core' ).'</span>
								</a>';
						} 
						echo '
						<a href="' . esc_url($plugin['docs_url']) . '" target="_blank" class="button button-primary">
							<span aria-hidden="true">' .esc_html__( 'Docs', 'nft-marketplace-core' ) . '</span>
							<span class="screen-reader-text">' . sprintf( esc_html__( 'Check Docs %s', 'nft-marketplace-core' ), $name ) . '</span>
						</a>';
						?>
					</div>
				</div>
			</div>
		</div>
		<?php
	endforeach;
}
