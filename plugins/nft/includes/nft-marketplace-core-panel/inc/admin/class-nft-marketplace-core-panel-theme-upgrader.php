<?php
/**
 * Theme Upgrader class.
 *
 * @package Nft_Marketplace_Core_Panel
 */

// Include the WP_Upgrader class.
if ( ! class_exists( 'WP_Upgrader', false ) ) :
	include_once ABSPATH . 'wp-admin/includes/class-wp-upgrader.php';
endif;

if ( ! class_exists( 'Nft_Marketplace_Core_Panel_Theme_Upgrader' ) ) :

	/**
	 * Extends the WordPress Theme_Upgrader class.
	 *
	 * This class makes modifications to the strings during install & upgrade.
	 *
	 * @class Nft_Marketplace_Core_Panel_Plugin_Upgrader
	 * @version 1.0.0
	 * @since 1.0.0
	 */
	class Nft_Marketplace_Core_Panel_Theme_Upgrader extends Theme_Upgrader {

		/**
		 * Initialize the upgrade strings.
		 *
		 * @since 1.0.0
		 */
		public function upgrade_strings() {
			parent::upgrade_strings();

			$this->strings['downloading_package'] = esc_html__( 'Downloading the upgrade package', 'nft-marketplace-core' );
		}

		/**
		 * Initialize the install strings.
		 *
		 * @since 1.0.0
		 */
		public function install_strings() {
			parent::install_strings();

			$this->strings['downloading_package'] = esc_html__( 'Downloading the install package', 'nft-marketplace-core' );
		}
	}

endif;

if ( ! class_exists( 'Nft_Marketplace_Core_Panel_Plugin_Upgrader' ) ) :

	/**
	 * Extends the WordPress Plugin_Upgrader class.
	 *
	 * This class makes modifications to the strings during install & upgrade.
	 *
	 * @class Nft_Marketplace_Core_Panel_Plugin_Upgrader
	 * @version 1.0.0
	 * @since 1.0.0
	 */
	class Nft_Marketplace_Core_Panel_Plugin_Upgrader extends Plugin_Upgrader {

		/**
		 * Initialize the upgrade strings.
		 *
		 * @since 1.0.0
		 */
		public function upgrade_strings() {
			parent::upgrade_strings();

			$this->strings['downloading_package'] = esc_html__( 'Downloading the upgrade package', 'nft-marketplace-core' );
		}

		/**
		 * Initialize the install strings.
		 *
		 * @since 1.0.0
		 */
		public function install_strings() {
			parent::install_strings();

			$this->strings['downloading_package'] = esc_html__( 'Downloading the package', 'nft-marketplace-core' );
		}
	}

endif;
