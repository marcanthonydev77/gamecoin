<?php

require __DIR__."/NFT_Marketplace_Core_List_Table_Copy.php";

class NFT_Marketplace_Core_List_Table_Custom extends NFT_Marketplace_Core_List_Table_Copy {

    protected function column_default($item, $column_name)
    {
        return $item[$column_name];
    }

    public function column_listing_price($item) {
        return $item["listing_price"].esc_html__(" ether","nft-marketplace-core");
    }

    public function column_is_deployed($item) {
        if($item['is_deployed'] === "1") {
            return esc_html__("Deployed","nft-marketplace-core");
        }
        return esc_html__("Not Deployed","nft-marketplace-core");
    }

    public function column_taxonomy_blockchain_id($item) {
        return esc_html(get_term($item['taxonomy_blockchain_id'])->name);
    }

    public function column_actions($item) {
        ?>
            <?php if($item['is_deployed'] !== "1") { ?>
            <button type="reset" id="deploy-contract" data-blockchain="<?php echo esc_attr(json_encode(get_term_meta($item['taxonomy_blockchain_id']))); ?>" class="button-primary"><?php esc_html_e("Deploy", "nft-marketplace-core") ?></button>
            <?php } ?>
            <a href="<?php echo esc_url( add_query_arg( "edit-nft", $item["id"] ) ) ?>" class="button-secondary"><?php esc_html_e("Edit", "nft-marketplace-core") ?></a>
        <?php
    }

    public function prepare_items()
    {
        global $wpdb;
        $posts_per_page = 50;
        $columns = $this->get_columns();
        $hidden = $this->get_hidden_columns();
        $sortable = $this->get_sortable_columns();
        $data = $wpdb->get_results("SELECT * FROM ".$wpdb->prefix."nft_marketplace_core_contracts LIMIT ".$posts_per_page,ARRAY_A );

        //usort( $data, array( &$this, 'sort_data' ) );

        $currentPage = $this->get_pagenum();
        $totalItems = count($data);

        $this->set_pagination_args( array(
            'total_items' => $totalItems,
            'per_page'    => $posts_per_page
        ) );

        $data = array_slice($data,(($currentPage-1)*$posts_per_page),$posts_per_page);

        $this->_column_headers = array($columns, $hidden, $sortable);
        $this->items = $data;
    }
    /**
     * Gets a list of columns.
     *
     * The format is:
     * - `'internal-name' => 'Title'`
     *
     * @since 3.1.0
     * @abstract
     *
     * @return array
     */
    public function get_columns()
    {
        return [
            "id" => esc_html__("ID","nft-marketplace-core"),
            "taxonomy_blockchain_id" => esc_html__("Blockchain","nft-marketplace-core"),
            "contract_address" => esc_html__("Contract Address","nft-marketplace-core"),
            "listing_price" => esc_html__( "Listing Price","nft-marketplace-core"),
            "is_deployed" => esc_html__( "Is Deployed","nft-marketplace-core"),
            "actions" => esc_html__( "Actions","nft-marketplace-core"),
        ];
    }

    private function table_data()
    {
    }

    private function get_hidden_columns() {
        return [];
    }
}
